# Celery-RS Worker
[![pipeline status](https://gitlab.com/asevans48/datacannon-rs-worker/badges/master/pipeline.svg)](https://gitlab.com/asevans48/datacannon-rs-worker/-/commits/master)

A rust celery worker for building tasks around other celery implementations.

## Current Support

A Kombu-like port will need to be written for this library. Until then, RabbitMQ and 
anything supporting AMQP will work as a broker. Backends use the BackendConfig.

I really need this library for an existing time-sensitive project though.

## Features

Existing features included in release 0.1:

    - SSL support
    - AMQP/RabbitMQ broker support
    - Elastic Search backend Support
    - Redis backend support
    - Client and Workers
    - Routing Key Support
    - Message protocol support and serialization
    - Identification matching Celery
    - Threadable connections in worker ;)
    - Tokio support in the client ;)
    
Features to include later (0.2+):

    - Redis Broker support
    - SQS Broker Support
    - Creation of a messaging framework like Kombu
    - All other backends
    - OAuth2.0 support (RabbitMQ, Elasticsearch)
    - monitoring support (PRIORITY)
    - celerybeat support (PRIORITY)
    - Rust implemented LevelDB Broker
    - 1 to 1 feature matching with celery and maybe some extras

Sorry guys, I am one man on a very specific mission.

## License


Copyright 2019- Andrew Evans

Any use, distribution, redistribution, copying, manipulating, or handling of this software may only be done with the
express permission of the copywrite holder. Freedom of access to this software may change at any stated time unless
stated directly in a written contract. Unauthorized use of this software is prohibited without the express consent of
the author. Derived works shall be considered a part of the original work and are subject to the terms of this license.

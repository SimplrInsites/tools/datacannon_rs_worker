//! Thread handle for future tasks running in the futures create. This is different from the thread
//! handle because of thhe need to handle futures instead of long running threads. This handle
//! ensures that a future executes to completion. Drop sends a completion notice so that the
//! handle can be appropriately dropped in the pool.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::borrow::Borrow;

use crossbeam::channel::Sender;
use futures::future::RemoteHandle;

use crate::threadpool::thread::event::ThreadEvents;

/// Futures Handle
pub struct FuturesHandle{
    thread_id: String,
    event_channel: Sender<ThreadEvents>,
    handle: Option<RemoteHandle<Result<(),()>>>,
}


/// Drop implementation
impl Drop for FuturesHandle{

    /// Send a killed message to the channel
    fn drop(&mut self) {
        let event = ThreadEvents::FUTUREHANDLEDROPPED(self.thread_id.clone());
        let _r = self.event_channel.send(event);
    }
}


/// Futures handle implementation
impl FuturesHandle{

    /// Obtains this handles uuidv4 id which is generated in the constructor. Immutable borrow.
    pub fn get_id(&self) -> &str{
        self.thread_id.borrow()
    }

    /// Terminate the thread. Blocks until finished. Consumes the thread handle.
    pub async fn terminate(&mut self) -> Result<(), ()> {
       if let Some(handle) = self.handle.take(){
           handle.await
       }else{
           error!("300 - Handle to terminate in futures pool was not set");
           Err(())
       }
    }

    /// Creates and returns a new thread handle
    ///
    /// # Arguments
    /// * `thread_id` - The thread id
    /// * `event_channel` - Event channel to send status on
    /// * `handle` - The remote handle generated from the futures pool
    pub fn new(
        thread_id: String, event_channel: Sender<ThreadEvents>, handle: RemoteHandle<Result<(),()>>) -> FuturesHandle{
        FuturesHandle{
            thread_id: thread_id,
            event_channel: event_channel,
            handle: Some(handle),
        }
    }
}

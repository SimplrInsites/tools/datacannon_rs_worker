//! Handles threadpool events. This is specific to the threadpool.
//!
//! ---
//! author: Andrew Evans
//! ---

#[derive(Clone)]
pub enum ThreadEvents{
    THREADKILLED,
    SPAWNFAILED,
    TASKCOMPLETEDWITHERROR(String),
    TASKCOMPLETED(String),
    FUTUREHANDLEDROPPED(String),
    CONTINUE,
    KILL
}

//! Thread handles that contain the kill switch and status of running thread. The implementation
//! allows for the related thread to be
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::{Arc, Condvar, Mutex, Weak};
use std::sync::atomic::{AtomicBool, Ordering};
use std::thread::JoinHandle;

use crossbeam::channel::Sender;

use crate::threadpool::thread::event::ThreadEvents;

/// Thread handle containing a control and status
pub(in crate) struct ThreadHandle {
    event_channel: Sender<ThreadEvents>,
    status: Arc<AtomicBool>,
    control: Weak<AtomicBool>,
    handle: Option<JoinHandle<()>>,
    start_barrier: Arc<(Mutex<bool>, Condvar)>,
    is_working: bool
}

/// Implementation of drop
impl Drop for ThreadHandle{

    /// Drop the thread
    fn drop(&mut self) {
        if self.is_working {
            let event = ThreadEvents::THREADKILLED;
            let _r = self.event_channel.send(event);
        }
    }
}


/// Handle implementation
impl ThreadHandle{

    /// Call before terminating to avoid excess notifications.
    pub fn set_completing(&mut self){
        self.is_working = false;
    }

    /// Tell the thread to terminate on the next loop iteration.
    pub fn set_to_terminate(&mut self){
        let _karc = self.status.clone();
        match self.control.upgrade(){
            Some(karc) => (*karc).store(false, Ordering::Relaxed),
            None => {}
        }
    }

    /// Terminate the thread. Blocks until finished. Consumes the thread handle.
    pub fn terminate(&mut self) -> Result<(), ()>{
        if let Some(handle) = self.handle.take(){
            handle.join().expect("failed to join thread");
            Ok(())
        }else{
            error!("403 - Thread handle not provided in threadpool");
            Err(())
        }
    }

    /// Wait for the thread to start
    pub fn wait_for_start(&mut self){
        let is_working = self.status.clone();
        let starc = self.start_barrier.clone();
        let &(ref smut, ref svar) = &*starc;
        let mut started = smut.lock().unwrap();
        while *started == false && (*is_working).load(Ordering::Relaxed){
            started = svar.wait(started).unwrap();
        }
    }

    /// Check the status of the thread
    #[allow(dead_code)]
    pub fn is_alive(&mut self) -> bool{
        match self.control.upgrade(){
            Some(_) => true,
            None => false
        }
    }

    /// Creates and returns a new thread handle
    ///
    /// # Arguments
    /// * `status` - The status boolean
    /// * `control` - Control for setting the variable
    /// * `handle` - The Join Handle
    pub fn new(
        event_channel: Sender<ThreadEvents>,
        status: Arc<AtomicBool>,
        control: Weak<AtomicBool>,
        handle: JoinHandle<()>,
        start_barrier: Arc<(Mutex<bool>, Condvar)>) -> ThreadHandle{
        ThreadHandle{
            event_channel: event_channel,
            status: status,
            control: control,
            handle: Some(handle),
            start_barrier: start_barrier,
            is_working: true
        }
    }
}

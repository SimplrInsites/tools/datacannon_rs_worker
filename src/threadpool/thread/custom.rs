//! Base custom thread for the pool. On start and the call function run in the new thread.
//! teardown runs when the thread is about to complete. The on complete function runs when the
//! thread is about to go out of scope.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::{Arc, Condvar, Mutex};
use std::sync::atomic::AtomicBool;

use crossbeam::channel::{Receiver, Sender};
use datacannon_rs_core::task::result::TaskResponse;
use tokio::sync::mpsc;

use crate::task::runner::TaskRunner;
use crate::threadpool::thread::event::ThreadEvents;

/// The pool must be able to run threads implementing a specific API. Users will implement this
/// API and pass the method to be executed to the pool.
#[derive(Clone)]
pub struct CustomPoolThread{
    pub event_channel: Sender<ThreadEvents>,
    pub on_start:  Arc<fn() -> Result<(), ()>>,
    pub on_teardown: Arc<fn() -> Result<(), ()>>,
    pub call_fn: Arc<fn(task_queue: Receiver<TaskRunner>, backend_sender: Vec<mpsc::UnboundedSender<TaskResponse>>, working: Arc<AtomicBool>, cvar_pair: Arc<(Mutex<bool>, Condvar)>, svar_pair: Arc<(Mutex<usize>, Condvar)>)>,
    pub is_setup_complete: bool
}


/// Implementation of drop
impl Drop for CustomPoolThread{

    /// Drop the thread
    fn drop(&mut self) {
        if self.is_setup_complete == false {
            let event = ThreadEvents::THREADKILLED;
            let _r = self.event_channel.send(event);
        }
    }
}


/// Implementation of the custom thread
impl CustomPoolThread{

    /// Creates a new custom thread struct, storing relevant functions. All functions pass through
    /// a thread boundary. Make sure to include this in your tests. `move` is used to move variables
    /// to the thread.
    ///
    /// # Arguments
    /// * `on_start` - Called when a thread is first started that must return nothing
    /// * `on_teardown` - Teardown function that must return nothing
    /// * `call_fn` - Function to call that must return a response to be sent to any backend handlers
    pub fn new(event_channel: Sender<ThreadEvents>, on_start: Arc<fn() -> Result<(), ()>>, on_teardown: Arc<fn() -> Result<(), ()>>, call_fn: Arc<fn(task_queue: Receiver<TaskRunner>, backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>, working: Arc<AtomicBool>, cvar_pair: Arc<(Mutex<bool>, Condvar)>, svar_pair: Arc<(Mutex<usize>, Condvar)>)>) -> CustomPoolThread {
        CustomPoolThread{
            event_channel,
            on_start: on_start,
            on_teardown: on_teardown,
            call_fn: call_fn,
            is_setup_complete: false
        }
    }
}

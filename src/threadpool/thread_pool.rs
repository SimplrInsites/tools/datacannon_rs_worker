//! Custom thread pool allowing users to execute their own code in threads. Users can implement
//! startup and cleanup code and interface with other tools and languages using this threadpool.
//!
//! To limit spin locking when queues are empty and create some backpressure and because Rust
//! dropped their Semaphore, two conditional variables attached to relevant mutexes allow the
//! threads to wait for work and then submit function to block until ready to push.
//!
//! NOTE: If the behavior of blocking send to the task queue causes any issues, the backpressure
//! conditional variable pair may change.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;
use std::panic;
use std::sync::{Arc, Condvar, Mutex, RwLock};
use std::sync::atomic::{AtomicBool, Ordering};
use std::thread;
use std::thread::JoinHandle;
use std::time::Duration;

use crossbeam::channel;
use crossbeam::channel::{Receiver, Sender};
use datacannon_rs_core::argparse::argtype::ArgType;
use datacannon_rs_core::broker::broker_type;
use datacannon_rs_core::config::config::CannonConfig;
use datacannon_rs_core::error::tasksearch::TaskNotFoundError;
use datacannon_rs_core::message_protocol::stream::StreamConfig;
use datacannon_rs_core::task::config::TaskConfig;
use datacannon_rs_core::task::context::TaskContext;
use datacannon_rs_core::task::result::{Response, TaskResponse};
use tokio::sync::mpsc;

use crate::task::runner::TaskRunner;
use crate::task::wrapper::Task;
use crate::threadpool::thread::custom::CustomPoolThread;
use crate::threadpool::thread::event::ThreadEvents;
use crate::threadpool::thread::handle::ThreadHandle;

/// Custom Pool configuration instantiated by the user
#[derive(Clone, Debug)]
pub struct CustomPoolConfig{
    pub pool_size: usize,
    pub max_tasks: Option<usize>,
    pub max_failures: usize,
    pub failures_per_n_calls: usize,
    pub on_start: fn() -> Result<(), ()>,
    pub on_teardown: fn() -> Result<(),()>,
    pub call_fn: fn(task_queue: Receiver<TaskRunner>, backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>, working: Arc<AtomicBool>, cvar_pair: Arc<(Mutex<bool>, Condvar)>, svar_pair: Arc<(Mutex<usize>, Condvar)>)
}


/// A configuration used to instantiate the pool in a given parallelism type.
#[derive(Clone, Debug)]
pub struct CustomThreadPoolConfig{
    pub pool_size: usize,
    pub backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>,
    pub max_tasks: Option<usize>,
    pub max_failures: usize,
    pub failures_per_n_calls: usize,
    pub pool_state: Arc<AtomicBool>,
    pub on_start: fn() -> Result<(), ()>,
    pub on_teardown: fn() -> Result<(),()>,
    pub call_fn: fn(task_queue: Receiver<TaskRunner>, backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>, working: Arc<AtomicBool>, cvar_pair: Arc<(Mutex<bool>, Condvar)>, svar_pair: Arc<(Mutex<usize>, Condvar)>)
}

/// Stores the actual pool. The public implementation must be lockable.
pub struct CustomThreadPool{
    registry: Arc<RwLock<HashMap<&'static str, Task>>>,
    config: CannonConfig<'static>,
    event_sender: Sender<ThreadEvents>,
    event_channel: Arc<Receiver<ThreadEvents>>,
    pool_size: usize,
    pool_state: Arc<AtomicBool>,
    threads: Vec<ThreadHandle>,
    task_receiver: Receiver<TaskRunner>,
    task_sender: Sender<TaskRunner>,
    backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>,
    cvar_pair: Arc<(Mutex<bool>,Condvar)>,
    sema_pair: Arc<(Mutex<usize>, Condvar)>,
    on_start:  Option<Arc<fn() -> Result<(), ()>>>,
    on_teardown: Option<Arc<fn() -> Result<(), ()>>>,
    call_fn: Option<Arc<fn(task_queue: Receiver<TaskRunner>, backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>, working: Arc<AtomicBool>, cvar_pair: Arc<(Mutex<bool>, Condvar)>, svar_pair: Arc<(Mutex<usize>, Condvar)>)>>,
    max_failures: usize,
    failures_per_n_calls: usize,
    current_failures: usize,
    current_calls: usize,
    max_tasks: usize
}


/// The pool thread implementation
impl CustomThreadPool{

    /// Submit a task to the underlying pool. Returns a flag that allows you to kill the thread.
    /// Beacuse of the way rust operates, this function may be unduly long.
    ///
    /// # Arguments
    /// * `t` - Setup for a custom threadpool
    /// * `cvar_pair` - Conditional variable to wait on as well as the associated lock
    /// * `sema_pair` - Conditional variable used to increase the number of permits for submission.
    #[allow(dead_code)]
    pub fn create_thread(
        &mut self, mut cthread: CustomPoolThread, cvar_pair: Arc<(Mutex<bool>,Condvar)>, svar_pair: Arc<(Mutex<usize>,Condvar)>) {
        let working = Arc::new(AtomicBool::new(true));
        let control = Arc::downgrade(&working);
        let w_arc = working.clone();
        let bqs = self.backend_senders.clone();
        let tq = self.task_receiver.clone();
        let startvar = Condvar::new();
        let startmut = Mutex::new(false);
        let start_arc = Arc::new((startmut, startvar));
        let tstart_arc = start_arc.clone();
        cthread.is_setup_complete = true;
        let t = cthread.clone();
        let h = thread::spawn(move||{
            let &(ref start_lock, ref start_var) = &*tstart_arc;
            let r = (t.on_start)();
            if r.is_ok() {
                {
                    let mut started = start_lock.lock().unwrap();
                    *started = true;
                    start_var.notify_all();
                }
                (t.call_fn)(tq, bqs, w_arc.clone(), cvar_pair, svar_pair);
                let teardown_result = (t.on_teardown)();
                if teardown_result.is_err(){
                    error!("402 - Teardown failed in thread");
                }
                (*w_arc).store(false, Ordering::Relaxed);
            }else{
                error!("401 - Failed to Spawn Thread");
                (*w_arc).store(false, Ordering::Relaxed);
            }
        });
        let thread_handle = ThreadHandle::new(self.event_sender.clone(), working, control, h, start_arc);
        self.threads.push(thread_handle);
    }

    /// Blocks and waits for the number of pemits to increase beyond 0.
    fn block_for_submission(&mut self){
        let (lock, cond) = &*self.sema_pair;
        let mut permits = lock.lock().unwrap();
        while *permits == 0{
            permits = cond.wait(permits).unwrap();
        }
        *permits -= 1;
    }

    /// Check and restart threads when dead or run a panic if the error threshold is exceeded.
    fn check_and_restart_dead_threads(&mut self){
        let mut event = self.event_channel.try_recv();
        while event.is_ok(){
            let tstart = self.on_start.clone().unwrap();
            let tdown= self.on_teardown.clone().unwrap();
            let tcfn = self.call_fn.clone().unwrap();
            let event_type = event.ok().unwrap();
            let spair = self.sema_pair.clone();
            if let ThreadEvents::THREADKILLED = event_type{
                let (lock, condvar) = &*spair.clone();
                let th = CustomPoolThread::new(
                    self.event_sender.clone(), tstart, tdown,tcfn);
                self.create_thread(th, self.cvar_pair.clone(), spair.clone());
                let mut permits = lock.lock().unwrap();
                if *permits < self.max_tasks{
                    *permits += 1 ;
                    condvar.notify_all();
                }
                self.current_failures += 1;
                if self.current_failures >= self.max_failures && self.current_calls < self.failures_per_n_calls{
                    error!("410 - More Failures than allowed in custom thread pool");
                    panic!("410 - More failures tha allowed by custom thread pool");
                }
            }
            event = self.event_channel.try_recv();
        }
    }

    /// Submit a task to the pool
    ///
    /// # Arguments
    /// * `args` - Argument vector
    /// * `kwargs` - Task mapped arguments
    /// * `task_config` - Task configuration
    /// * `stream_config` - stream configuration for task execution
    pub fn submit(
        &mut self, args: Vec<ArgType>, kwargs: HashMap<String, ArgType>, task_config: TaskConfig, stream_config: StreamConfig) -> Result<bool, TaskNotFoundError>{
        let registry = self.registry.clone();
        let task_name = task_config.get_task_name();
        let rlock = registry.read();
        if rlock.is_ok() {
            let gaurd = rlock.unwrap();
            let task_opt = (*gaurd).get(task_name.as_str());
            if task_opt.is_some() {
                let task = task_opt.unwrap().clone();
                let task_runner = TaskRunner::new(args, kwargs, task_config.clone(), stream_config, task.f);
                let tq = self.task_sender.clone();
                self.check_and_restart_dead_threads();
                self.block_for_submission();
                let _r = tq.send(task_runner);
                self.current_calls += 1;
                if self.current_calls >= self.failures_per_n_calls {
                    self.current_calls = 0;
                    self.current_failures = 0;
                }
                let (lock, cvar) = &*self.cvar_pair;
                let mut has_work = lock.lock().unwrap();
                if !*has_work {
                    *has_work = true;
                    cvar.notify_all();
                }
                Ok(true)
            } else {
                Err(TaskNotFoundError)
            }
        }else{
            Err(TaskNotFoundError)
        }
    }

    fn get_termination_task(config: CannonConfig<'static>) -> TaskConfig{
        TaskConfig::new(
            config,
            broker_type::RABBITMQ.to_string(),
            "TERMINATE".to_string(),
            None,
            None,
            None,
            None,
            None,
            None,
            Some("TERMINATE".to_string()),
            None,
            None,
            None,
            None,
            None,
            10000,
            Some(1),
            None,
            None)
    }

    fn end_fn(_ctx: TaskContext) -> Result<Response, String>{
        Err("TERMINATING".to_string())
    }

    /// Terminate all threads in this pool and cleanup the pool. If one or more threads fail to
    /// terminate, an error is returned. Consumes the pool.
    pub fn close(self) -> Result<(), ()> {
        let cfg = self.config.clone();
        self.pool_state.store(false, Ordering::Relaxed);
        let mut r = Ok(());
        let mut thread_vec = self.threads;
        let sender = self.task_sender.clone();
        let mut new_vec = vec![];
        for _i in 0..thread_vec.len(){
            let mut handle = thread_vec.pop().unwrap();
            handle.set_to_terminate();
            handle.set_completing();
            new_vec.push(handle);
            let task = CustomThreadPool::get_termination_task(cfg.clone());
            let sc = StreamConfig::new(None, None);
            let _r = sender.clone().send(
                TaskRunner::new(
                    vec![],
                    HashMap::new(),
                    task,
                    sc,
                    CustomThreadPool::end_fn));
        }
        for mut thread in new_vec{
            let hr = thread.terminate();
            if hr.is_err() {
                r = Err(())
            }
        }
        r
    }

    /// Start the pool. Must be called to run the pool.
    ///
    /// # Arguments
    /// * `on_start` - Called when a thread is first started that must return nothing
    /// * `on_teardown` - Teardown function that must return nothing
    /// * `call_fn` - Function to call that must return a( response to be sent to any backend handlers
    pub fn start(&mut self, on_start: fn() -> Result<(), ()>, on_teardown: fn() -> Result<(),()>, call_fn: fn(task_queue: Receiver<TaskRunner>, backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>, working: Arc<AtomicBool>, cvar_pair: Arc<(Mutex<bool>, Condvar)>, svar_pair: Arc<(Mutex<usize>, Condvar)>)){
        self.on_start = Some(Arc::new(on_start));
        self.on_teardown = Some(Arc::new(on_teardown));
        self.call_fn = Some(Arc::new(call_fn));
        for _i in 0..self.pool_size{
            let ch = self.event_sender.clone();
            let th =  CustomPoolThread::new(
                ch,
                self.on_start.clone().unwrap(),
                self.on_teardown.clone().unwrap(),
                self.call_fn.clone().unwrap());
            let tvar= self.cvar_pair.clone();
            let svar = self.sema_pair.clone();
            self.create_thread(th, tvar, svar);
        }
        for i in 0..self.threads.len(){
            let handle = self.threads.get_mut(i);
            handle.unwrap().wait_for_start();
        }
    }

    /// Run in a blocking loop receiving tasks. Allows the pool to be executed from
    /// another thread.
    ///
    /// # Arguments
    /// * `task_receiver` - Queue for receiving the tasks if the pol runs in a separate thread
    /// * `app_control` - Controls the application flow
    pub fn run(&mut self, task_receiver: Receiver<TaskRunner>, app_control: Arc<AtomicBool>){
        let sender = self.task_sender.clone();
        while (*app_control).load(Ordering::Relaxed){
            let task_result = task_receiver.recv();
            if task_result.is_ok(){
                let runner = task_result.ok().unwrap();
                let task_name = runner.task_config.get_task_name().clone();
                if task_name.eq("TERMINATE") == false {
                    let _r = sender.send(runner);
                }
            }
        }
    }

    /// Creates a new thread pool. Requires calling start. Allows for a backlog of tasks
    ///  in the queue at once before creating backpressure.
    ///
    /// # Arguments
    /// * `pool_config` - Pool configuration class
    pub fn new(
        registry: Arc<RwLock<HashMap<&'static str, Task>>>,
        app_config: CannonConfig<'static>,
        pool_config: CustomThreadPoolConfig) -> CustomThreadPool{
        let task_max = pool_config.max_tasks.clone().unwrap_or(10000);
        let (task_sender, task_receiver) = channel::unbounded();
        let (event_sender, event_receiver) = channel::unbounded();
        let thread_vec = Vec::<ThreadHandle>::new();
        let cvar= Condvar::new();
        let cmut: Mutex<bool> = Mutex::new(false);
        let semavar = Condvar::new();
        let semamut = Mutex::new(task_max.clone());
        let state = pool_config.pool_state.clone();
        CustomThreadPool{
            registry,
            config: app_config,
            event_sender: event_sender,
            event_channel: Arc::new(event_receiver),
            pool_size: pool_config.pool_size.clone(),
            pool_state: state,
            threads: thread_vec,
            task_receiver: task_receiver,
            task_sender: task_sender,
            backend_senders: pool_config.backend_senders.clone(),
            cvar_pair: Arc::new((cmut, cvar)),
            sema_pair: Arc::new((semamut, semavar)),
            on_start: None,
            on_teardown: None,
            call_fn: None,
            max_failures: pool_config.max_failures.clone(),
            failures_per_n_calls: pool_config.failures_per_n_calls.clone(),
            current_failures: 0,
            current_calls: 0,
            max_tasks: task_max
        }
    }
}


/// Run the pool in a new thread which is more expensive. Returns an appropriate join handle.
///
/// # Arguments
/// * `registry` - Task registry
/// * `app_config` - Application configuration
/// * 'cfg` - Configuration for running the pool
/// * `pool` - The Custom threadpool
/// * `task_receiver` - For sending task information too from anywhere else.
pub fn run_in_thread(
    registry: Arc<RwLock<HashMap<&'static str, Task>>>,
    app_config: CannonConfig<'static>,
    cfg: CustomThreadPoolConfig,
    task_receiver: Receiver<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>) -> JoinHandle<()>{
    let tr = task_receiver;
    thread::spawn(move||{
        let mut pool = CustomThreadPool::new(
            registry, app_config, cfg.clone());
        pool.start(cfg.on_start, cfg.on_teardown, cfg.call_fn);
        while cfg.pool_state.load(Ordering::Relaxed) {
            let runner_opt = tr.recv_timeout(Duration::new(0,50000));
            if runner_opt.is_ok(){
                let (args, kwargs, config, stream) = runner_opt.ok().unwrap();
                let _r = pool.submit(args, kwargs, config, stream);
            }
        }
        let _r = pool.close();
    })
}


/// Run the threadpool on a runtime. The task receiver runs in a runtime. Returns an appropriate
/// join handle.
///
/// # Arguments
/// * `registry` - The task registry
/// * `app_config` - Application Configuration
/// * 'cfg` - Configuration for running the pool
/// * `task_receiver` - Queue for receiving the tasks if the pol runs in a separate thread
/// * `event_receiver` - For receiving continuation events when ready to proceed again.
/// * `app_control` - Controls the application flow
pub async fn run_on_runtime(
    registry: Arc<RwLock<HashMap<&'static str, Task>>>,
    app_config: CannonConfig<'static>,
    cfg: CustomThreadPoolConfig,
    task_receiver: mpsc::UnboundedReceiver<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>) -> tokio::task::JoinHandle<()>{
    let mut tr = task_receiver;
    let pool_cfg = cfg.clone();
    tokio::spawn(async move{
        let mut pool = CustomThreadPool::new(
            registry, app_config, pool_cfg);
        pool.start(cfg.on_start, cfg.on_teardown, cfg.call_fn);
        while cfg.pool_state.load(Ordering::Relaxed) {
            let runner_opt = tr.recv().await;
            if runner_opt.is_some(){
                let (args, kwargs, config, stream) = runner_opt.unwrap();
                let _r = pool.submit(args, kwargs, config, stream);
            }
        }
        let _r = pool.close();
    })
}



#[cfg(test)]
pub mod test{
    use std::{env, panic};
    use std::collections::HashMap;
    use std::sync::{Arc, Condvar, Mutex, RwLock};
    use std::sync::atomic::{AtomicBool, Ordering};
    use std::time::Duration;

    use crossbeam::channel;
    use crossbeam::channel::Receiver;
    use datacannon_rs_core::argparse::argtype::ArgType;
    use datacannon_rs_core::backend::config::BackendConfig;
    use datacannon_rs_core::broker::broker_type;
    use datacannon_rs_core::config::config;
    use datacannon_rs_core::config::config::CannonConfig;
    use datacannon_rs_core::connection::amqp::connection_inf::AMQPConnectionInf;
    use datacannon_rs_core::connection::connection::ConnectionConfig;
    use datacannon_rs_core::message_protocol::stream::StreamConfig;
    use datacannon_rs_core::router::router::Routers;
    use datacannon_rs_core::task::config::TaskConfig;
    use datacannon_rs_core::task::context::TaskContext;
    use datacannon_rs_core::task::result::{Response, TaskResponse};
    use tokio::runtime::Runtime;
    use tokio::sync::mpsc;
    use tokio::time;

    use crate::task::runner::TaskRunner;
    use crate::task::wrapper::Task;
    use crate::threadpool::thread_pool::{CustomThreadPool, CustomThreadPoolConfig, run_in_thread, run_on_runtime};

    fn get_backend_queue() -> (Vec<mpsc::UnboundedSender<TaskResponse>>, mpsc::UnboundedReceiver<TaskResponse>){
        let (s, r) = mpsc::unbounded_channel();
        (vec![s], r)
    }

    /// Test task that can be used as an example. Notice the loop. You can run Python, Java
    /// or other supported code in this loop.
    fn test_task(
        task_receiver: Receiver<TaskRunner>,
        bsend: Vec<mpsc::UnboundedSender<TaskResponse>>,
        working: Arc<AtomicBool>,
        cvar_pair: Arc<(Mutex<bool>, Condvar)>, svar_pair: Arc<(Mutex<usize>, Condvar)>){
        let backend_senders = bsend.clone();
        let &(ref slock, ref svar) = &*svar_pair;
        let &(ref lock, ref cvar) = &*cvar_pair;
        let rt = get_runtime();
        while (*working).load(Ordering::Relaxed) {
            let task_result = panic::catch_unwind( ||{
                task_receiver.recv_timeout(Duration::new(0, 100))
            });
            if task_result.is_ok() {
                let task = task_result.ok().unwrap();
                if task.is_ok() {
                    let r = panic::catch_unwind(|| {
                        let mut runner: TaskRunner = task.unwrap();
                        let sc = runner.get_stream_config().clone();
                        let task_config = runner.get_task().clone();
                        let args = task_config.get_args().clone();
                        let kwargs = task_config.get_kwargs().clone();
                        let stream_config = sc.clone();
                        let task_context = TaskContext::new(
                            Some(args), Some(kwargs), Some(stream_config));
                        let response = runner.get_fn()(task_context);
                        if response.is_ok() {
                            Ok((response.ok().unwrap(), sc, task_config))
                        } else {
                            Err(("Task Failed", sc, task_config))
                        }
                    });
                    let senders = backend_senders.clone();
                    if r.is_ok() {
                        let task_result = r.ok().unwrap();
                        if task_result.is_ok() {
                            let (response, sc, task_config) = task_result.unwrap();
                            let task_response = TaskResponse::new(task_config, response, sc, true, None);
                            rt.block_on(async move {
                                for sender in senders {
                                    let _r = sender.send(task_response.clone());
                                }
                            });
                        } else {
                            let (err, sc, task_config) = task_result.err().unwrap();
                            let response = Response::new();
                            let task_response = TaskResponse::new(task_config, response, sc, false, Some(err.to_string()));
                            rt.block_on(async move {
                                for sender in senders {
                                    let _r = sender.send(task_response.clone());
                                }
                            });
                        }
                    } else {
                        error!("Function Failed");
                    }
                } else {
                    let mut has_work = lock.lock().unwrap();
                    *has_work = false;
                    while !*has_work {
                        has_work = cvar.wait(has_work).unwrap();
                    }
                }
                let mut permits = slock.lock().unwrap();
                if *permits < 10000 {
                    *permits += 1;
                }
                svar.notify_one();
            }
        }
    }

    fn test_it(_ctx: TaskContext) -> Result<Response, String>{
        for _i in 0..1 {
            //big test string
            let lstr = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

            // really big shitty ((n^2)/2)m regex
            //let re = Regex::new(r"L.*").unwrap();
            //re.find_iter(lstr);

            //really small max nm loop
            let f = lstr.find("industry");
            if f.is_some() {
                let idx = f.unwrap();
                let splarr = lstr.split_at(idx);
                let _r = splarr.0.replace("Lorem", "Lore");
            }
        }
        let mut r= Response::new();
        r.add_result("test_result".to_string(), ArgType::Bool(true));
        Ok(r)
    }

    fn get_pool_config() -> (CustomThreadPoolConfig, mpsc::UnboundedReceiver<TaskResponse>){
        let (rs, rq) = get_backend_queue();
        let on_start = || -> Result<(),()>{
            Ok(())
        };
        let on_teardown =  || -> Result<(),()>{
            Ok(())
        };
        let cfn = test_task;
        (CustomThreadPoolConfig{
            pool_size: 4,
            backend_senders: rs,
            max_tasks: Some(10000),
            max_failures: 0,
            failures_per_n_calls: 0,
            pool_state: Arc::new(AtomicBool::new(true)),
            on_start: on_start,
            on_teardown: on_teardown,
            call_fn: cfn
        }, rq)
    }

    fn get_pool() -> (CustomThreadPool, mpsc::UnboundedReceiver<TaskResponse>, CustomThreadPoolConfig){
        let registry = get_registry();
        let (cfg, rq) = get_pool_config();
        let tcfg = get_config();
        let pool = CustomThreadPool::new(registry, tcfg, cfg.clone());
        (pool, rq, cfg)
    }

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user").ok().unwrap();
        let pwd = env::var("rabbit_test_pwd").ok().unwrap();
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user),
            Some(pwd),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = BackendConfig{
            url: "",
            username: None,
            password: None,
            transport_options: None
        };
        let routers = Routers::new();
        let mut cannon_conf = CannonConfig::new(
            conn_conf, config::BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 100;
        cannon_conf
    }

    fn  get_runtime() -> Runtime{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build();
        rt.unwrap()
    }

    fn get_registry() -> Arc<RwLock<HashMap<&'static str, Task>>>{
        let task = Task{
            f: test_it
        };
        let mut mp = HashMap::new();
        mp.insert("test_it", task);
        Arc::new(RwLock::new(mp))
    }

    #[test]
    fn should_create_thread_pool(){
        get_pool();
    }

    #[test]
    fn should_be_able_to_setup_thread_funcs(){
        let (mut pool, _rq, _cfg) = get_pool();
        let on_start = || -> Result<(),()>{
            Ok(())
        };
        let on_teardown =  || -> Result<(),()>{
            Ok(())
        };

        let cfn = test_task;
        pool.start(on_start, on_teardown, cfn);
        let r = pool.close();
        assert!(r.is_ok());
    }

    fn get_task() -> TaskConfig{
        let cfg = get_config();
        TaskConfig::new(
            cfg,
            broker_type::RABBITMQ.to_string(),
            "test_it".to_string(),
            None,
            None,
            Some("test_exchange".to_string()),
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            10000,
            None,
            Some("rs".to_string()),
            None
        )
    }

    #[test]
    fn should_submit_and_execute_function_on_thread_in_pool() {
        let (mut pool, mut rq, _cfg) = get_pool();
        let on_start = || -> Result<(), ()>{
            Ok(())
        };
        let on_teardown = || -> Result<(), ()>{
            Ok(())
        };
        let task = get_task();
        let cfn = test_task;
        pool.start(on_start, on_teardown, cfn);
        let sc = StreamConfig::new(None, None);
        let r = pool.submit(vec![], HashMap::<String, ArgType>::new(),task, sc);
        assert!(r.is_ok());
        let rt = get_runtime();
        let r = rt.block_on(async move {
            rq.recv().await
        });
        assert!(r.is_some());
        let r: TaskResponse = r.unwrap();
        let resp = r.get_response().clone();
        let v = resp.get_result("test_result");
        assert!(v.is_some());
        let vwrap = v.unwrap();
        if let ArgType::Bool(vwrap) = vwrap {
            assert!(vwrap);
        }
    }

    #[test]
    fn should_load_without_issue() {
        let (mut pool, mut rq, _cfg) = get_pool();
        let on_start = || -> Result<(), ()>{
            Ok(())
        };
        let on_teardown = || -> Result<(), ()>{
            Ok(())
        };

        let cfn = test_task;
        pool.start(on_start, on_teardown, cfn);
        let sc = StreamConfig::new(None, None);
        let cfg = get_config();
        let task = TaskConfig::new(
            cfg,
            broker_type::RABBITMQ.to_string(),
            "test_it".to_string(),
            None,
            None,
            Some("test_exchange".to_string()),
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            100000,
            None,
            Some("rs".to_string()),
            None
        );
        let num_its = 100;
        for _i in 0..num_its.clone() {
            let res = pool.submit(vec![], HashMap::<String, ArgType>::new(), task.clone(), sc.clone());
            assert!(res.is_ok());
        }
        let mut cit = 0;
        let rt = get_runtime();
        rt.block_on(async move {
            while cit < num_its {
                let r = time::timeout(Duration::from_millis(10), rq.recv()).await;
                if r.is_ok() {
                    cit += 1;
                    let r: TaskResponse = r.ok().unwrap().unwrap();
                    let resp = r.get_response().clone();
                    let v = resp.get_result("test_result");
                    assert!(v.is_some());
                    let vwrap = v.unwrap();
                    if let ArgType::Bool(vwrap) = vwrap {
                        assert!(vwrap);
                    }
                }
            }
        });
    }

    #[test]
    fn should_execute_on_thread() {
        let registry = get_registry();
        let cfg = get_config();
        let task = get_task();
        let sc = StreamConfig::new(None, None);
        let (sender, receiver) = channel::unbounded();
        let app_control = Arc::new(AtomicBool::new(true));
        let (pool_cfg, mut rq) = get_pool_config();
        let _h = run_in_thread(
            registry, cfg, pool_cfg.clone(), receiver);
        let sender_c = sender.clone();
        let num_its = 100000;
        for _i in 0..num_its.clone() {
            let _r = sender_c.send((vec![], HashMap::<String, ArgType>::new(), task.clone(), sc.clone()));
        }
        let rt = get_runtime();
        let mut cit = 0;
        rt.block_on(async move {
            while cit < num_its {
                let r = time::timeout(Duration::from_millis(10), rq.recv()).await;
                if r.is_err() {
                    time::sleep(time::Duration::from_millis(10)).await;
                }
                if r.is_ok() {
                    cit += 1;
                    let r: TaskResponse = r.ok().unwrap().unwrap();
                    let resp = r.get_response().clone();
                    let v = resp.get_result("test_result");
                    assert!(v.is_some());
                    let vwrap = v.unwrap();
                    if let ArgType::Bool(vwrap) = vwrap {
                        assert!(vwrap);
                    }
                }
            }
        });
        app_control.store(false, Ordering::Relaxed);
    }

    #[test]
    fn should_execute_on_runtime() {
        let registry = get_registry();
        let cfg = get_config();
        let rt = get_runtime();
        let num_its = 100000;
        let its1 = num_its.clone();
        let (pool_cfg, mut rq) = get_pool_config();
        let (sender, receiver) = tokio::sync::mpsc::unbounded_channel();
        let h = rt.spawn(async move {
            let task = get_task();
            let sc = StreamConfig::new(None, None);
            let _r = run_on_runtime(
                registry,cfg,pool_cfg.clone(), receiver).await;
            for _i in 0..its1 {
                let sender_c = sender.clone();
                let at = task.clone();
                let asc = sc.clone();
                let _r = sender_c.send((vec![], HashMap::<String, ArgType>::new(), at, asc));
            }
        });

        let mut cit = 0;
        rt.block_on(async move {
            let _r = h.await;
            while cit < num_its {
                let r = time::timeout(Duration::from_millis(10), rq.recv()).await;
                if r.is_ok() {
                    cit += 1;
                    let r: TaskResponse = r.ok().unwrap().unwrap();
                    let resp = r.get_response().clone();
                    let v = resp.get_result("test_result");
                    assert!(v.is_some());
                    let vwrap = v.unwrap();
                    if let ArgType::Bool(vwrap) = vwrap {
                        assert!(vwrap);
                    }
                }
            }
        });
    }
}

//! This is not a pool per se but a collection of objects storing information for running tasks
//! on an existing runtime as well as the methods to do so. It will be necessary to determine the
//! pool being used and the nuances for each due to the complications from running each type of
//! pool.
//!
//! A new runtime is not considered for use in the methods which use the context runtime.
//!
//! The pool object actually manages failures to ensure that they do not exceed the allotted amount.
//!
//! Functions must exist within the futures task registry.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use datacannon_rs_core::argparse::argtype::ArgType;
use datacannon_rs_core::message_protocol::stream::StreamConfig;
use datacannon_rs_core::task::config::TaskConfig;
use datacannon_rs_core::task::context::TaskContext;
use datacannon_rs_core::task::result::{Response, TaskResponse};
use tokio;
use tokio::sync::{mpsc, RwLock, Semaphore};
use tokio::task::JoinHandle;
use tokio::time;
use tokio::time::Duration;

use crate::task::wrapper::FutureTask;
use crate::threadpool::thread::event::ThreadEvents;

/// IO Pool configuration set by the user.
#[derive(Clone, Debug)]
pub struct IOPoolConfig{
    pub max_tasks: usize
}


/// Pool configuration for running io operations on an existing runtime.
#[derive(Clone, Debug)]
pub struct IOFuturesPoolConfig{
    pub backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>,
    pub sema: Arc<Semaphore>,
    pub pool_state: Arc<AtomicBool>,
    pub max_tasks: usize
}


/// IOFutures Pool Config
impl IOFuturesPoolConfig{

    /// Create a new pool configuration
    pub fn new(backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>, pool_state: Arc<AtomicBool>, max_tasks: usize) -> IOFuturesPoolConfig{
        let sema = Semaphore::new(max_tasks.clone());
        IOFuturesPoolConfig{
            backend_senders,
            sema: Arc::new(sema),
            pool_state,
            max_tasks
        }
    }
}


/// Structure containing necessary variables for managing the pool.
pub struct IOFuturesPool{
    pool_config: IOFuturesPoolConfig,
    event_sender: mpsc::UnboundedSender<ThreadEvents>,
    events_handle: JoinHandle<()>
}


/// Implementation of the pool
impl IOFuturesPool{

    /// Close the pool and events handle. Consumes and destroys the pool.
    pub async fn close(self) {
        self.pool_config.pool_state.store(false, Ordering::Relaxed);
        let _r = self.events_handle.await;
    }

    /// Get the thread event sender.
    pub fn get_event_sender(&self) -> mpsc::UnboundedSender<ThreadEvents>{
        self.event_sender.clone()
    }

    /// Creates a new futures pool within a runtime.
    ///
    /// # Arguments
    /// * `pool_config` - The pool configuration
    pub async fn new(
        pool_config: IOFuturesPoolConfig) -> IOFuturesPool{
        let (event_sender, event_receiver) = mpsc::unbounded_channel();
        let handle_config = pool_config.clone();
        let handle = tokio::spawn(async move{
            handle_events(handle_config, event_receiver).await;
        });
        IOFuturesPool{
            pool_config: pool_config,
            event_sender: event_sender,
            events_handle: handle
        }
    }
}


/// Handle failures and other issues. Set the termination status as needed.
pub async fn handle_events(pool_config: IOFuturesPoolConfig,
                           receiver: mpsc::UnboundedReceiver<ThreadEvents>){
    let mut event_receiver = receiver;
    while pool_config.pool_state.load(Ordering::Relaxed){
        let event_result = time::timeout(Duration::from_millis(10000), event_receiver.recv()).await;
        if event_result.is_ok(){
            let event = event_result.ok().unwrap().unwrap();
            if let ThreadEvents::TASKCOMPLETEDWITHERROR(event) = event{
                error!("510 - Task Completed with Error [{}]", event);
            }
        }
    }
}


/// Close the pool by setting the termination status.
///
/// # Arguments
/// * `pool_config` - Configuration for the pool
pub fn close(pool_config: IOFuturesPoolConfig){
    pool_config.pool_state.store(false, Ordering::Relaxed);
}


/// Execute a function on the existing runtime.
///
/// # Arguments
/// * `args` - Arguments for the function
/// * `kwargs` - Mapped arguments for the function
/// * `pool_config` - The pool configuration
/// * `task_config` - Task configuration
/// * `stream_config` - Stream configuration for the task
pub async fn execute(
    registry: Arc<RwLock<HashMap<&'static str, FutureTask>>>,
    args: Vec<ArgType>,
    kwargs: HashMap<String, ArgType>,
    pool_config: IOFuturesPoolConfig,
    task_config: TaskConfig,
    stream_config: StreamConfig,
    event_sender: mpsc::UnboundedSender<ThreadEvents>){
    let sema = pool_config.sema.clone();
    let backend_senders = pool_config.backend_senders.clone();
    let task_name = task_config.get_task_name();
    let reg = registry.clone();
    let rlock = reg.read().await;
    let task_opt = (*rlock).get(task_name.as_str());
    if task_opt.is_some(){
        let task = task_opt.unwrap();
        let task_context = TaskContext::new(Some(args), Some(kwargs), Some(stream_config.clone()));
        let f = task.f;
        drop(rlock);
        let response_result = f(task_context).await;
        if response_result.is_ok(){
            let response = response_result.unwrap();
            if response.is_ok() {
                let response_vars = response.unwrap();
                let task_response = TaskResponse::new(
                    task_config.clone(), response_vars, stream_config.clone(), true, None);
                for backend_sender in backend_senders {
                    let _r = backend_sender.send(task_response.clone());
                }
            }else{
                let e = format!(
                    "Failed to Join Task - [{}][{}]", task_config.get_correlation_id(), task_config.get_task_name());
                let response = Response::new();
                task_config.get_correlation_id();
                let task_response = TaskResponse::new(
                    task_config.clone(), response, stream_config.clone(), true, Some(e));
                for backend_sender in backend_senders {
                    let _r = backend_sender.send(task_response.clone());
                }
                let event = ThreadEvents::TASKCOMPLETEDWITHERROR(task_config.get_correlation_id().clone());
                let _r = event_sender.send(event);
            }
        }else{
            let e = format!(
                "Failed to Join Task - [{}][{}]", task_config.get_correlation_id(), task_config.get_task_name());
            let response = Response::new();
            task_config.get_correlation_id();
            let task_response = TaskResponse::new(
                task_config.clone(), response, stream_config.clone(), true, Some(e));
            for backend_sender in backend_senders {
                let _r = backend_sender.send(task_response.clone());
            }
            let event = ThreadEvents::TASKCOMPLETEDWITHERROR(task_config.get_correlation_id().clone());
            let _r = event_sender.send(event);
        }
        sema.add_permits(1);
    }
}


/// Executes the pool in a runtime. Return the join Handle
///
/// # Arguments
/// * `config` - The pool configuration
/// * `task_receiver` - Receiver for obtaining task submission information
pub async fn run_on_runtime(
    registry: Arc<RwLock<HashMap<&'static str, FutureTask>>>,
    config: IOFuturesPoolConfig,
    task_receiver: mpsc::UnboundedReceiver<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>) -> JoinHandle<()>{
    let tconfig = config.clone();
    let pool_state = config.pool_state.clone();
    let mut tr = task_receiver;
    let f  = tokio::spawn(async move{
        let sema = tconfig.sema.clone();
        let econfig = tconfig.clone();
        let pool = IOFuturesPool::new(tconfig).await;
        while pool_state.load(Ordering::Relaxed){
            let task = time::timeout(Duration::from_millis(10), tr.recv()).await;
            if task.is_ok(){
                let (args, kwargs, task_config, stream_config) = task.unwrap().unwrap();
                let permit = sema.acquire().await.unwrap();
                permit.forget();
                let event_sender = pool.get_event_sender();
                let async_config = econfig.clone();
                let arc_reg = registry.clone();
                tokio::spawn(async move{
                    execute(
                        arc_reg,
                        args,
                        kwargs,
                        async_config,
                        task_config,
                        stream_config,
                        event_sender).await;
                });
            }
        }
        pool.close().await;
    });
    f
}


#[cfg(test)]
pub mod tests{
    use std::collections::HashMap;
    use std::env;
    use std::sync::Arc;
    use std::sync::atomic::{AtomicBool, Ordering};

    use datacannon_rs_core::argparse::argtype::ArgType;
    use datacannon_rs_core::backend::config::BackendConfig;
    use datacannon_rs_core::broker::broker_type;
    use datacannon_rs_core::config::config;
    use datacannon_rs_core::config::config::CannonConfig;
    use datacannon_rs_core::connection::amqp::connection_inf::AMQPConnectionInf;
    use datacannon_rs_core::connection::connection::ConnectionConfig;
    use datacannon_rs_core::message_protocol::stream::StreamConfig;
    use datacannon_rs_core::router::router::Routers;
    use datacannon_rs_core::task::config::TaskConfig;
    use datacannon_rs_core::task::context::TaskContext;
    use datacannon_rs_core::task::result::{Response, TaskResponse};
    use tokio;
    use tokio::runtime::Runtime;
    use tokio::sync::{mpsc, RwLock};
    use tokio::task::JoinHandle;

    use crate::task::wrapper::FutureTask;
    use crate::threadpool::io_futures_pool::{execute, IOFuturesPool, IOFuturesPoolConfig, run_on_runtime};

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user").ok().unwrap();
        let pwd = env::var("rabbit_test_pwd").ok().unwrap();
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user),
            Some(pwd),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = BackendConfig{
            url: "",
            username: None,
            password: None,
            transport_options: None
        };
        let routers = Routers::new();
        let mut cannon_conf = CannonConfig::new(
            conn_conf, config::BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 100;
        cannon_conf
    }

    fn get_task() -> TaskConfig{
        let cfg = get_config();
        TaskConfig::new(
            cfg,
            broker_type::RABBITMQ.to_string(),
            "test_it".to_string(),
            None,
            None,
            Some("test_exchange".to_string()),
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            100000,
            None,
            Some("rs".to_string()),
            None
        )
    }

    fn get_registry() -> Arc<RwLock<HashMap<&'static str, FutureTask>>>{
        let ft = FutureTask{
            f: test_it
        };
        let mut mp = HashMap::new();
        mp.insert("test_it", ft);
        Arc::new(RwLock::new(mp))
    }

    fn get_runtime() -> Arc<Runtime> {
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build();
        Arc::new(rt.unwrap())
    }

    fn test_it(_ctx: TaskContext) -> JoinHandle<Result<Response, String>>{
        tokio::spawn(async move {
            let lstr = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
            let f = lstr.find("industry");
            if f.is_some() {
                let idx = f.unwrap();
                let splarr = lstr.split_at(idx);
                let _r = splarr.0.replace("Lorem", "Lore");
            }
            let mut r = Response::new();
            r.add_result("test_result".to_string(), ArgType::Bool(true));
            Ok(r)
        })
    }

    fn get_pool_config(state: Arc<AtomicBool>) -> (IOFuturesPoolConfig, mpsc::UnboundedReceiver<TaskResponse>){
        let (backend_sender, backend_receiver) = mpsc::unbounded_channel();
        (IOFuturesPoolConfig::new(vec![backend_sender], state, 1000), backend_receiver)
    }

    #[test]
    fn should_create_pool(){
        let state = Arc::new(AtomicBool::new(true));
        let (config, _receiver) = get_pool_config(state);
        let _pool = IOFuturesPool::new(config);
    }

    #[test]
    fn should_execute_function(){
        let rt = get_runtime();
        let registry = get_registry();
        rt.block_on(async move {
            let state = Arc::new(AtomicBool::new(true));
            let (config, mut receiver) = get_pool_config(state);
            let pool = IOFuturesPool::new(config.clone()).await;
            let task = get_task();
            let stream_config = StreamConfig::new(None, None);
            let esender = pool.get_event_sender().clone();
            execute(registry,
                    vec![],
                    HashMap::<String, ArgType>::new(),
                    config.clone(),
                    task.clone(),
                    stream_config.clone(),
                    esender).await;
            let response_result = receiver.recv().await;
            assert!(response_result.is_some());
            let response = response_result.unwrap();
            let r = response.get_response().clone();
            assert!(r.get_result("test_result").is_some());
        });
    }

    #[test]
    fn should_close_pool(){
        let state = Arc::new(AtomicBool::new(true));
        let (config, _receiver) = get_pool_config(state.clone());
        let _pool = IOFuturesPool::new(config);
        state.store(false, Ordering::Relaxed);
    }

    #[test]
    fn should_execute_on_runtime() {
        let rt = get_runtime();
        let registry = get_registry();
        rt.block_on(async move {
            let (task_sender, task_receiver) = mpsc::unbounded_channel();
            let state = Arc::new(AtomicBool::new(true));
            let (config, mut receiver) = get_pool_config(state.clone());
            let _handle = run_on_runtime(registry, config, task_receiver).await;
            let task = get_task();
            let stream_config = StreamConfig::new(None, None);
            let _r = task_sender.send((vec![], HashMap::<String, ArgType>::new(), task, stream_config));
            let response_result = receiver.recv().await;
            assert!(response_result.is_some());
            let response = response_result.unwrap();
            let r = response.get_response().clone();
            let ropt = r.get_result("test_result");
            assert!(ropt.is_some());
            let rval = ropt.unwrap();
            if let ArgType::Bool(rval) = rval {
                assert!(rval);
            } else {
                assert!(false);
            }
        });
    }

    #[test]
    fn should_load_on_runtime(){
        let rt = get_runtime();
        let registry = get_registry();
        rt.block_on(async move {
            let (task_sender, task_receiver) = mpsc::unbounded_channel();
            let state = Arc::new(AtomicBool::new(true));
            let (config, mut receiver) = get_pool_config(state.clone());
            let _handle = run_on_runtime(registry, config, task_receiver).await;
            let task = get_task();
            let stream_config = StreamConfig::new(None, None);
            let cit = 100000 as i32;
            for _i in 0..cit {
                let _r = task_sender.clone().send((vec![], HashMap::<String, ArgType>::new(), task.clone(), stream_config.clone()));
            }
            for _j in 0..cit {
                let response_result = receiver.recv().await;
                assert!(response_result.is_some());
                let response = response_result.unwrap();
                let r = response.get_response().clone();
                let ropt = r.get_result("test_result");
                assert!(ropt.is_some());
                let rval = ropt.unwrap();
                if let ArgType::Bool(rval) = rval{
                    assert!(rval);
                }else{
                    assert!(false);
                }
            }
        });
    }
}

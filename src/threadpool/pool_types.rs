//! Enumeration related to the pool types. Tasks need to register tasks to a specific pool. This
//! enumeration allows them to specify which.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;

use datacannon_rs_core::argparse::argtype::ArgType;
use datacannon_rs_core::message_protocol::stream::StreamConfig;
use datacannon_rs_core::task::config::TaskConfig;
use tokio::sync::mpsc;
use tokio::task::JoinHandle;

use crate::threadpool::cpu_pool::CPUPoolConfig;
use crate::threadpool::io_futures_pool::IOPoolConfig;
use crate::threadpool::thread_pool::CustomPoolConfig;

/// Pool type enumeration
#[derive(Clone, Debug)]
pub enum PoolType{
    IOPOOL(IOPoolConfig),
    CPUPOOL(CPUPoolConfig),
    CUSTOMPOOL(CustomPoolConfig)
}


pub(in crate) struct FuturesPoolWrapper{
    pub pool_handle: JoinHandle<()>,
    pub task_sender: mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>
}


pub(in crate) struct IOFuturesPoolWrapper{
    pub pool_handle: JoinHandle<()>,
    pub task_sender: mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>
}


pub(in crate) struct CustomThreadPoolWrapper{
    pub pool_handle: JoinHandle<()>,
    pub task_sender: mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>
}

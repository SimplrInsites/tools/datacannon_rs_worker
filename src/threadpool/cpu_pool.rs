//! A threadpool for executing tasks. Executes CPU intensive tasks off of the Tokio Loop
//! The threadpool needs to be safely shared among futures in tokio. An MPMC queue
//! backs stores tasks to execute. This crate only executes a function and does not
//! handle pushing to the backend.
//!
//! While the custom threadpoool requires implementing constantly running threads and managing them,
//! this CPU bound pool offloads these functions to the futures crate. This allow for only a
//! backpressure semaphore to be used.
//!
//! To avoid blocking the main loop. Threadpools should be run in their own loop.
//!
//! This threadpool requires an extra thread to run and execute tasks to avoid blocking
//! the main loop. This is beacuse of the need to consume tasks.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::{panic, thread};
use std::collections::HashMap;
use std::sync::{Arc, Condvar, Mutex, RwLock};
use std::sync::atomic::{AtomicBool, Ordering};
use std::thread::JoinHandle;
use std::time::Duration;

use crossbeam::channel::{Receiver, Sender};
use datacannon_rs_core::argparse::argtype::ArgType;
use datacannon_rs_core::error::tasksearch::TaskNotFoundError;
use datacannon_rs_core::message_protocol::stream::StreamConfig;
use datacannon_rs_core::task::config::TaskConfig;
use datacannon_rs_core::task::context::TaskContext;
use datacannon_rs_core::task::result::{Response, TaskResponse};
use futures::executor;
use futures::executor::ThreadPoolBuilder;
use tokio::sync::mpsc;

use crate::task::runner::TaskRunner;
use crate::task::wrapper::Task;
use crate::threadpool::thread::event::ThreadEvents;

/// Configuration for the user. Pool size is the size of the futures threadpool. Max queued
/// tasks specifies the maximum number of tasks that can wait in the queue.
#[derive(Clone, Debug)]
pub struct CPUPoolConfig{
    pub pool_size: usize,
    pub max_queued_tasks: usize,
    pub failures_per_n_calls: usize,
    pub reset_failures_on_n_calls: usize
}


/// Configuration for a futures pool for users or the application to provide.
#[derive(Clone, Debug)]
pub struct FuturesPoolConfig{
    pub pool_size: usize,
    pub failures_per_n_calls: usize,
    pub reset_failures_on_n_calls: usize,
    pub event_sender: Sender<ThreadEvents>,
    pub event_receiver: Receiver<ThreadEvents>,
    pub task_sender: Sender<TaskRunner>,
    pub task_receiver: Receiver<TaskRunner>,
    pub backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>,
    pub status: Arc<AtomicBool>,
    pub sema_pair: Arc<(Mutex<usize>, Condvar)>
}


/// Futures pool whose implementation controls access to the queues for task management
pub struct FuturesPool{
    registry: Arc<RwLock<HashMap<&'static str, Task>>>,
    event_receiver: Receiver<ThreadEvents>,
    task_sender: Sender<TaskRunner>,
    sema_pair: Arc<(Mutex<usize>, Condvar)>,
    failures_per_n_calls: usize,
    reset_failures_on_n_calls: usize,
    current_failures: usize,
    current_calls: usize,
    status: Arc<AtomicBool>
}


/// Implementation of the futures pool
impl FuturesPool{

    /// Handle failures appropriately
    fn handle_completions_and_failures(&mut self){
        loop {
            let event_result = self.event_receiver.try_recv();
            if event_result.is_err(){
                break;
            }else{
                let event = event_result.ok().unwrap();
                if let ThreadEvents::TASKCOMPLETEDWITHERROR(event) = event{
                    error!("Task Failed in Futures Pool- [{}]", event);
                    eprintln!("Task Failed in Futures Pool- [{}]", event);
                    self.current_failures += 1;
                }else if let ThreadEvents::TASKCOMPLETED(event) = event{
                    debug!("Task Completed in Futures Pool - [{}]", event);
                }else if let ThreadEvents::SPAWNFAILED = event{
                    error!("Failed to spawn in Futures Pool");
                    self.current_failures += 1;
                }
                if self.current_failures >= self.reset_failures_on_n_calls{
                    error!("310 - More Failures than allowed in futures pool");
                    panic!("310 - More Failures than allowed in futures pool");
                }
            }
        }
    }

    /// Blocks and waits for the number of pemits to increase beyond 0.
    fn block_for_submission(&mut self){
        let (lock, cond) = &*self.sema_pair;
        let mut permits = lock.lock().unwrap();
        while *permits == 0{
            permits = cond.wait(permits).unwrap();
        }
        *permits -= 1;
    }

    /// Update calls as needed
    fn update_calls(&mut self){
        self.current_calls += 1;
        if self.current_calls > self.failures_per_n_calls{
            self.current_calls = 0;
            self.current_failures = 0;
        }
    }

    /// Set the pool status to closing, forcing termination of all loops.
    pub fn close(&mut self) {
        self.status.store(false, Ordering::Relaxed);
    }

    /// Submit a task to the task channel while ensuring the existance of a sufficient number of
    /// permits. The permits object is threadsafe. The conditional variable is waited on. Permits
    /// themselves are only updated on completion of a task
    ///
    /// # Arguments
    /// * `args` - Arguments to use in execution
    /// * `kwargs` - Mapped arguments
    /// * `task_config` - Task configuration
    /// * `stream_config` - stream configuration for task execution
    pub fn submit(
        &mut self, args: Vec<ArgType>, kwargs: HashMap<String, ArgType>, task_config: TaskConfig, stream_config: StreamConfig) -> Result<Result<bool, ()>, TaskNotFoundError>{
        let task_name = task_config.get_task_name().clone();
        let rlock = self.registry.read();
        if rlock.is_ok() {
            let task_opt = {
                let gaurd = rlock.unwrap();
                let opt = (*gaurd).get(task_name.as_str());
                if opt.is_some(){
                    Some(opt.unwrap().clone())
                }else{
                    None
                }
            };
            if task_opt.is_some() {
                self.handle_completions_and_failures();
                self.block_for_submission();
                let task = task_opt.unwrap();
                let task_f = task.f;
                let runner = TaskRunner::new(args, kwargs, task_config, stream_config, task_f);
                let r = self.task_sender.send(runner);
                self.update_calls();
                if r.is_ok() {
                    Ok(Ok(true))
                } else {
                    Ok(Err(()))
                }
            } else {
                Err(TaskNotFoundError)
            }
        }else{
            Err(TaskNotFoundError)
        }
    }

    /// Create a new futures pool.
    pub fn new(config: FuturesPoolConfig, registry: Arc<RwLock<HashMap<&'static str, Task>>>) -> FuturesPool{
        FuturesPool{
            registry,
            sema_pair: config.sema_pair.clone(),
            event_receiver: config.event_receiver.clone(),
            task_sender: config.task_sender.clone(),
            failures_per_n_calls: config.failures_per_n_calls,
            reset_failures_on_n_calls: config.reset_failures_on_n_calls,
            current_failures: 0,
            current_calls: 0,
            status: config.status.clone()
        }
    }
}


async fn execute(
    task_id: String,
    event_sender: Sender<ThreadEvents>,
    runner: TaskRunner,
    backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>,
    sema_pair: Arc<(Mutex<usize>, Condvar)>){
    let f = runner.f;
    let args = runner.args;
    let kwargs = runner.kwargs;
    let sc = runner.stream_config.clone();
    let cfg = runner.task_config.clone();
    let ctx = TaskContext::new(Some(args.clone()), Some(kwargs.clone()), Some(sc.clone()));
    let r = f(ctx);
    if r.is_ok() {
        let resp = r.ok().unwrap();
        let task_resp = TaskResponse::new(cfg, resp, sc, true, None);
        for backend_sender in backend_senders {
            let _r = backend_sender.send(task_resp.clone());
        }
        let event = ThreadEvents::TASKCOMPLETED(task_id.clone());
        let _r = event_sender.send(event);
    }else{
        let err = r.err().unwrap();
        let resp = Response::new();
        let task_resp = TaskResponse::new(cfg, resp, sc, false, Some(err));
        for backend_sender in backend_senders {
            let _r = backend_sender.send(task_resp.clone());
        }
        let m = format!("{} - Task Failed", task_id.clone());
        eprintln!("{}", m);
        error!("{}", m);
        let event = ThreadEvents::TASKCOMPLETEDWITHERROR(task_id);
        let _r = event_sender.send(event);
    }
    {
        let (lock, cvar) = &*sema_pair;
        let mut permits = lock.lock().unwrap();
        *permits += 1;
        cvar.notify_one();
    }
}


async fn create_executor(
    status: Arc<AtomicBool>,
    event_sender: Sender<ThreadEvents>,
    backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>,
    task_receiver: Receiver<TaskRunner>,
    pool_size: usize,
    sema_pair: Arc<(Mutex<usize>, Condvar)>){
    let pool_result = ThreadPoolBuilder::default().pool_size(pool_size).create();
    if pool_result.is_ok() {
        let pool = pool_result.ok().unwrap();
        while status.load(Ordering::Relaxed) {
            let r = task_receiver.recv_timeout(Duration::new(0, 1000));
            if r.is_ok() {
                let tr = r.ok().unwrap();
                let task_id = tr.task_config.get_correlation_id().clone();
                let f = execute(task_id, event_sender.clone(), tr, backend_senders.clone(), sema_pair.clone());
                pool.spawn_ok(f);
            }
        }
        let event = ThreadEvents::KILL;
        let _r = event_sender.clone().send(event);
    }else{
        let event = ThreadEvents::SPAWNFAILED;
        let _r = event_sender.clone().send(event);
    }
}


/// Run the pool on the runtime
///
/// # Arguments
/// * `registry` - Task registry
/// * `config` - The pool configuration
/// * `task_receiver` - Receiver for obtaining task submission information
pub async fn run_on_runtime(
    registry: Arc<RwLock<HashMap<&'static str, Task>>>,
    pool_config: FuturesPoolConfig,
    task_receiver: mpsc::UnboundedReceiver<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>) -> tokio::task::JoinHandle<()>{
    let mut tr = task_receiver;
    let handle = tokio::spawn(async move {
        let app_control = pool_config.status.clone();
        let exec_control = app_control.clone();
        let exec_sender = pool_config.event_sender.clone();
        let exec_backend_senders = pool_config.backend_senders.clone();
        let exec_task_receiver = pool_config.task_receiver.clone();
        let psize = pool_config.pool_size.clone();
        let sema = pool_config.sema_pair.clone();
        let h = thread::spawn(move ||{
            let f = create_executor(
                    exec_control,
                    exec_sender,
                    exec_backend_senders,
                    exec_task_receiver,
                    psize,
                    sema);
            executor::block_on(f);
        });
        let mut pool = FuturesPool::new(pool_config, registry);
        while app_control.load(Ordering::Relaxed){
            let runner_opt = tr.recv().await;
            if runner_opt.is_some(){
                let (args, kwargs, config, stream) = runner_opt.unwrap();
                let _r = pool.submit(args, kwargs, config, stream);
            }
        }
        let _r = pool.close();
        let _r = h.join();
    });
    handle
}


/// Run the pool on the runtime
pub fn run_in_thread(
    registry: Arc<RwLock<HashMap<&'static str, Task>>>,
    pool_config: FuturesPoolConfig,
    task_receiver: Receiver<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>) -> JoinHandle<()>{
    let tr = task_receiver;
    let handle = thread::spawn(move || {
        let app_control = pool_config.status.clone();
        let exec_control = app_control.clone();
        let exec_sender = pool_config.event_sender.clone();
        let exec_backend_senders = pool_config.backend_senders.clone();
        let exec_task_receiver = pool_config.task_receiver.clone();
        let psize = pool_config.pool_size.clone();
        let sema = pool_config.sema_pair.clone();
        let h = thread::spawn(move ||{
            let f = create_executor(
                    exec_control,
                    exec_sender,
                    exec_backend_senders,
                    exec_task_receiver,
                    psize,
                    sema);
            executor::block_on(f);
        });
        let mut pool = FuturesPool::new(pool_config, registry);
        while app_control.load(Ordering::Relaxed){
            let runner_opt = tr.recv_timeout(Duration::new(0,50000));
            if runner_opt.is_ok(){
                let (args, kwargs, config, stream) = runner_opt.ok().unwrap();
                let _r = pool.submit(args, kwargs, config, stream);
            }
        }
        pool.close();
        let _r = h.join();
    });
    handle
}


#[cfg(test)]
pub mod tests{
    use std::{env, thread};
    use std::collections::HashMap;
    use std::sync::{Arc, Condvar, Mutex, RwLock};
    use std::sync::atomic::{AtomicBool, Ordering};

    use crossbeam::channel;
    use datacannon_rs_core::argparse::argtype::ArgType;
    use datacannon_rs_core::backend::config::BackendConfig;
    use datacannon_rs_core::broker::broker_type;
    use datacannon_rs_core::config::config;
    use datacannon_rs_core::config::config::CannonConfig;
    use datacannon_rs_core::connection::amqp::connection_inf::AMQPConnectionInf;
    use datacannon_rs_core::connection::connection::ConnectionConfig;
    use datacannon_rs_core::message_protocol::stream::StreamConfig;
    use datacannon_rs_core::router::router::Routers;
    use datacannon_rs_core::task::config::TaskConfig;
    use datacannon_rs_core::task::context::TaskContext;
    use datacannon_rs_core::task::result::{Response, TaskResponse};
    use futures::executor;
    use tokio::runtime::Runtime;
    use tokio::sync::mpsc;
    use tokio::time;

    use crate::task::runner::TaskRunner;
    use crate::task::wrapper::Task;
    use crate::threadpool::cpu_pool::{create_executor, FuturesPool, FuturesPoolConfig, run_in_thread, run_on_runtime};
    use crate::threadpool::thread::event::ThreadEvents;
    use tokio::time::Duration;

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user").ok().unwrap();
        let pwd = env::var("rabbit_test_pwd").ok().unwrap();
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user),
            Some(pwd),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = BackendConfig{
            url: "",
            username: None,
            password: None,
            transport_options: None
        };
        let routers = Routers::new();
        let mut cannon_conf = CannonConfig::new(
            conn_conf, config::BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 100;
        cannon_conf
    }

    fn get_registry() -> Arc<RwLock<HashMap<&'static str, Task>>>{
        let task = Task{
            f: test_it
        };
        let mut mp = HashMap::new();
        mp.insert("test_it", task);
        Arc::new(RwLock::new(mp))
    }

    fn get_task() -> TaskConfig{
        let cfg = get_config();
        TaskConfig::new(
            cfg,
            broker_type::RABBITMQ.to_string(),
            "test_it".to_string(),
            None,
            None,
            Some("test_exchange".to_string()),
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            100000,
            None,
            Some("rs".to_string()),
            None
        )
    }

    fn get_runtime() -> Arc<Runtime>{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(4)
            .build();
        Arc::new(rt.unwrap())
    }

    fn test_it(_ctx: TaskContext) -> Result<Response, String>{
        for _i in 0..1000 {
            //big test string
            let lstr = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

            // really big shitty ((n^2)/2)m regex
            //let re = Regex::new(r"L.*").unwrap();
            //re.find_iter(lstr);

            //really small max nm loop
            let f = lstr.find("industry");
            if f.is_some() {
                let idx = f.unwrap();
                let splarr = lstr.split_at(idx);
                let _r = splarr.0.replace("Lorem", "Lore");
            }
        }
        let mut r = Response::new();
        r.add_result("test_result".to_string(), ArgType::Bool(true));
        Ok(r)
    }

    #[test]
    fn should_create_and_close_executor(){
        let (event_sender, event_receiver) = channel::bounded(10000);
        let (_task_sender, task_receiver) = channel::bounded(10000);
        let (bs, _br) = mpsc::unbounded_channel();
        let abool = Arc::new(AtomicBool::new(true));
        let tbool = abool.clone();
        let sema_pair = Arc::new((Mutex::new(1000), Condvar::new()));
        let h = thread::spawn(move ||{
            let f = create_executor(
                tbool.clone(),
                event_sender.clone(),
                vec![bs.clone()],
                task_receiver.clone(),
                3,
                sema_pair.clone());
            executor::block_on(f);
        });
        abool.clone().store(false, Ordering::Relaxed);
        let r = event_receiver.recv();
        assert!(r.is_ok());
        let te = r.ok().unwrap();
        if let ThreadEvents::SPAWNFAILED = te{
            assert!(false);
        }
        let _r = h.join();
    }

    #[test]
    fn should_use_executor(){
        let (event_sender, event_receiver) = channel::unbounded();
        let (task_sender, task_receiver) = channel::unbounded();
        let (bs, mut br) = mpsc::unbounded_channel();
        let abool = Arc::new(AtomicBool::new(true));
        let tbool = abool.clone();
        let sema_pair = Arc::new((Mutex::new(1000), Condvar::new()));
        let h = thread::spawn(move ||{
            let f = create_executor(
                tbool.clone(),
                event_sender.clone(),
                vec![bs.clone()],
                task_receiver.clone(),
                3,
                sema_pair.clone());
            executor::block_on(f);
        });
        let task = get_task();
        let sc = StreamConfig::new(None, None);
        let runner = TaskRunner::new(vec![], HashMap::<String, ArgType>::new(),task, sc, test_it);
        let _r = task_sender.send(runner);
        let mut r = event_receiver.recv();
        assert!(r.is_ok());
        let event = r.ok().unwrap();
        if let ThreadEvents::TASKCOMPLETEDWITHERROR(event) = event {
            eprintln!("{}", event);
            assert!(false);
        }
        let rt = get_runtime();
        let backend_result = rt.block_on(async move {
            br.recv().await
        });
        assert!(backend_result.is_some());
        abool.clone().store(false, Ordering::Relaxed);
        r = event_receiver.recv();
        assert!(r.is_ok());
        let _r = h.join();
    }

    #[test]
    fn should_execute_at_load(){
        let (event_sender, event_receiver) = channel::unbounded();
        let (task_sender, task_receiver) = channel::unbounded();
        let (bs, mut br) = mpsc::unbounded_channel();
        let abool = Arc::new(AtomicBool::new(true));
        let tbool = abool.clone();
        let sema_pair = Arc::new((Mutex::new(1000), Condvar::new()));
        let h = thread::spawn(move ||{
            let f = create_executor(
                tbool.clone(),
                event_sender.clone(),
                vec![bs.clone()],
                task_receiver.clone(),
                3,
                sema_pair.clone());
            executor::block_on(f);
        });
        let rt = get_runtime();
        let cit = 1000;
        for _i in 0..cit {
            let task = get_task();
            let sc = StreamConfig::new(None, None);
            let runner = TaskRunner::new(vec![], HashMap::<String, ArgType>::new(), task, sc, test_it);
            let _r = task_sender.send(runner);
        }
        rt.block_on(async move {
            let mut i = 0 as i32;
            while i < cit {
                let backend_result = br.recv().await;
                if backend_result.is_some() {
                    i += 1;
                    let btr = backend_result.unwrap();
                    let mr = btr.get_response().clone();
                    let ropt = mr.get_result("test_result");
                    assert!(ropt.is_some());
                }
            }
        });
        abool.clone().store(false, Ordering::Relaxed);
        let r = event_receiver.recv();
        assert!(r.is_ok());
        let _r = h.join();
    }

    #[test]
    fn should_submit_to_pool(){
        let registry = get_registry();
        let (event_sender, event_receiver) = channel::unbounded();
        let (task_sender, task_receiver) = channel::unbounded();
        let (backend_sender, mut backend_receiver) = mpsc::unbounded_channel();
        let pool_status = Arc::new(AtomicBool::new(true));
        let sema_pair = Arc::new((Mutex::new(1000), Condvar::new()));
        let pool_config = FuturesPoolConfig{
            pool_size: 3,
            failures_per_n_calls: 10,
            reset_failures_on_n_calls: 100,
            event_sender: event_sender.clone(),
            event_receiver: event_receiver.clone(),
            task_sender: task_sender.clone(),
            task_receiver: task_receiver.clone(),
            backend_senders: vec![backend_sender.clone()],
            status: pool_status.clone(),
            sema_pair: sema_pair.clone()
        };
        let psize = pool_config.pool_size.clone();
        let status = pool_config.status.clone();
        let estatus= pool_config.status.clone();
        let mut pool = FuturesPool::new(pool_config, registry);
        let handle = thread::spawn(move||{
            let f = create_executor(
                estatus,
                event_sender.clone(),
                vec![backend_sender.clone()],
                task_receiver.clone(),
                psize,
                sema_pair.clone());
            executor::block_on(f);
        });

        let task = get_task();
        let stream_config = StreamConfig::new(None, None);
        let submit_result = pool.submit(Vec::<ArgType>::new(), HashMap::<String, ArgType>::new(),task, stream_config);
        assert!(submit_result.is_ok());
        let is_spawn = submit_result.ok().unwrap();
        assert!(is_spawn.is_ok());
        let rt = get_runtime();
        let br = rt.block_on(async move {
            backend_receiver.recv().await
        });
        assert!(br.is_some());
        let er = event_receiver.recv();
        assert!(er.is_ok());
        status.clone().store(false, Ordering::Relaxed);
        let _r = handle.join();
    }

    #[test]
    fn should_run_on_runtime(){
        let registry = get_registry();
        let rt = get_runtime();
        rt.block_on(async move {
            let (event_sender, event_receiver) = channel::unbounded();
            let (task_sender, task_receiver) = channel::unbounded();
            let (backend_sender, mut backend_receiver) = mpsc::unbounded_channel();
            let pool_status = Arc::new(AtomicBool::new(true));
            let sema_pair = Arc::new((Mutex::new(10000), Condvar::new()));
            let pool_config = FuturesPoolConfig{
                pool_size: 4,
                failures_per_n_calls: 10,
                reset_failures_on_n_calls: 100,
                event_sender: event_sender.clone(),
                event_receiver: event_receiver.clone(),
                task_sender: task_sender.clone(),
                task_receiver: task_receiver.clone(),
                backend_senders: vec![backend_sender.clone()],
                status: pool_status.clone(),
                sema_pair: sema_pair.clone()
            };
            let status = pool_status.clone();
            let (task_sender, task_receiver) = tokio::sync::mpsc::unbounded_channel();
            let task = get_task();
            let sc = StreamConfig::new(None, None);
            let _handle = run_on_runtime(registry,
                                         pool_config.clone(),
                                         task_receiver).await;
            let num_its = 10000;
            for _i in 0..num_its.clone() {
                let sender_c = task_sender.clone();
                let at = task.clone();
                let asc = sc.clone();
                    let _r = sender_c.send((vec![], HashMap::<String, ArgType>::new(),at, asc));
            }
            let mut cit: i32 = 0;
            while cit < num_its {
                let r = time::timeout(Duration::from_millis(10), backend_receiver.recv()).await;
                if r.is_ok() {
                    cit += 1;
                    let r: TaskResponse = r.ok().unwrap().unwrap();
                    let resp = r.get_response().clone();
                    let v = resp.get_result("test_result");
                    assert!(v.is_some());
                    let vwrap = v.unwrap();
                    if let ArgType::Bool(vwrap) = vwrap {
                        assert!(vwrap);
                    }
                }else{
                    time::sleep(time::Duration::from_millis(10)).await;
                }
            }
            status.store(false, Ordering::Relaxed);
        });
    }

    #[test]
    fn should_run_in_thread(){
        let registry = get_registry();
        let rt = get_runtime();
        rt.block_on(async move {
            let (event_sender, event_receiver) = channel::unbounded();
            let (task_sender, task_receiver) = channel::unbounded();
            let (backend_sender, mut backend_receiver) = mpsc::unbounded_channel();
            let pool_status = Arc::new(AtomicBool::new(true));
            let sema_pair = Arc::new((Mutex::new(100000), Condvar::new()));
            let pool_config = FuturesPoolConfig{
                pool_size: 3,
                failures_per_n_calls: 10,
                reset_failures_on_n_calls: 100,
                event_sender: event_sender.clone(),
                event_receiver: event_receiver.clone(),
                task_sender: task_sender.clone(),
                task_receiver: task_receiver.clone(),
                backend_senders: vec![backend_sender.clone()],
                status: pool_status.clone(),
                sema_pair: sema_pair.clone()
            };
            let status = pool_status.clone();
            let (task_sender, task_receiver) = channel::unbounded();
            let task = get_task();
            let sc = StreamConfig::new(None, None);
            let _handle = run_in_thread(registry,
                                        pool_config.clone(),
                                        task_receiver);
            let num_its = 10000;
            for _i in 0..num_its.clone() {
                let sender_c = task_sender.clone();
                let at = task.clone();
                let asc = sc.clone();
                let _r = sender_c.send((vec![], HashMap::<String, ArgType>::new(),at, asc));
            }
            let mut cit: i32 = 0;
            while cit < num_its {
                let r =  backend_receiver.recv().await;
                if r.is_some() {
                    cit += 1;
                    let r: TaskResponse = r.unwrap();
                    let resp = r.get_response().clone();
                    let v = resp.get_result("test_result");
                    assert!(v.is_some());
                    let vwrap = v.unwrap();
                    if let ArgType::Bool(vwrap) = vwrap {
                        assert!(vwrap);
                    }
                }
            }
            status.store(false, Ordering::Relaxed);
        });
    }
}
//! Chain handler submitting back to the given broker in the task. Requires starting different
//! brokers to allow for pushing back to the broker.
//!
//! The response from the application is added to kwargs for the task.
//!
//! ---
//! author: Andrew Evans
//! ---


use std::collections::HashMap;
use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;

use datacannon_rs_core::argparse::argtype::ArgType;
use datacannon_rs_core::broker::amqp::rabbitmq::RabbitMQBroker;
use datacannon_rs_core::broker::broker_type;
use datacannon_rs_core::config::config::{BrokerType, CannonConfig};
use datacannon_rs_core::message_protocol::stream::StreamConfig;
use datacannon_rs_core::router::router::Routers;
use datacannon_rs_core::task::config::TaskConfig;
use datacannon_rs_core::task::result::TaskResponse;
use tokio::sync::{mpsc, Mutex, RwLock};
use tokio::sync::mpsc::Sender;
use tokio::task::JoinHandle;
use tokio::runtime::Runtime;

/// Chain handler configuration supplied by the user
pub struct ChainHandlerConfig{
    pub status: Arc<AtomicBool>,
    pub routers: Routers,
    pub brokers: Vec<BrokerType>,
    pub receiver: Arc<Mutex<mpsc::UnboundedReceiver<TaskResponse>>>,
}


/// Chain handler for sending tasks back to the correct broker
pub struct ChainHandler{
    config: CannonConfig<'static>,
    routers: Routers,
    brokers: Vec<BrokerType>,
    receiver: Arc<Mutex<mpsc::UnboundedReceiver<TaskResponse>>>,
    status: Arc<AtomicBool>,
    rabbitmq_broker: Option<Arc<Mutex<RabbitMQBroker>>>,
    handle: Option<JoinHandle<()>>,
    result_store: Arc<RwLock<HashMap<String, Arc<Mutex<Sender<TaskResponse>>>>>>
}


/// Implementation of chain handler
impl ChainHandler{

    /// Update the kwargs of the new task to the old task kwargs.
    ///
    /// # Arguments
    /// * `old_kw` - Old task kwargs
    /// * `new_kw` - New task kwargs
    fn update_kwargs(old_kw: HashMap<String, ArgType>, new_kw: HashMap<String, ArgType>) -> HashMap<String, ArgType>{
        let mut kw_update = new_kw.clone();
        for (k,v) in old_kw{
            kw_update.insert(k, v);
        }
        kw_update
    }

    /// Get the updated new task from the old task and updated variables.
    ///
    /// # Arguments
    /// * `config` - Application configuration
    /// * `next_task` - The next task
    /// * `parent_id` - Parent task id
    /// * `reply_to` - Key for the response
    /// * `kwargs` - Updated mapped arguments
    fn get_updated_task(
        config: CannonConfig<'static>,
        next_task: TaskConfig,
        parent_id: String,
        reply_to: String,
        kwargs: HashMap<String, ArgType>) -> TaskConfig{
        TaskConfig::new(
            config.clone(),
            next_task.get_broker(),
            next_task.get_task_name().clone(),
        next_task.get_exchange(),
        next_task.get_exchange_type(),
        next_task.get_routing_key(),
                Some(parent_id),
        Some(next_task.get_args()),
        Some(kwargs),
        Some(reply_to),
        Some(next_task.get_correlation_id().clone()),
        Some(next_task.get_result_expires().clone()),
        Some(next_task.get_priority()),
        Some(next_task.get_time_limit()),
        Some(next_task.get_soft_time_limit()),
        next_task.get_eta(),
        Some(next_task.get_retries()),
        Some(next_task.get_task_lang().clone()),
        Some(next_task.get_shadow()))
    }

    /// Update the task with the appropriate stream information. Returns the updated task
    /// and updated stream.
    ///
    /// # Arguments
    /// * `config` = Application configuration
    /// * `response` - The task response
    fn update_task(
        config: CannonConfig<'static>,
        response: TaskResponse) -> Option<(TaskConfig, StreamConfig)>{
        let r = response;
        let mut stream = r.get_stream_config().clone();
        let rmap = r.get_response().clone();
        let next_opt = stream.chain.pop();
        if next_opt.is_some(){
            let mut ochain = stream.chain;
            let next_config = next_opt.unwrap();
            if next_config.chain.is_empty() == false{
                let mut tchain = next_config.chain;
                tchain.append(&mut ochain);
                ochain = tchain;
            }
            let mut chord = next_config.chord;
            let next_task_opt = chord.pop();
            if next_task_opt.is_some(){
                let kwargs = rmap.get_map();
                let next_task = next_task_opt.unwrap();
                let up_kwargs = ChainHandler::update_kwargs(
                    kwargs, next_task.get_kwargs());
                let reply_to = r.get_task().get_reply_to().clone();
                let task_config = ChainHandler::get_updated_task(
                    config,
                next_task,
                r.get_task().get_parent_id(),
                reply_to,
                up_kwargs);
                let next_stream = StreamConfig::new(
                    Some(chord), Some(ochain));
                Some((task_config, next_stream))
            }else {
                None
            }
        }else{
            None
        }
    }

    /// Ensure that at least one broker started. Panic and terminate if one was not.
    fn check_brokers(&mut self){
        if self.rabbitmq_broker.is_none(){
            error!("No Brokers Created in Chain Handler");
            panic!("No brokers created in Chain Handler");
        }
    }

    /// Create a rabbitmq broker
    async fn create_rabbitmq_broker(&mut self, rt: Arc<Runtime>){
        let config = self.config.clone();
        let broker_result = RabbitMQBroker::new(
            config,self.routers.clone(), self.result_store.clone(), rt).await;
        if broker_result.is_ok() {
            let mut broker = broker_result.ok().unwrap();
            let is_setup = broker.setup().await;
            if is_setup.is_err() || is_setup.ok().unwrap() == false{
                error!("RabbitMQ Broker not setup in ChainHandler");
                panic!("RabbitMQ Broker not setup in ChainHandler");
            }else {
                self.rabbitmq_broker = Some(Arc::new(Mutex::new(broker)));
            }
        }else{
            error!("RabbitMQ Broker not setup in ChainHandler");
            panic!("RabbitMQ Broker not setup in ChainHandler")
        }
    }

    /// Setup the brokers
    async fn setup_brokers(&mut self, rt: Arc<Runtime>){
        for i in 0..self.brokers.len(){
            let broker = self.brokers.get(i).unwrap();
            match broker{
                BrokerType::RABBITMQ => {
                    self.create_rabbitmq_broker(rt.clone()).await;
                }
            }
        }
        self.check_brokers();
    }

    /// Send the updated task to rabbitmq
    ///
    /// # Arguments
    /// * `task` - The task configuration
    /// * `stream_config` - The stream config for hte application
    /// * `broker` - The broker arc containing the rabbitmq broker
    /// * `cfg` - The application configuration
    async fn send_to_rabbitmq(
        task: TaskConfig,
        stream_config: StreamConfig,
        mut broker: Option<Arc<Mutex<RabbitMQBroker>>>,
        cfg: CannonConfig<'static>){
        if broker.is_some(){
            if let Some(b) = broker.take() {
                let br = b.clone();
                let mut br_lock = br.lock().await;
                let r = br_lock.send(cfg.clone(), task.clone(), Some(stream_config)).await;
                drop(br_lock);
                if r.is_err() {
                    let task_string = serde_json::to_string(&task);
                    if task_string.is_ok() {
                        error!("Failed to send task to RabbitMQ in chain:\n{}", task_string.ok().unwrap());
                    } else {
                        error!("Failed to send task to RabbitMQ in chain:\n Serialization failed in err log");
                    }
                }
            }
        }else{
            warn!("RabbitMQ Broker Not Setup but required by Chain Handler");
        }
    }

    /// Start the chain handler
    ///
    /// # Arguments
    /// * `config` - Cannon configuration
    pub async fn start(&mut self, config: CannonConfig<'static>, rt: Arc<Runtime>){
        self.setup_brokers(rt).await;
        let recarc = self.receiver.clone();
        let status = self.status.clone();
        let rabbitmq_broker = self.rabbitmq_broker.clone();
        let handle = tokio::spawn(async move{
            while status.load(Ordering::Relaxed){
                let mut receiver_gaurd = recarc.lock().await;
                let response_opt = receiver_gaurd.recv().await;
                if response_opt.is_some(){
                    let response: TaskResponse = response_opt.unwrap();
                    let task_opt = ChainHandler::update_task(
                        config.clone(), response);
                    if task_opt.is_some(){
                        let (task, stream_config) = task_opt.unwrap();
                        if task.get_task_name().clone().eq("FAKE") == false {
                            let broker = task.get_broker().clone();
                            match broker.as_str() {
                                broker_type::RABBITMQ => {
                                    ChainHandler::send_to_rabbitmq(
                                        task,
                                        stream_config,
                                        rabbitmq_broker.clone(),
                                        config.clone()).await;
                                },
                                _ => {}
                            }
                        }
                    }
                }
            };
        });
        self.handle = Some(handle);
    }

    /// Close the Chord Handler and await for the future to terminate
    pub fn close(&mut self) -> Option<JoinHandle<()>>{
        self.status.store(false, Ordering::Relaxed);
        self.handle.take()
    }

    /// Creates a new chain handler from the supplied configuration
    ///
    /// # Arguments
    /// * `app_config` - Application config
    /// * `config` - Chain Handler Config
    /// * `result_store` - Result storage
    pub fn new(
        app_config: CannonConfig<'static>,
        config: ChainHandlerConfig,
        result_store: Arc<RwLock<HashMap<String, Arc<Mutex<Sender<TaskResponse>>>>>>) -> ChainHandler{
        ChainHandler {
            config: app_config,
            routers: config.routers,
            brokers: config.brokers,
            receiver: config.receiver,
            status: config.status.clone(),
            rabbitmq_broker: None,
            handle: None,
            result_store
        }
    }
}


#[cfg(test)]
pub mod tests{
    use std::collections::HashMap;
    use std::sync::Arc;
    use std::sync::atomic::AtomicBool;
    use std::thread;
    use std::time::Duration;

    use datacannon_rs_core::argparse::argtype::ArgType;
    use datacannon_rs_core::backend::config::BackendConfig;
    use datacannon_rs_core::broker::broker_type;
    use datacannon_rs_core::config::config::{BackendType, BrokerType, CannonConfig};
    use datacannon_rs_core::connection::amqp::connection_inf::AMQPConnectionInf;
    use datacannon_rs_core::connection::connection::ConnectionConfig;
    use datacannon_rs_core::message_protocol::stream::StreamConfig;
    use datacannon_rs_core::message_structure::amqp::exchange::Exchange;
    use datacannon_rs_core::message_structure::amqp::queue::AMQPQueue;
    use datacannon_rs_core::message_structure::queues::GenericQueue;
    use datacannon_rs_core::replication::rabbitmq::{RabbitMQHAPolicies, RabbitMQHAPolicy};
    use datacannon_rs_core::replication::replication::HAPolicy;
    use datacannon_rs_core::router::router::{Router, Routers};
    use datacannon_rs_core::task::config::TaskConfig;
    use datacannon_rs_core::task::result::{Response, TaskResponse};
    use lapin::ExchangeKind;
    use tokio::runtime::Runtime;
    use tokio::sync::{mpsc, Mutex, RwLock};

    use crate::chain::handler::{ChainHandler, ChainHandlerConfig};

    fn get_backend_config() -> BackendConfig{
        let bc = BackendConfig{
            url: "127.0.0.1:6379",
            username: None,
            password: None,
            transport_options: None
        };
        bc
    }

    fn get_routers() -> Routers{
        let mut rts = Routers::new();
        let mut queues = Vec::<GenericQueue>::new();
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            5272,
            Some("test".to_string()),
            Some("dev".to_string()),
            Some("rtp*4500".to_string()),
            1000);
        let q= AMQPQueue::new("lapin_test_queue_two".to_string(), Some("test_exchange".to_string()), Some("test_route".to_string()), 0, HAPolicy::RabbitMQ(RabbitMQHAPolicy::new(RabbitMQHAPolicies::ALL, 1)), true, amq_conf);
        queues.push(GenericQueue::AMQPQueue(q));
        let exch = Exchange::new("test_exchange".to_string(), ExchangeKind::Direct);
        let router = Router::new("test_key".to_string(),queues, Some(exch));
        rts.add_router("test_key".to_string(), router);
        rts
    }

    fn get_config<'a>() -> CannonConfig<'a>{
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some("dev".to_string()),
            Some("rtp*4500".to_string()),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = get_backend_config();
        let routers = get_routers();
        let mut cannon_conf = CannonConfig::new(conn_conf, BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 100;
        cannon_conf
    }

    pub fn get_runtime() -> Arc<Runtime>{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build();
        Arc::new(rt.unwrap())
    }

    fn get_test_task() -> TaskConfig{
        let config = get_config();
        TaskConfig::new(
            config,
            broker_type::RABBITMQ.to_string(),
            "test_it".to_string(),
            Some("test_exchange".to_string()),
            Some("DIRECT".to_string()),
            Some("test_key".to_string()),
            None,
            None,
            None,
            Some("test".to_string()),
            None,
            None,
            None,
            None,
            None,
            10000,
            Some(1),
            None,
            None)
    }

    fn get_chain_task() -> TaskConfig{
        let config = get_config();
        TaskConfig::new(
            config,
            broker_type::RABBITMQ.to_string(),
            "test_itb".to_string(),
            Some("test_exchange".to_string()),
            Some("DIRECT".to_string()),
            Some("test_key".to_string()),
            None,
            None,
            None,
            Some("testa".to_string()),
            None,
            None,
            None,
            None,
            None,
            10000,
            Some(1),
            None,
            None)
    }

    fn get_stream_config() -> StreamConfig{
        let chain_task = get_chain_task();
        let inner_stream_config = StreamConfig::new(
            Some(vec![chain_task]), None);
        StreamConfig::new(
            None, Some(vec![inner_stream_config]))
    }

    #[test]
    fn should_push_response_to_the_broker(){
        let rt = get_runtime();
        let new_rt = rt.clone();
        let h = rt.clone().spawn(async move {
            let app_config = get_config();
            let results = Arc::new(RwLock::new(HashMap::new()));
            let config = get_config();
            let status = Arc::new(AtomicBool::new(true));
            let (sender, receiver) = mpsc::unbounded_channel();
            let handler_config = ChainHandlerConfig {
                status: status,
                routers: config.routers.clone(),
                brokers: vec![BrokerType::RABBITMQ],
                receiver: Arc::new(Mutex::new(receiver))
            };
            let stream_config = get_stream_config();
            let task = get_test_task();
            let mut response = Response::new();
            response.add_result("test_key".to_string(), ArgType::Bool(true));
            let mut handler = ChainHandler::new(
                app_config, handler_config, results.clone());
            let response = TaskResponse::new(
                task, response, stream_config, true, None);
            handler.start(config, rt).await;
            let _r = sender.send(response.clone());
            handler.close();
            thread::sleep(Duration::from_millis(1000));
            let _r = sender.send(response);
        });
        new_rt.block_on(async move{
           let _r = h.await;
        });
    }
}

//! Redis backend handler that pushes responses from the threadpool to redis. The handler
//! receives responses from a threadpool or consumer if the task cannot be found.
//!
//! The handler places the response in the reply_to key. A certain number of failures are allowed
//! before a panic.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use datacannon_rs_core::backend::config::BackendConfig;
use datacannon_rs_core::backend::redis::producer::{self, RedisProducer};
use datacannon_rs_core::broker::broker_type;
use datacannon_rs_core::config::config::CannonConfig;
use datacannon_rs_core::message_protocol::stream::StreamConfig;
use datacannon_rs_core::task::config::TaskConfig;
use datacannon_rs_core::task::result::{Response, TaskResponse};
use tokio::sync::{mpsc, Mutex};
use tokio::sync::Semaphore;
use tokio::task::JoinHandle;

/// Redis Backend Configuration Class
#[derive(Clone, Debug)]
pub struct RedisHandlerConfig{
    pub backend_config: BackendConfig,
    pub max_failures: u8,
    pub num_producers: u8,
    pub semaphore: Arc<Semaphore>,
    pub status: Arc<AtomicBool>,
    pub receiver: Arc<Mutex<mpsc::UnboundedReceiver<TaskResponse>>>,
    pub sender: mpsc::UnboundedSender<TaskResponse>,
    pub chord_sender: mpsc::UnboundedSender<TaskResponse>,
    pub chain_sender: mpsc::UnboundedSender<TaskResponse>
}


/// The redis handler structure
pub struct RedisHandler{
    app_config: CannonConfig<'static>,
    backend_config: BackendConfig,
    max_failures: u8,
    num_producers: u8,
    semaphore: Arc<Semaphore>,
    status: Arc<AtomicBool>,
    receiver: Arc<Mutex<mpsc::UnboundedReceiver<TaskResponse>>>,
    sender: mpsc::UnboundedSender<TaskResponse>,
    producer_handles: Vec<(JoinHandle<()>, Arc<AtomicBool>)>,
    chord_sender: mpsc::UnboundedSender<TaskResponse>,
    chain_sender: mpsc::UnboundedSender<TaskResponse>
}


impl RedisHandler{

    /// Runs the producer in the runtime loop. Sets the producer status to false ot indicate the
    /// producer exited. This is required as join handles cannot be polled for whether they are
    /// alive.
    ///
    /// Permits are incremented only when a message succeeds or the maximum failure count is not
    /// exceeded for the producer.
    ///
    /// # Arguments
    /// * `backend_config` - The backend configuration
    /// * `semaphore` - Tokio sempahore controlling caching and messaging
    /// * `status` - Application status for shutdown
    /// * `receiver` - The Unbounded Receiver
    /// * `max_failures` - Maximum allowed send failures
    /// * `producer_status` - Status of the producer
    async fn run_producer(backend_config: BackendConfig,
                          sender: mpsc::UnboundedSender<TaskResponse>,
                          semaphore: Arc<Semaphore>,
                          status: Arc<AtomicBool>,
                          receiver: Arc<Mutex<mpsc::UnboundedReceiver<TaskResponse>>>,
                          max_failures: u8,
                          producer_status: Arc<AtomicBool>,
                          chord_sender: mpsc::UnboundedSender<TaskResponse>,
                          chain_sender: mpsc::UnboundedSender<TaskResponse>){
        let mut failures = 0;
        let redis_producer = RedisProducer::new(backend_config).await;
        let conn = redis_producer.get_paired_conn();
        while status.load(Ordering::Relaxed) && producer_status.load(Ordering::Relaxed){
            let mut receiver_guard = receiver.lock().await;
            let task_response_opt = receiver_guard.recv().await;
            if task_response_opt.is_some() {
                let task_response: TaskResponse = task_response_opt.unwrap();
                if task_response.get_stream_config().chord.is_empty() {
                    if task_response.get_stream_config().chain.is_empty() {
                        let mut fail_response = task_response.clone();
                        let tconfig = task_response.get_task();
                        let task_name = tconfig.get_task_name();
                        if task_name.eq("fake_result") == false {
                            let route = task_response.get_task().get_reply_to().clone();
                            let val_r = serde_json::to_string(&task_response);
                            if val_r.is_ok() {
                                let val = val_r.ok().unwrap();
                                let r = producer::publish(conn.clone(), Some(3), Some(100), route, val.clone()).await;
                                if r.0 == false {
                                    fail_response.decrement_retries();
                                    if fail_response.get_remaining_retries() > 0 {
                                        let _r = sender.send(fail_response);
                                    } else {
                                        error!("905 - Dropping Message after too many Retries")
                                    }
                                    error!("903 - Failed to Publish Message in Redis Producer\n{}", val);
                                    failures += 1;
                                    if failures > max_failures {
                                        error!("902 - Too Many Failures in Redis Handler. Terminating Producer!");
                                        producer_status.store(false, Ordering::Relaxed);
                                    }
                                } else {
                                    semaphore.add_permits(1);
                                }
                            } else {
                                error!("901 - Failed to Serialize Task Response in Redis Handler. Consider using base64.");
                                failures += 1;
                                if failures > max_failures {
                                    error!("902 - Too Many Failures in Redis Handler. Terminating Producer!");
                                    producer_status.store(false, Ordering::Relaxed);
                                } else {
                                    semaphore.add_permits(1);
                                }
                            }
                        }
                    }else{
                        let r = chain_sender.send(task_response);
                        if r.is_err(){
                            error!("904 - Failed to Send Chain from Redis Handler");
                            failures += 1;
                            if failures > max_failures{
                                error!("902 - Too Many Failures ni Redis Handler. Terminating Producer!");
                                producer_status.store(false, Ordering::Relaxed);
                            }
                        }
                        semaphore.add_permits(1);
                    }
                }else{
                    let r = chord_sender.send(task_response);
                    if r.is_err(){
                        error!("903 - Failed to Send Chord from Redis Handler");
                        failures += 1;
                        if failures > max_failures{
                            error!("902 - Too Many Failures in Redis Handler. Terminating Producer!");
                            producer_status.store(false, Ordering::Relaxed);
                        }else {
                            semaphore.add_permits(1);
                        }
                    }
                }
            }
        }
    }

    /// Obtain a fake task response to force the loop to close
    ///
    /// # Arguments
    /// * `config` - Application configuration
    fn get_fake_task_response(config: CannonConfig<'static>) -> TaskResponse{
        let task = TaskConfig::new(
            config,
            broker_type::RABBITMQ.to_string(),
            "test_it".to_string(),
            Some("test_exchange".to_string()),
            Some("DIRECT".to_string()),
            Some("test_key".to_string()),
            Some("test_parent_id".to_string()),
            None,
            None,
            Some("test_reply".to_string()),
            None,
            None,
            None,
            None,
            None,
            10000,
            Some(1),
            None,
            None);
        let stream_config = StreamConfig::new(None, None);
        let response =  Response::new();
        TaskResponse::new(task, response, stream_config, false, None)
    }

    /// Force the loop closure
    ///
    /// # Arguments
    /// * `sender` - Unbounded Sender for sending task responses
    /// * `config` - Application config
    fn force_loop_closure(sender: mpsc::UnboundedSender<TaskResponse>, config: CannonConfig<'static>){
        let response = RedisHandler::get_fake_task_response(config);
        let _r = sender.send(response);
    }

    /// Stop the backend producer
    pub async fn close(self){
        let config = self.app_config.clone();
        self.status.store(false, Ordering::Relaxed);
        let handles = self.producer_handles;
        let sender = self.sender.clone();
        for (_handle, arc) in &handles{
            arc.store(false, Ordering::Relaxed);
            RedisHandler::force_loop_closure(sender.clone(), config.clone());
        }
        for (handle, _arc) in handles{
            let _r = handle.await;
        }
    }

    /// Actually creates a single producer.
    pub async fn create_producer(&mut self){
        let chain_sender = self.chain_sender.clone();
        let sender = self.sender.clone();
        let sema = self.semaphore.clone();
        let config = self.backend_config.clone();
        let status = self.status.clone();
        let receiver = self.receiver.clone();
        let max_failures = self.max_failures.clone();
        let producer_arc = Arc::new(AtomicBool::new(true));
        let spawn_arc = producer_arc.clone();
        let csender = self.chord_sender.clone();
        let h = tokio::spawn(async move{
            RedisHandler::run_producer(
                config.clone(),
                sender,
                sema.clone(),
                status.clone(),
                receiver.clone(),
                max_failures.clone(),
                spawn_arc,
                csender.clone(),
                chain_sender).await;
        });
        self.producer_handles.push((h, producer_arc));
    }

    /// Start the redis producer. Create the producers and add the handle to the producer
    /// handles. Stores a handle and atomic boolean of the running status in an array for
    /// further access.
    pub async fn start(&mut self){
        for _i in 0..self.num_producers{
            self.create_producer().await;
        }
    }

    /// Remove dead producers from the producer handle array.
    ///
    /// # Arguments
    /// * `indices` - Indices of dead producers
    fn remove_dead_producers(&mut self, indices: Vec<usize>) -> usize{
        let idx_len = indices.len().clone();
        if indices.len() > 0{
            let mut i = indices.len() -1;
            while i >= 0 as usize {
                let idx = indices.get(i).unwrap().clone();
                let (handle, status) = self.producer_handles.remove(idx);
                drop(handle);
                drop(status);
                i -= 1;
            }
        }
        idx_len
    }

    /// Find and remove dead producers, return the number of producers to create
    fn find_and_remove_dead_producers(&mut self) -> usize {
        let mut remove_indices = Vec::<usize>::new();
        let handles = &mut self.producer_handles;
        for i in 0..handles.len(){
            let (_handle, status) = handles.get(i).unwrap();
            if status.load(Ordering::Relaxed) == false{
                remove_indices.push(i);
            }
        }
        self.remove_dead_producers(remove_indices)
    }

    /// Checks and restarts producers. This should be done periodically or the entire process
    /// will stall. Failed producers are not counted as something else could be amiss.
    pub async fn check_and_restart_producers(&mut self){
        let num_producers_to_create = self.find_and_remove_dead_producers();
        for _i in 0..num_producers_to_create {
            self.create_producer().await;
        }
    }

    /// Create a new Redis Handler
    ///
    /// # Arguments
    /// * `app_config` - Application Config
    /// * `config` - Configuration for the RedisHandler
    pub fn new(app_config: CannonConfig<'static>, config: RedisHandlerConfig) -> RedisHandler{
        RedisHandler{
            app_config,
            backend_config: config.backend_config.clone(),
            max_failures: config.max_failures.clone(),
            num_producers: config.num_producers.clone(),
            semaphore: config.semaphore.clone(),
            status: config.status.clone(),
            receiver: config.receiver.clone(),
            sender: config.sender.clone(),
            producer_handles: vec![],
            chord_sender: config.chord_sender.clone(),
            chain_sender: config.chain_sender.clone()
        }
    }
}


#[cfg(test)]
pub mod tests{
    use std::env;
    use std::sync::Arc;
    use std::sync::atomic::AtomicBool;

    use datacannon_rs_core::backend::config::BackendConfig;
    use datacannon_rs_core::broker::broker_type;
    use datacannon_rs_core::config::config::{BackendType, CannonConfig};
    use datacannon_rs_core::connection::amqp::connection_inf::AMQPConnectionInf;
    use datacannon_rs_core::connection::connection::ConnectionConfig;
    use datacannon_rs_core::message_protocol::stream::StreamConfig;
    use datacannon_rs_core::router::router::Routers;
    use datacannon_rs_core::task::config::TaskConfig;
    use datacannon_rs_core::task::result::{Response, TaskResponse};
    use futures::StreamExt;
    use redis_async::client;
    use redis_async::resp::FromResp;
    use tokio::sync::{mpsc, Mutex, Semaphore};

    use crate::backend::redis::{RedisHandler, RedisHandlerConfig};
    use crate::chain::handler::tests::get_runtime;

    fn get_backend_config() -> BackendConfig{
        let bc = BackendConfig{
            url: "127.0.0.1:6379",
            username: None,
            password: None,
            transport_options: None
        };
        bc
    }

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user").ok().unwrap();
        let pwd = env::var("rabbit_test_pwd").ok().unwrap();
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user),
            Some(pwd),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = get_backend_config();
        let routers = Routers::new();
        let mut cannon_conf = CannonConfig::new(conn_conf, BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 100;
        cannon_conf
    }


    fn get_empty_semaphore_handler_config() -> RedisHandlerConfig{
        let (sender, receiver) = mpsc::unbounded_channel::<TaskResponse>();
        let (csender, _creceiver) = mpsc::unbounded_channel::<TaskResponse>();
        let (chsender, _chreceiver) = mpsc::unbounded_channel::<TaskResponse>();
        RedisHandlerConfig{
            backend_config: get_backend_config(),
            max_failures: 1,
            num_producers: 2,
            semaphore: Arc::new(Semaphore::new(0)),
            status: Arc::new(AtomicBool::new(true)),
            receiver: Arc::new(Mutex::new(receiver)),
            sender: sender,
            chord_sender: csender,
            chain_sender: chsender
        }
    }

    fn get_handler_config() -> RedisHandlerConfig{
        let (sender, receiver) = mpsc::unbounded_channel::<TaskResponse>();
        let (csender, _creceiver) = mpsc::unbounded_channel::<TaskResponse>();
        let (chsender, _chreceiver) = mpsc::unbounded_channel::<TaskResponse>();
        RedisHandlerConfig{
            backend_config: get_backend_config(),
            max_failures: 1,
            num_producers: 2,
            semaphore: Arc::new(Semaphore::new(2)),
            status: Arc::new(AtomicBool::new(true)),
            receiver: Arc::new(Mutex::new(receiver)),
            sender: sender,
            chord_sender: csender,
            chain_sender: chsender
        }
    }

    fn get_task_response() -> TaskResponse{
        let config = get_config();
        let cfg = TaskConfig::new(
            config,
            broker_type::RABBITMQ.to_string(),
            "test_task".to_string(),
            None,
            None,
            Some("test_route".to_string()),
            None,
            None,
            None,
            Some("test_reply".to_string()),
            Some("abs".to_string()),
            None,
            None,
            None,
            None,
            10000,
            None,
            None,
            None);
        let response = Response::new();
        let stream_config = StreamConfig::new(None,None);
        TaskResponse::new(cfg, response, stream_config, true, None)
    }

    #[test]
    fn should_create_redis_handler(){
        let cfg = get_config();
        let rt = get_runtime();
        let h = rt.spawn(async move {
            let redis_cfg = get_handler_config();
            let mut handler = RedisHandler::new(cfg, redis_cfg);
            handler.start().await;
            handler.close().await;
        });
        rt.block_on(async move{
            let _r = h.await;
        })
    }

    #[test]
    fn should_send_response_to_redis() {
        let cfg = get_config();
        let rt = get_runtime();
        let redis_cfg = get_handler_config();
        let mut handler = RedisHandler::new(cfg, redis_cfg.clone());
        let response = get_task_response();
        let sender = redis_cfg.sender.clone();
        let handle = rt.spawn(async move{
            handler.start().await;
            let addr = redis_cfg.backend_config.url;
            let addr_socket = addr.parse().unwrap();
            let ps_connr = client::pubsub_connect(&addr_socket).await;
            assert!(ps_connr.is_ok());
            let ps_conn = ps_connr.ok().unwrap();
            let ps_conn2 = ps_conn.clone();
            let _h = tokio::spawn(async move {
                let msgs = ps_conn2.subscribe("test_reply").await;
                assert!(msgs.is_ok());
                let mut msg_stream = msgs.ok().unwrap();
                if let Some(message) = msg_stream.next().await {
                    match message {
                        Ok(message) => {
                            let mstr = String::from_resp(message).unwrap();
                            println!("{}", mstr);
                            let task_deser = serde_json::from_str(mstr.as_str());
                            assert!(task_deser.is_ok());
                            let task_response: TaskResponse = task_deser.ok().unwrap();
                            assert!(task_response.get_task().get_task_name().eq("test_task"));
                        },
                        Err(e) => {
                            eprintln!("ERROR: {}", e);
                            assert!(false);
                        }
                    }
                } else {
                    assert!(false);
                }
            });
            let _r = sender.send(response);
            handler.close().await;
        });
        rt.block_on(async move{
            let _r = handle.await;
        })
    }


    #[test]
    fn should_increment_semaphore(){
        let cfg = get_config();
        let rt = get_runtime();
        let redis_cfg = get_empty_semaphore_handler_config();
        let sema = redis_cfg.semaphore.clone();
        let task_sender = redis_cfg.sender.clone();
        let task_response = get_task_response();
        let mut handler = RedisHandler::new(cfg,redis_cfg);
        rt.block_on(async move {
            handler.start().await;
            let _r = task_sender.send(task_response);
            let permit = sema.acquire().await.unwrap();
            permit.forget();
            handler.close().await;
        });
    }

    #[test]
    fn should_find_and_remove_dead_producers(){
        let cfg = get_config();
        let rt = get_runtime();
        let handle = rt.spawn(async move {
            let redis_cfg = get_handler_config();
            let mut handler = RedisHandler::new(cfg, redis_cfg.clone());
            handler.start().await;
            let r = handler.find_and_remove_dead_producers();
            assert!(r == 0);
            handler.close().await;
        });
        rt.block_on(async move{
            let _r = handle.await;
        })
    }
}

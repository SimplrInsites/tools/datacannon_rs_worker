//! Contains two structures. A backend types enum storing a backend config for each available
//! backend to pass to the application by the user. A backend types enum storing instantiated
//!
//! ---
//! author: Andrew Evans
//! ---

use crate::backend::redis::RedisHandler;

/// Enum for instantiated backend
pub enum Backend{
    Redis(RedisHandler)
}


//! A chord handler which takes a response and sends it using hte available receivers.
//! Each backend needs to pass tasks with existing chords to this handler. The handler
//! checks the existing keys and sends a new task runner to the appropriate threadpool.
//!
//! Chords execute on the same node. Chains submit new tasks to the broker. Use chords
//! if tasks run quickly, cannot be placed in the same task, and will benefit from running
//! on the same route. The semaphore will not be updated to allow for the consumer to
//! retrieve new tasks when sending to the chord handler.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::{HashMap, HashSet};
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use datacannon_rs_core::argparse::argtype::ArgType;
use datacannon_rs_core::broker::broker_type;
use datacannon_rs_core::config::config::CannonConfig;
use datacannon_rs_core::message_protocol::stream::StreamConfig;
use datacannon_rs_core::task::config::TaskConfig;
use datacannon_rs_core::task::result::TaskResponse;
use tokio::sync::{mpsc, Mutex, Semaphore};
use tokio::task::JoinHandle;

use crate::task::registry::{self, Registry};

/// Deconstructed variables for executing tasks
struct DeconstructedTaskVars{
    args: Vec<ArgType>,
    kwargs: HashMap<String, ArgType>,
    stream_config: StreamConfig,
    task: TaskConfig
}

/// Chord handler configuration
pub struct ChordHandlerConfig{
    pub semaphore: Arc<Semaphore>,
    pub io_task_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
    pub cpu_task_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
    pub custom_task_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
    pub status: Arc<AtomicBool>,
    pub receiver: Arc<Mutex<mpsc::UnboundedReceiver<TaskResponse>>>
}


/// The existing chord handler
pub struct ChordHandler{
    config: CannonConfig<'static>,
    sema: Arc<Semaphore>,
    io_task_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
    cpu_task_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
    custom_task_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
    receiver: Arc<Mutex<mpsc::UnboundedReceiver<TaskResponse>>>,
    status: Arc<AtomicBool>,
    handle: Option<JoinHandle<()>>,
    registry: Registry
}


/// Chord handler implementation
impl ChordHandler{

    /// Merge old and new task configurations
    ///
    /// # Arguments
    /// * `old` - Task configuration from the old message
    /// * `new` - Task configuration from the new message
    /// * `config` - Application configuration
    fn merge_tasks(old: TaskConfig, new: TaskConfig, config: CannonConfig<'static>) -> TaskConfig{
        TaskConfig::new(
            config,
            broker_type::RABBITMQ.to_string(),
            new.get_task_name().clone(),
            new.get_exchange(),
            new.get_exchange_type(),
            new.get_routing_key(),
                    Some(old.get_parent_id()),
            Some(new.get_args()),
            Some(new.get_kwargs()),
            Some(old.get_reply_to().clone()),
            Some(new.get_correlation_id().clone()),
            Some(new.get_result_expires()),
            Some(new.get_priority()),
            Some(new.get_time_limit()),
            Some(new.get_soft_time_limit()),
            new.get_eta(),
            Some(new.get_retries()),
            Some(new.get_task_lang().clone()),
            Some(new.get_shadow())
        )
    }

    /// Create the task runner from the task response
    ///
    /// # Arguments
    /// * `response` - The task response to handle
    /// * `config` - Application config
    fn get_updated_vars(
        response: TaskResponse, config: CannonConfig<'static>) -> Option<DeconstructedTaskVars>{
        let r = response;
        let mut sc = r.get_stream_config().clone();
        let old_task = r.get_task();
        let task_opt = sc.chord.pop();
        if task_opt.is_some(){
            let new_task = task_opt.unwrap();
            let config = ChordHandler::merge_tasks(
                old_task.clone(), new_task, config);
            let args = config.get_args();
            let mut kwargs = config.get_kwargs();
            let thread_response = r.get_response().clone();
            let response_map = thread_response.get_map();
            for (k, v) in response_map{
                kwargs.insert(k, v);
            }
            let vars = DeconstructedTaskVars{
                args,
                kwargs,
                stream_config: sc,
                task: config
            };
            Some(vars)
        }else{
            None
        }
    }

    /// Push a task to the appropriate threadpool
    ///
    /// # Arguments
    /// * `vars` - Updated task variables after processing from the channel
    /// * `io_keys` - IO related task keys
    /// * `cpu_keys` - CPU realated task keys
    /// * `custom_keys` Keys for tasks in the custom pool
    /// * `io_sender` - Channel for sending io tasks
    /// * `cpu_sender` - Channel for sending cpu tasks
    /// * `custom_sender` - Channel for sending custom tasks
    fn push_task(
        vars: DeconstructedTaskVars,
        io_keys: HashSet<String>,
        cpu_keys: HashSet<String>,
        custom_keys: HashSet<String>,
        io_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
        cpu_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
        custom_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>) -> Result<bool, ()>{
        let v = vars;
        let args = v.args;
        let kwargs = v.kwargs;
        let cfg = v.task;
        let stream_config = v.stream_config;
        let mut found_task = false;
        let task_name = cfg.get_task_name().clone();
        if io_keys.contains(&task_name){
            if io_sender.is_some() {
                let _r = io_sender.clone().unwrap().send(
                    (args.clone(), kwargs.clone(), cfg.clone(), stream_config.clone()));
                found_task = true;
            }else{
                error!("IO Sender Not specified with IO Task");
            }
        }
        if cpu_keys.contains(&task_name){
            if cpu_sender.is_some() {
                let _r = cpu_sender.clone().unwrap().send(
                    (args.clone(), kwargs.clone(), cfg.clone(), stream_config.clone()));
                found_task = true;
            }else{
                error!("CPU Sender Not Specified with CPU Bound Task")
            }
        }
        if custom_keys.contains(&task_name) {
            if custom_sender.is_some() {
                let _r = custom_sender.clone().unwrap().send(
                    (args.clone(), kwargs.clone(), cfg.clone(), stream_config.clone()));
                found_task = true;
            }else{
                error!("Custom Sender Not Specified with Custom Task")
            }
        }
        if !found_task{
            let e = format!("409- Task Key Not Found [{}]", task_name);
            error!("{}", e);
        }
        Ok(found_task)
    }

    /// Create a loop that handles tasks sent to the receiver
    ///
    ///
    /// # Arguments
    /// * `status` - The status of the application for shutting down the handler
    /// * `receiver` - The Unbounded receiver for receiving tasks
    /// * `io_keys` - IO related task keys
    /// * `cpu_keys` - CPU realated task keys
    /// * `custom_keys` Keys for tasks in the custom pool
    /// * `io_sender` - Channel for sending io tasks
    /// * `cpu_sender` - Channel for sending cpu tasks
    /// * `custom_sender` - Channel for sending custom tasks
    async fn handle_tasks(
        config: CannonConfig<'static>,
        sema: Arc<Semaphore>,
        status: Arc<AtomicBool>,
        receiver: Arc<Mutex<mpsc::UnboundedReceiver<TaskResponse>>>,
        io_keys: HashSet<String>,
        cpu_keys: HashSet<String>,
        custom_keys: HashSet<String>,
        io_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
        cpu_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
        custom_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>){
        while status.load(Ordering::Relaxed){
            let mut receiver_guard = receiver.lock().await;
            let receive_result = receiver_guard.recv().await;
            if receive_result.is_some(){
                let response: TaskResponse = receive_result.unwrap();
                let task_name = response.get_task().get_task_name();
                if task_name.eq("FAKE") == false {
                    let vars_opt = ChordHandler::get_updated_vars(
                        response, config.clone());
                    if vars_opt.is_some() {
                        let vars = vars_opt.unwrap();
                        let is_found = ChordHandler::push_task(
                            vars,
                            io_keys.clone(),
                            cpu_keys.clone(),
                            custom_keys.clone(),
                            io_sender.clone(),
                            cpu_sender.clone(),
                            custom_sender.clone());
                        if is_found.is_err() || is_found.ok().unwrap() == false {
                            sema.add_permits(1);
                        }
                    } else {
                        sema.add_permits(1);
                    }
                }
            }
        }
    }

    /// Close the Chord Handler and await for the future to terminate
    pub fn close(&mut self) -> Option<JoinHandle<()>>{
        self.status.store(false, Ordering::Relaxed);
        self.handle.take()
    }

    /// Start the event handler
    pub async fn start(&mut self) {
        let registry = self.registry.clone();
        let io_keys = registry::get_io_keys(
            registry.io_registry.clone()).await;
        let cpu_keys = registry::get_cpu_keys(
            registry.cpu_registry.clone());
        let custom_keys = registry::get_custom_keys(
            registry.custom_registry);
        let config = self.config.clone();
        let sema = self.sema.clone();
        let status = self.status.clone();
        let receiver = self.receiver.clone();
        let io_sender = self.io_task_sender.clone();
        let cpu_sender = self.cpu_task_sender.clone();
        let custom_sender = self.custom_task_sender.clone();
        let handle = tokio::spawn(async move {
            ChordHandler::handle_tasks(
                config,
                sema,
                status,
                receiver,
                io_keys,
                cpu_keys,
                custom_keys,
                io_sender,
                cpu_sender,
                custom_sender).await;
        });
        self.handle = Some(handle);
    }

    /// Creates a new chord handler
    ///
    /// # Arguments
    /// * `registry` - Task registry
    /// * `app_config` - Application configuration
    /// * `config` - The chrod handler configuration
    pub fn new(
        registry: Registry,
        app_config: CannonConfig<'static>,
        config: ChordHandlerConfig)-> ChordHandler{
        ChordHandler{
            config: app_config,
            sema: config.semaphore,
            io_task_sender: config.io_task_sender.clone(),
            cpu_task_sender: config.cpu_task_sender.clone(),
            custom_task_sender: config.custom_task_sender.clone(),
            receiver: config.receiver,
            status: config.status.clone(),
            handle: None,
            registry
        }
    }
}

#[cfg(test)]
pub mod tests{
    use std::collections::HashMap;
    use std::env;
    use std::sync::{Arc, RwLock};
    use std::sync::atomic::AtomicBool;

    use datacannon_rs_core::argparse::argtype::ArgType;
    use datacannon_rs_core::backend::config::BackendConfig;
    use datacannon_rs_core::broker::broker_type;
    use datacannon_rs_core::config::config::{BackendType, CannonConfig};
    use datacannon_rs_core::connection::amqp::connection_inf::AMQPConnectionInf;
    use datacannon_rs_core::connection::connection::ConnectionConfig;
    use datacannon_rs_core::message_protocol::stream::StreamConfig;
    use datacannon_rs_core::router::router::Routers;
    use datacannon_rs_core::task::config::TaskConfig;
    use datacannon_rs_core::task::context::TaskContext;
    use datacannon_rs_core::task::result::{Response, TaskResponse};
    use tokio::runtime::Runtime;
    use tokio::sync::{mpsc, Mutex, RwLock as TokioLock, Semaphore};

    use crate::chord::handler::{ChordHandler, ChordHandlerConfig};
    use crate::task::registry::Registry;
    use crate::task::wrapper::Task;

    fn get_backend_config() -> BackendConfig{
        let bc = BackendConfig{
            url: "127.0.0.1:6379",
            username: None,
            password: None,
            transport_options: None
        };
        bc
    }

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user").ok().unwrap();
        let pwd = env::var("rabbit_test_pwd").ok().unwrap();
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user),
            Some(pwd),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = get_backend_config();
        let routers = Routers::new();
        let mut cannon_conf = CannonConfig::new(conn_conf, BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 100;
        cannon_conf
    }

    /// Get the application runtime
    fn get_runtime() -> Arc<Runtime>{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build();
        Arc::new(rt.unwrap())
    }

    fn test_it(_ctx: TaskContext) -> Result<Response, String>{
        for _i in 0..1000 {
            //big test string
            let lstr = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

            // really big shitty ((n^2)/2)m regex
            //let re = Regex::new(r"L.*").unwrap();
            //re.find_iter(lstr);

            //really small max nm loop
            let f = lstr.find("industry");
            if f.is_some() {
                let idx = f.unwrap();
                let splarr = lstr.split_at(idx);
                let _r = splarr.0.replace("Lorem", "Lore");
            }
        }
        let mut r = Response::new();
        r.add_result("test_result".to_string(), ArgType::Bool(true));
        Ok(r)
    }

    fn test_itb(ctx: TaskContext) -> Result<Response, String>{
        let mut c = ctx;
        for _i in 0..1000 {
            //big test string
            let lstr = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

            // really big shitty ((n^2)/2)m regex
            //let re = Regex::new(r"L.*").unwrap();
            //re.find_iter(lstr);

            //really small max nm loop
            let f = lstr.find("industry");
            if f.is_some() {
                let idx = f.unwrap();
                let splarr = lstr.split_at(idx);
                let _r = splarr.0.replace("Lorem", "Lore");
            }
        }
        let mut r = Response::new();
        let kwargs = c.get_kwargs();
        if kwargs.is_some() {
            for (k, v) in kwargs.unwrap() {
                r.add_result(k, v);
            }
        }
        r.add_result("test_resultb".to_string(), ArgType::Bool(true));
        Ok(r)
    }

    fn get_registry() -> Registry{
        let task = Task{
            f: test_it
        };
        let taskb = Task{
            f: test_itb
        };
        let mut mp = HashMap::new();
        mp.insert("test_it", task);
        mp.insert("test_itb", taskb);
        let mp_arc = Arc::new(RwLock::new(mp));
        Registry{
            io_registry: Arc::new(TokioLock::new(HashMap::new())),
            cpu_registry: mp_arc,
            custom_registry: Arc::new(RwLock::new(HashMap::new()))
        }
    }

    fn get_test_task() -> TaskConfig{
        let cfg = get_config();
        TaskConfig::new(
            cfg,
            broker_type::RABBITMQ.to_string(),
            "test_it".to_string(),
            None,
            None,
            None,
            None,
            None,
            None,
            Some("test".to_string()),
            None,
            None,
            None,
            None,
            None,
            10000,
            Some(1),
            None,
            None)
    }

    fn get_chord_task() -> TaskConfig{
        let cfg = get_config();
        TaskConfig::new(
            cfg,
            broker_type::RABBITMQ.to_string(),
            "test_itb".to_string(),
            None,
            None,
            None,
            None,
            None,
            None,
            Some("testa".to_string()),
            None,
            None,
            None,
            None,
            None,
            10000,
            Some(1),
            None,
            None)
    }

    fn get_stream_config() -> StreamConfig{
        let stream_task = get_chord_task();
        StreamConfig::new(Some(vec![stream_task]), None)
    }

    fn get_chord_handler_config() -> (ChordHandlerConfig, mpsc::UnboundedSender<TaskResponse>, mpsc::UnboundedReceiver<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>){
        let (sender, receiver) = mpsc::unbounded_channel();
        let sema = Arc::new(Semaphore::new(10));
        let app_status = Arc::new(AtomicBool::new(true));
        let (hsender, hreceiver) = mpsc::unbounded_channel();
        let handler = ChordHandlerConfig{
            semaphore: sema,
            io_task_sender: None,
            cpu_task_sender: Some(sender),
            custom_task_sender: None,
            status: app_status,
            receiver: Arc::new(Mutex::new(hreceiver))
        };
        (handler, hsender, receiver)
    }

    #[test]
    fn should_complete_task_chord(){
        let registry = get_registry();
        let rt = get_runtime();
        let app_config = get_config();
        let (config, sender, mut receiver) = get_chord_handler_config();
        let mut handler = ChordHandler::new(
            registry, app_config, config);
        rt.block_on(async move {
            handler.start().await;
            let task = get_test_task();
            let mut response = Response::new();
            let stream = get_stream_config();
            response.add_result("hello".to_string(), ArgType::String("test".to_string()));
            let response = TaskResponse::new(task, response, stream, true, None);
            let sr = sender.send(response);
            assert!(sr.is_ok());
            let r = receiver.recv().await;
            assert!(r.is_some());
            let (args, kwargs, task, stream): (Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig) = r.unwrap();
            assert!(kwargs.get("hello").is_some());
            assert!(task.get_task_name().eq("test_itb"));
            assert!(task.get_reply_to().eq("test"));
            assert_eq!(stream.chord.len(), 0);
            assert_eq!(args.len(), 0);
        });
    }
}

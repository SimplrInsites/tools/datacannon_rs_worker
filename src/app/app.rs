//! Worker side application. The applicaion uses a tokio loop for networking and a cpu-bound pool
//! to process workloads. Each application maintains access to a cpu-based future pool. A builder
//! sets the correct configuration before starting. Call build, set the correct tasks in the app,
//! and, when ready, run start. Application components run in the runtime. Set an appropriate number
//! of threads to allow for handling messages and tasks as data runs from consumer to threadpool
//! to Backend. Most failures will terminate the application. However, if a producer fails, this
//! will not be the case do  to some complexities.
//!
//! Note that the IO Pool runs on the runtime as well.
//!
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;
use std::io::Write;
use std::panic;
use std::sync::{Arc, Condvar, Mutex};
use std::sync::atomic::AtomicBool;
use std::sync::RwLock as Lock;

use chrono::Local;
use crossbeam::channel;
use datacannon_rs_core::argparse::argtype::ArgType;
use datacannon_rs_core::backend::config::BackendConfig;
use datacannon_rs_core::backend::redis::producer;
use datacannon_rs_core::backend::redis::producer::RedisProducer;
use datacannon_rs_core::backend::types::AvailableBackend;
use datacannon_rs_core::broker::broker_type;
use datacannon_rs_core::config::config::{BrokerType, CannonConfig};
use datacannon_rs_core::events::redis_handler::RedisEventHandler;
use datacannon_rs_core::message_protocol::stream::StreamConfig;
use datacannon_rs_core::router::router::Routers;
use datacannon_rs_core::task::config::TaskConfig;
use datacannon_rs_core::task::result::{Response, TaskResponse};
use env_logger::Builder;
use log::{info, LevelFilter};
use tokio::runtime;
use tokio::sync::{self, mpsc, Mutex as TokioMutex, Notify, RwLock, Semaphore};
use tokio::sync::mpsc::Sender;
use tokio::task::JoinHandle;

use crate::app::config::GeneralConfig;
use crate::app::events;
use crate::app::threadpools::PoolStore;
use crate::backend::redis::{RedisHandler, RedisHandlerConfig};
use crate::backend::store::Backend;
use crate::chain::handler::{ChainHandler, ChainHandlerConfig};
use crate::chord::handler::{ChordHandler, ChordHandlerConfig};
use crate::consumer::consumer_types::ConsumerType;
use crate::consumer::rabbitmq::{RabbitMQConsumer, RabbitMQConsumerConfig};
use crate::consumer::store::Consumer;
use crate::task::registry::Registry;
use crate::task::wrapper::{FutureTask, Task};
use crate::threadpool::{cpu_pool, io_futures_pool, thread_pool};
use crate::threadpool::cpu_pool::{CPUPoolConfig, FuturesPoolConfig};
use crate::threadpool::io_futures_pool::{IOFuturesPoolConfig, IOPoolConfig};
use crate::threadpool::pool_types::{CustomThreadPoolWrapper, FuturesPoolWrapper, IOFuturesPoolWrapper, PoolType};
use crate::threadpool::thread_pool::{CustomPoolConfig, CustomThreadPoolConfig};
use tokio::runtime::Runtime;


/// Application builder for the tool. Contains a runtime builder and a set of functions that should
/// almost always be called but are optional. The builder grants access to the underlying runtime
/// constructor unlike in the `crate::app::app::App` which only contains a new function.
///
/// All functions return a mutable reference to the builder. The build function creates an instance
/// of `crate::app::app::App`
pub struct AppBuilder{
    name: String,
    config: CannonConfig<'static>,
    backends: Vec<AvailableBackend>,
    consumers: Vec<ConsumerType>,
    brokers: Vec<BrokerType>,
    pool_types: Vec<PoolType>,
    routers: Routers
}


/// App Builder implementation
impl AppBuilder {

    /// Application name
    ///
    /// # Arguments
    /// * `app_name` - The application name
    pub fn app_name(&mut self, name: String) -> &mut AppBuilder{
        self.name = name;
        self
    }

    /// Set the backends to be instantiated
    ///
    /// # Arguments
    /// * `backends` - A vector of backend types
    pub fn backends(&mut self, backends: Vec<AvailableBackend>) -> &mut AppBuilder{
        self.backends = backends;
        self
    }

    /// Set the consumers to be instantiated
    ///
    /// # Arguments
    /// * 'consumers' - A vector of consumer types
    pub fn consumers(&mut self, consumers: Vec<ConsumerType>) -> &mut AppBuilder{
        self.consumers = consumers;
        self
    }

    /// Set the threadpools to instantiate
    ///
    /// # Arguments
    /// * `pool_types` - Vector containing the types of threadpools to instantiate
    pub fn pool_types(&mut self, pool_types: Vec<PoolType>) -> &mut AppBuilder{
        self.pool_types = pool_types;
        self
    }

    /// Set the brokers
    ///
    /// * Arguments
    /// * `brokers` - The brokers for the chain handler
    pub fn brokers(&mut self, brokers: Vec<BrokerType>) -> &mut AppBuilder{
        self.brokers = brokers;
        self
    }

    /// Set the routers for the application
    ///
    /// # Arguments
    /// * `routers` - Routers for the application
    pub fn routers(&mut self, routers: Routers) -> &mut AppBuilder{
        self.routers = routers;
        self
    }

    /// Build the application and return an `crate::app::app::App` instance. Can override the name.
    /// If no application name is given, 'data_cannon_worker' is used instead.
    pub fn build(&mut self) -> App{
        let registry = Registry{
            io_registry: Arc::new(RwLock::new(HashMap::new())),
            cpu_registry: Arc::new(Lock::new(HashMap::new())),
            custom_registry: Arc::new(Lock::new(HashMap::new()))
        };
        let app_name = self.name.clone();
        App{
            name: app_name,
            config: self.config.clone(),
            backends: self.backends.clone(),
            consumers: self.consumers.clone(),
            brokers: self.brokers.clone(),
            event_handler: None,
            pool_types: self.pool_types.clone(),
            available_backends: vec![],
            available_consumers: vec![],
            general_config: None,
            pool_store: PoolStore{
                io_wrapper: None,
                future_wrapper: None,
                custom_wrapper: None
            },
            chord_sender: None,
            chain_sender: None,
            chain_handler: None,
            chord_handler: None,
            routers: self.routers.clone(),
            backend_senders: vec![],
            registry
        }
    }

    /// Create a new application builder. Requires a standard configuration.
    ///
    /// # Arguments
    /// * `config` - The Cannon Configuration to use
    pub fn new(config: CannonConfig<'static>) -> AppBuilder {
        let routers = config.routers.clone();
        let num_broker_threads = config.num_broker_threads.clone();
        let num_backend_threads = config.num_backend_threads.clone();
        let total_threads = num_broker_threads + num_backend_threads;
        let mut rt_builder = runtime::Builder::new_multi_thread();
        rt_builder.enable_all();
        rt_builder.worker_threads(total_threads as usize);
        Builder::new()
            .format(|buf, record| {
                writeln!(buf,
                         "{} [{}] - {}",
                         Local::now().format("%Y-%m-%dT%H:%M:%S"),
                         record.level(),
                         record.args()
                )
            })
            .filter(None, LevelFilter::Info)
            .init();
        AppBuilder{
            name: "data_cannon_worker".to_string(),
            brokers: vec![],
            backends: vec![],
            consumers: vec![],
            config,
            pool_types: vec![],
            routers
        }
    }
}


/// Implementation of the application. This structure ties together the entire workmr,
/// creating, running, and monitoring the runtime loop, threadpools, backend handler,
/// broker consumer.
pub struct App {
    name: String,
    config: CannonConfig<'static>,
    backends: Vec<AvailableBackend>,
    consumers: Vec<ConsumerType>,
    brokers: Vec<BrokerType>,
    event_handler: Option<JoinHandle<Result<(), ()>>>,
    pool_types: Vec<PoolType>,
    available_backends: Vec<Backend>,
    available_consumers: Vec<Consumer>,
    general_config: Option<GeneralConfig>,
    pool_store: PoolStore,
    chord_sender: Option<mpsc::UnboundedSender<TaskResponse>>,
    chain_sender: Option<mpsc::UnboundedSender<TaskResponse>>,
    chain_handler: Option<ChainHandler>,
    chord_handler: Option<ChordHandler>,
    routers: Routers,
    backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>,
    registry: Registry
}


/// Application implementation
impl App{

    /// Register an io bound task with the relevant pool.
    ///
    /// # Arguments
    /// * `task_name` - Name of the task to register
    /// * `task` - Function to register wrapped in a FutureTask
    pub async fn register_io_task(
        &mut self,
        task_name: &'static str,
        task: FutureTask) -> &mut Self{
        let registry = self.registry.clone();
        let mut wlock = registry.io_registry.write().await;
        (*wlock).insert(task_name, task);
        self
    }

    /// Register a task in the cpu bound registry.
    ///
    /// # Arguments
    /// * `task_name` - The task name which must also match the name in the client
    /// * `task` - The function to store and execute
    pub fn register_cpu_task(&mut self, task_name: &'static str, task: Task) -> &mut Self{
        let registry = self.registry.cpu_registry.clone();
        let wlock = registry.write();
        if wlock.is_ok() {
            let mut gaurd = wlock.unwrap();
            (*gaurd).insert(task_name, task);
        }
        self
    }

    /// Register a custom threadpool task with the registry
    ///
    /// # Arguments
    /// * `task_name` - Name of the task
    /// * `task` - Relevant task
    pub fn register_custom_task(&mut self, task_name: &'static str, task: Task) -> &mut Self{
        let registry = self.registry.custom_registry.clone();
        let wlock = registry.write();
        if wlock.is_ok() {
            let mut gaurd = wlock.unwrap();
            (*gaurd).insert(task_name, task);
        }
        self
    }

    /// Takes configuration and creates a configuration containing general shared variables.
    fn get_general_config(&mut self) -> GeneralConfig{
        let app_status = Arc::new(AtomicBool::new(true));
        let max_tasks = self.config.max_concurrent_tasks;
        debug!("Maximum Task Size: {}", max_tasks.clone());
        let sema = Arc::new(Semaphore::new(max_tasks));
        let gen_config = GeneralConfig::new(app_status, sema);
        gen_config
    }

    /// Create the futures pool from a relevant configuration
    ///
    /// # Arguments
    /// * `config` - CPU based pool configuration
    /// * `backend_sender` - Tokio unbounded sender to send responses on
    /// * `app_status` - Application status
    fn get_future_pool_config_from_user_config(
        &mut self,
        config: CPUPoolConfig,
        backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>,
        app_status: Arc<AtomicBool>) -> FuturesPoolConfig{
        let max_tasks = config.max_queued_tasks;
        let sema = Arc::new((Mutex::new(max_tasks), Condvar::new()));
        let (event_sender, event_receiver) = channel::unbounded();
        let (task_sender, task_receiver) = channel::unbounded();
        FuturesPoolConfig{
            pool_size: config.pool_size,
            failures_per_n_calls: config.failures_per_n_calls,
            reset_failures_on_n_calls: config.reset_failures_on_n_calls,
            event_sender,
            event_receiver,
            task_sender,
            task_receiver,
            backend_senders,
            status: app_status,
            sema_pair: sema
        }
    }

    /// Get the IO Pool configuration from given config and shared variables
    ///
    /// # Arguments
    /// * `config` - IO based pool configuration
    /// * `backend_sender` - Tokio unbounded sender to send responses on
    /// * `app_status` - Application status
    fn get_io_pool_config_from_user_config(
        &mut self,
        config: IOPoolConfig,
        backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>,
        app_status: Arc<AtomicBool>) -> IOFuturesPoolConfig {
        let max_tasks = config.max_tasks.clone();
        let sema = Arc::new(Semaphore::new(max_tasks));
        IOFuturesPoolConfig{
            backend_senders,
            sema: sema,
            pool_state: app_status,
            max_tasks: config.max_tasks
        }
    }

    /// Get the custom pool config from the given config and shared variables
    ///
    /// # Arguments
    /// * `config` - Custom based pool configuration
    /// * `backend_sender` - Tokio unbounded sender to send responses on
    /// * `app_status` - Application status
    /// * `worker_sema_pair` - Worker semaphore and conditional variable for indicating a task sent
    fn get_custom_pool_config_from_user_config(
        &mut self,
        config: CustomPoolConfig,
        backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>,
        app_status: Arc<AtomicBool>) -> CustomThreadPoolConfig {
        let task_count = config.max_tasks;
        CustomThreadPoolConfig{
            pool_size: config.pool_size,
            backend_senders,
            max_tasks: task_count,
            max_failures: config.max_failures,
            failures_per_n_calls: config.failures_per_n_calls,
            pool_state: app_status,
            on_start: config.on_start,
            on_teardown: config.on_teardown,
            call_fn: config.call_fn
        }
    }

    /// Create the futures pool
    ///
    /// # Arguments
    /// * `config` - The IO Pool config
    /// * `general_config` - General configuration containing shared variables
    async fn build_futures_pool(&mut self, config: CPUPoolConfig, general_config: GeneralConfig){
        let registry = self.registry.cpu_registry.clone();
        let mut gconf = general_config;
        let backend_senders = self.backend_senders.clone();
        let app_status = gconf.get_app_status();
        let futures_pool_config = self.get_future_pool_config_from_user_config(
            config, backend_senders, app_status);
        let (task_sender, task_receiver) = mpsc::unbounded_channel();
        let pool_handle = cpu_pool::run_on_runtime(
            registry, futures_pool_config, task_receiver).await;
        let pool_wrapper = FuturesPoolWrapper{
            pool_handle,
            task_sender: task_sender
        };
        self.pool_store.future_wrapper = Some(Arc::new(pool_wrapper));
    }

    /// Build IO pool and store in the available backends
    ///
    /// # Arguments
    /// * `config` - The IO Pool config
    /// * `general_config` - General configuration containing shared variables
    async fn build_io_pool(&mut self, config: IOPoolConfig, general_config: GeneralConfig){
        let registry = self.registry.io_registry.clone();
        let mut gconf = general_config;
        let backend_senders = self.backend_senders.clone();
        let app_status = gconf.get_app_status();
        let io_pool_config = self.get_io_pool_config_from_user_config(
            config, backend_senders, app_status);
        let (task_sender, task_receiver) = mpsc::unbounded_channel();
        let pool_handle = io_futures_pool::run_on_runtime(
            registry, io_pool_config, task_receiver).await;
        let pool_wrapper = IOFuturesPoolWrapper{
            pool_handle,
            task_sender
        };
        self.pool_store.io_wrapper = Some(Arc::new(pool_wrapper));
    }

    /// Build custom pool
    ///
    /// # Arguments
    /// * `config` - The Custom pool config
    /// * `general_config` - General configuration containing shared variables
    async fn build_custom_pool(&mut self, config: CustomPoolConfig, general_config: GeneralConfig){
        let registry = self.registry.custom_registry.clone();
        let app_config = self.config.clone();
        let mut gconf = general_config;
        let backend_senders = self.backend_senders.clone();
        let app_status = gconf.get_app_status();
        let custom_pool_config = self.get_custom_pool_config_from_user_config(
            config, backend_senders, app_status);
        let (task_sender, task_receiver) = mpsc::unbounded_channel();
        let pool_handle = thread_pool::run_on_runtime(
            registry, app_config,custom_pool_config, task_receiver).await;
        let pool_wrapper = CustomThreadPoolWrapper {
            pool_handle,
            task_sender
        };
        self.pool_store.custom_wrapper = Some(Arc::new(pool_wrapper));
    }

    /// Panic when there are no threadpools
    fn panic_on_no_threadpools(&mut self){
        if self.pool_store.custom_wrapper.is_none()
            &&
            self.pool_store.io_wrapper.is_none()
            &&
            self.pool_store.future_wrapper.is_none(){
            let e = "3 - No Threadpools";
            error!("[{}] {}", self.name, e);
            panic!("[{}] {}", self.name, e);
        }
    }

    /// Create available threadpools and submit to the vector
    ///
    /// # Arguments
    /// * `pool_config` - Configuration
    async fn create_threadpools(&mut self, pool_config: GeneralConfig){
        let pool_types = self.pool_types.clone();
        for config in pool_types{
            if let PoolType::CPUPOOL(config)  = config{
                self.build_futures_pool(config, pool_config.clone()).await;
            }else if let PoolType::IOPOOL(config) = config{
                self.build_io_pool(config, pool_config.clone()).await;
            }else if let PoolType::CUSTOMPOOL(config) = config{
                self.build_custom_pool(config, pool_config.clone()).await;
            }
        }
        self.panic_on_no_threadpools();
    }

    /// Get the custom sender
    fn get_custom_sender(&mut self) -> Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>{
        let custom_opt = self.pool_store.custom_wrapper.clone();
        if custom_opt.is_some(){
            let sender = custom_opt.clone().unwrap().task_sender.clone();
            Some(sender)
        }else{
            None
        }
    }

    /// Get the cpu sender if it exists
    fn get_cpu_sender(&mut self) -> Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>{
        let cpu_opt = self.pool_store.future_wrapper.clone();
        if cpu_opt.is_some(){
            let sender = cpu_opt.clone().unwrap().task_sender.clone();
            Some(sender)
        }else{
            None
        }
    }

    /// Get the io sender if it exists
    fn get_io_sender(&mut self) -> Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>{
        let io_opt = self.pool_store.io_wrapper.clone();
        if io_opt.is_some(){
            let sender = io_opt.clone().unwrap().task_sender.clone();
            Some(sender)
        }else{
            None
        }
    }

    /// Setup the consumer for rabbitmq
    ///
    /// # Arguments
    /// * `general_config` - General set of shared variables for the application
    async fn setup_rabbitmq_consumer(
        &mut self,
        general_config: GeneralConfig,
        result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>,
        rt: Arc<Runtime>){
        let registry = self.registry.clone();
        let config = self.config.clone();
        let mut gconf = general_config;
        let io_sender = self.get_io_sender();
        let cpu_sender = self.get_cpu_sender();
        let custom_sender = self.get_custom_sender();
        let consumer_config = RabbitMQConsumerConfig{
            config: config.clone(),
            task_semaphore: gconf.get_sema().clone(),
            io_task_sender: io_sender,
            cpu_task_sender: cpu_sender,
            custom_task_sender: custom_sender,
            status: gconf.get_app_status(),
            backend_senders: self.backend_senders.clone(),
            consumer_delay: None,
            consumers_per_queue: self.config.consumers_per_queue.clone()
        };
        let consumer_result = RabbitMQConsumer::new(
            registry,config, consumer_config, result_store, rt).await;
        let inner_result = consumer_result.unwrap();
        if inner_result.is_ok() {
            let mut consumer = inner_result.ok().unwrap();
            consumer.start().await;
            let available_consumer = Consumer::RabbitMQ(consumer);
            self.available_consumers.push(available_consumer)
        }else{
            let e = " 2 - Failed to Create Consumer";
            error!("[{}]{}", self.name, e);
            panic!("[{}]{}", self.name, e);
        }
    }

    /// Panic when there are no consumers
    fn panic_on_no_consumers(&mut self){
        if self.available_consumers.is_empty(){
            let e = "4 - No Consumers Created!";
            error!("[{}] {}", self.name, e);
            panic!("[{}] {}", self.name, e);
        }
    }

    /// Create the available consumers
    ///
    /// # Arugments
    /// * `pool_configs` - General set of shared variables for the application
    async fn create_consumers(&mut self,
                        pool_config: GeneralConfig,
                        result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>,
                        rt: Arc<Runtime>){
        let consumer_configs = self.consumers.clone();
        for conf in consumer_configs{
            match conf{
                ConsumerType::RabbitMQ =>{
                    info!("Datacannon :: Worker :: Starting RabbitMQ Consumer");
                    self.setup_rabbitmq_consumer(
                        pool_config.clone(), result_store.clone(), rt.clone()).await;
                }
            }
        }
        info!("Datacannon :: Worker :: Finished Creating consumers");
        self.panic_on_no_consumers();
    }

    /// Setup the redis backend
    ///
    /// # Arguments
    /// * `backend_config` - Backend configuration for redis
    /// * `num_producers` - number of producers to start
    /// * `pool_config` - General configuration of shared variables related to the app
    /// * `backend_receiver` - Receiver for backend tasks in the application
    /// * `sender` - Backend sender for this backend which is used on a failure
    /// * `chord_sender` - Channel for sending chords too
    async fn setup_redis_backend(
        &mut self,
        backend_config: BackendConfig,
        num_producers: u8,
        pool_config: GeneralConfig,
        backend_receiver: Arc<sync::Mutex<mpsc::UnboundedReceiver<TaskResponse>>>,
        sender: mpsc::UnboundedSender<TaskResponse>,
        chord_sender: mpsc::UnboundedSender<TaskResponse>,
        chain_sender: mpsc::UnboundedSender<TaskResponse>){
        let config = self.config.clone();
        let mut gconf = pool_config;
        let redis_config = RedisHandlerConfig{
            backend_config,
            max_failures: self.config.maximum_allowed_failures.clone(),
            num_producers,
            semaphore: gconf.get_sema().clone(),
            status: gconf.get_app_status().clone(),
            receiver: backend_receiver,
            sender,
            chord_sender,
            chain_sender
        };
        let mut redis_handler = RedisHandler::new(config, redis_config);
        redis_handler.start().await;
        let available_backend = Backend::Redis(redis_handler);
        self.available_backends.push(available_backend);
    }

    /// Panic when no backends are specified
    fn panic_on_no_backends(&mut self){
        if self.backends.is_empty(){
            let e = "5 - No backends created!";
            error!("[{}] {}", self.name, e);
            panic!("[{}] {}", self.name, e);
        }
    }

    /// Create the available backends
    ///
    /// # Arguments
    /// * `pool_config` - Configuration variable stemming from the threadpool
    /// * `backend_receiver` - Task Response for the backend
    /// * `chord_sender` - Channel for sending chords through
    /// * `chain_sender` - Channel for sending chain events through
    async fn create_backends(
        &mut self,
        pool_config: GeneralConfig,
        chord_sender: mpsc::UnboundedSender<TaskResponse>,
        chain_sender: mpsc::UnboundedSender<TaskResponse>){
        let backend_configs = self.backends.clone();
        for config in backend_configs{
            match config{
                AvailableBackend::Redis(config) => {
                    let (backend_sender, backend_receiver) = mpsc::unbounded_channel();
                    let (config, num_producers) = config;
                    self.setup_redis_backend(
                        config, num_producers,
                        pool_config.clone(),
                        Arc::new(sync::Mutex::new(backend_receiver)),
                        backend_sender.clone(),
                        chord_sender.clone(),
                        chain_sender.clone()).await;
                    self.backend_senders.push(backend_sender);
                }
                _ =>{
                    panic!("Backend Config Not Understood")
                }
            }
        }
        self.panic_on_no_backends();
    }

    /// Panic if consumers, backends, or pools are not specified
    fn panic_on_all_empty(&mut self){
        if self.consumers.is_empty() || self.backends.is_empty() || self.pool_types.is_empty(){
            let e = "Pools, consumers, or backends Empty!";
            error!("[{}] 1 - {}", self.name, e);
            panic!("[{}] 1 - {}", self.name, e);
        }
    }

    /// Start the event handler loop which responds to specific events
    async fn start_event_handler(&mut self) {
        let cfg = self.config.clone();
        info!("Datacannon :: Worker :: Starting Event Handler");
        let app_status = self.general_config.clone().unwrap().get_app_status();
        let backend_config = self.config.event_backend.clone();
        let handler_result = RedisEventHandler::new(
            cfg, app_status, backend_config, events::delegator).await;
        if handler_result.is_ok(){
            let mut handler = handler_result.ok().unwrap();
            let spawn_handle = handler.start().await;
            self.event_handler = Some(spawn_handle);
        }else{
            let e = "Failed to Start Event Handler";
            error!("{}", e);
            panic!("{}", e);
        }
    }

    /// Start the chain handler for chaining additional tasks
    ///
    /// # Arguments
    /// * `chain_receiver` - Channel receiver responses are pushed on
    /// * `config` - General configuration variables for the application
    /// * `result_store` - A result store
    async fn start_chain_handler(
        &mut self,
        chain_receiver: mpsc::UnboundedReceiver<TaskResponse>,
        config: GeneralConfig,
        result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>,
        rt: Arc<Runtime>){
        let mut cfg = config;
        let ctx = self.config.clone();
        let handler_config = ChainHandlerConfig{
            status: cfg.get_app_status(),
            routers: self.routers.clone(),
            brokers: self.brokers.clone(),
            receiver: Arc::new(sync::Mutex::new(chain_receiver))
        };
        let mut handler = ChainHandler::new(
            ctx.clone(), handler_config, result_store);
        handler.start(ctx, rt).await;
        self.chain_handler = Some(handler);
    }

    /// Starts the chord handler
    ///
    /// # Arguments
    /// * `chord_receiver` - Channel for receving chord tasks
    /// * `config` - General configuration variables
    async fn start_chord_handler(
        &mut self, chord_receiver: mpsc::UnboundedReceiver<TaskResponse>, config: GeneralConfig){
        let registry = self.registry.clone();
        let app_config = self.config.clone();
        let mut cfg = config;
        let handler_config = ChordHandlerConfig{
            semaphore: cfg.get_sema().clone(),
            io_task_sender: self.get_io_sender(),
            cpu_task_sender: self.get_cpu_sender(),
            custom_task_sender: self.get_custom_sender(),
            status: cfg.get_app_status().clone(),
            receiver: Arc::new(sync::Mutex::new(chord_receiver))
        };
        let mut handler = ChordHandler::new(
            registry, app_config, handler_config);
        handler.start().await;
        self.chord_handler = Some(handler);
    }

    /// Starts the broker and handles inbound tasks. Creates a consumer
    /// and handles inbound message. Should be called after registering tasks. Threadpool
    /// setup performs receiver and sender setup as well.
    pub async fn start(
        &mut self,
        result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>,
        notifier: Arc<Notify>,
        rt: Arc<Runtime>){
        info!("Datacannon :: Worker :: Starting System");
        let (chord_sender, chord_receiver) = mpsc::unbounded_channel();
        let (chain_sender, chain_receiver) = mpsc::unbounded_channel();
        self.chain_sender = Some(chain_sender.clone());
        self.chord_sender = Some(chord_sender.clone());
        self.panic_on_all_empty();
        let conf = self.get_general_config();
        self.general_config = Some(conf.clone());
        info!("Datacannon :: Worker :: Creating Backends");
        self.create_backends(
            conf.clone(), chord_sender, chain_sender).await;
        info!("Datacannon :: Worker :: Creating Threadpools");
        self.create_threadpools(conf.clone()).await;
        info!("Datacannon :: Worker :: Starting Chain Handler");
        self.start_chain_handler(
            chain_receiver, conf.clone(), result_store.clone(), rt.clone()).await;
        info!("Datacannon :: Worker :: Starting Chord Handler");
        self.start_chord_handler(chord_receiver, conf.clone()).await;
        info!("Datacannon :: Worker :: Creating Consumers");
        self.create_consumers(conf.clone(), result_store.clone(), rt).await;
        info!("Datacannon :: Worker :: Starting Event Handler");
        self.start_event_handler().await;
        info!("Datacannon :: Worker :: Started");
        notifier.notify_one();
    }

    /// Close consumers
    async fn close_consumers(&mut self){
        while self.available_consumers.len() > 0 {
            let consumer_opt = self.available_consumers.pop();
            if consumer_opt.is_some(){
                let available_consumer = consumer_opt.unwrap();
                match available_consumer{
                    Consumer::RabbitMQ(available_consumer) =>{
                        let mut consumer = available_consumer;
                        consumer.close().await;
                    }
                }
            }
        }
    }

    /// Close backends
    async fn close_backends(&mut self){
        while self.available_backends.len() > 0{
            let backend_opt = self.available_backends.pop();
            if backend_opt.is_some(){
                let available_backend = backend_opt.unwrap();
                match available_backend{
                    Backend::Redis(available_backend) =>{
                        let backend = available_backend;
                        backend.close().await;
                    }
                }
            }
        }
    }

    /// Wait for the event handle to shutdown after sending an event.
    async fn shutdown_event_handle(&mut self){
        let event_handler = self.event_handler.take();
        if let Some(event_handler) = event_handler {
            let backend_config = self.config.event_backend.clone();
            let backend_route = self.config.event_routing_key.clone().to_string();
            let redis_producer = RedisProducer::new(
                backend_config).await;
            let pconn = redis_producer.get_paired_conn();
            let m = "SKIP".to_string();
            producer::publish(
                pconn, Some(1), Some(10), backend_route, m).await;
            let r = event_handler.await;
            if r.is_err(){
                error!("Event Handler in Worker Did Not Shut Down Correctly!");
                panic!("Event Handler in Worker Did Not Shut Down Correctly!");
            }
        }
    }

    /// Get a fake task to advance the flow control futures
    fn get_fake_task_config(&mut self) -> TaskConfig {
        let cfg = self.config.clone();
        TaskConfig::new(
            cfg,
            broker_type::RABBITMQ.to_string(),
            "FAKE".to_string(),
            Some("test_exchange".to_string()),
            Some("DIRECT".to_string()),
            Some("test_key".to_string()),
            Some("test_parent_id".to_string()),
            None,
            None,
            Some("test_reply".to_string()),
            None,
            None,
            None,
            None,
            None,
            10000,
            Some(1),
            None,
            None)
    }

    /// Close the chain handler
    async fn close_chain_handler(&mut self){
        let cfg = self.get_fake_task_config();
        let response = Response::new();
        let sc = StreamConfig::new(None, None);
        let response = TaskResponse::new(cfg, response, sc, true, None);
        let sender = self.chain_sender.clone().unwrap();
        if let Some(mut chain_handler) = self.chain_handler.take(){
            let handle_opt = chain_handler.close();
            let _r = sender.send(response);
            if handle_opt.is_some(){
                let handle = handle_opt.unwrap();
                let _r = handle.await;
            }
        }
    }

    /// Close the chord handler
    async fn close_chord_handler(&mut self){
        let cfg = self.get_fake_task_config();
        let response = Response::new();
        let sc = StreamConfig::new(None, None);
        let response = TaskResponse::new(cfg, response, sc, true, None);
        let sender = self.chord_sender.clone().unwrap();
        if let Some(mut chord_handler) = self.chord_handler.take(){
            let handle_opt = chord_handler.close();
            let _r = sender.send(response);
            if handle_opt.is_some() {
                let handle = handle_opt.unwrap();
                let _r = handle.await;
            }
        }
    }

    /// Closes the application. Calls close on threadpools, conusmers, and backends.
    pub async fn close(&mut self){
        info!("Datacannon :: worker :: Closing Application");
        if self.general_config.is_some(){
            let mut config = self.general_config.clone().unwrap();
            config.set_app_status_closing();
        }
        info!("Datacannon :: worker :: Closing consumers");
        self.close_consumers().await;
        info!("Datacannon :: worker :: Closing Backends");
        self.close_backends().await;
        info!("Datacannon :: worker :: Closing Chord Handler");
        self.close_chord_handler().await;
        info!("Datacannon :: worker :: Closing Chain Handler");
        self.close_chain_handler().await;
        info!("Datacannon :: worker :: Shutting Down Event Handler");
        self.shutdown_event_handle().await;
        info!("Datacannon :: Worker :: Closed");

    }
}


#[cfg(test)]
pub mod tests {
    use std::{env, panic, thread};
    use std::collections::HashMap;
    use std::sync::{Arc, Condvar, Mutex};
    use std::sync::atomic::{AtomicBool, Ordering};
    use std::time::Duration;

    use crossbeam;
    use datacannon_rs_core::argparse::argtype::ArgType;
    use datacannon_rs_core::backend::config::BackendConfig;
    use datacannon_rs_core::backend::types::AvailableBackend;
    use datacannon_rs_core::broker::amqp::rabbitmq::RabbitMQBroker;
    use datacannon_rs_core::broker::broker_type;
    use datacannon_rs_core::config::config;
    use datacannon_rs_core::config::config::{BrokerType, CannonConfig};
    use datacannon_rs_core::connection::amqp::connection_inf::AMQPConnectionInf;
    use datacannon_rs_core::connection::connection::ConnectionConfig;
    use datacannon_rs_core::message_protocol::stream::StreamConfig;
    use datacannon_rs_core::message_structure::amqp::exchange::Exchange;
    use datacannon_rs_core::message_structure::amqp::queue::AMQPQueue;
    use datacannon_rs_core::message_structure::queues::GenericQueue;
    use datacannon_rs_core::replication::rabbitmq::{RabbitMQHAPolicies, RabbitMQHAPolicy};
    use datacannon_rs_core::replication::replication::HAPolicy;
    use datacannon_rs_core::router::router::{Router, Routers};
    use datacannon_rs_core::task::config::TaskConfig;
    use datacannon_rs_core::task::context::TaskContext;
    use datacannon_rs_core::task::result::{Response, TaskResponse};
    use lapin::ExchangeKind;
    use tokio::runtime::Runtime;
    use tokio::sync::mpsc;
    use tokio::sync::{Notify, RwLock};

    use crate::app::app::AppBuilder;
    use crate::consumer::consumer_types::ConsumerType;
    use crate::task::runner::TaskRunner;
    use crate::task::wrapper::Task;
    use crate::threadpool::cpu_pool::CPUPoolConfig;
    use crate::threadpool::io_futures_pool::IOPoolConfig;
    use crate::threadpool::pool_types::PoolType;
    use crate::threadpool::thread_pool::CustomPoolConfig;

    use super::*;

    fn get_routers() -> Routers {
        let mut rts = Routers::new();
        let username = env::var("RABBITMQ_USERNAME").unwrap();
        let pwd = env::var("RABBITMQ_PWD").unwrap();
        let mut queues = Vec::<GenericQueue>::new();
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(username),
            Some(pwd),
            1000);
        let mut q = AMQPQueue::new(
            "lapin_test_queue".to_string(),
            Some("test_exchange".to_string()),
            Some("test_key".to_string()),
            0,
            HAPolicy::RabbitMQ(
                RabbitMQHAPolicy::new(
                    RabbitMQHAPolicies::ALL, 1)),
            false, amq_conf);
        q.set_prefetch_count(1000);
        queues.push(GenericQueue::AMQPQueue(q));
        let exch = Exchange::new("test_exchange".to_string(), ExchangeKind::Direct);
        let router = Router::new("test_key".to_string(), queues, Some(exch));
        rts.add_router("test_key".to_string(), router);
        rts
    }

    fn get_runtime() -> Arc<Runtime>{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(10).build();
        Arc::new(rt.unwrap())
    }

    /// Test task that can be used as an example. Notice the loop. You can run Python, Java
    /// or other supported code in this loop.
    fn test_custom(task_receiver: crossbeam::channel::Receiver<TaskRunner>, bsend: Vec<mpsc::UnboundedSender<TaskResponse>>, working: Arc<AtomicBool>, cvar_pair: Arc<(Mutex<bool>, Condvar)>, svar_pair: Arc<(Mutex<usize>, Condvar)>) {
        let backend_sender = bsend.clone();
        let &(ref slock, ref svar) = &*svar_pair;
        let &(ref lock, ref cvar) = &*cvar_pair;
        while (*working).load(Ordering::Relaxed) {
            let task_result = task_receiver.recv_timeout(
                Duration::new(0, 100));
            if task_result.is_ok() {
                let r = panic::catch_unwind(|| {
                    let mut runner = task_result.ok().unwrap();
                    let sc = runner.get_stream_config().clone();
                    let task_config = runner.get_task().clone();
                    let args = task_config.get_args().clone();
                    let kwargs = task_config.get_kwargs().clone();
                    let stream_config = sc.clone();
                    let task_context = TaskContext::new(
                        Some(args), Some(kwargs), Some(stream_config));
                    let response = runner.get_fn()(task_context);
                    if response.is_ok() {
                        Ok((response.ok().unwrap(), sc, task_config))
                    } else {
                        Err(("Task Failed", sc, task_config))
                    }
                });
                let senders = backend_sender.clone();
                if r.is_ok() {
                    let task_result = r.ok().unwrap();
                    if task_result.is_ok() {
                        let (response, sc, task_config) = task_result.unwrap();
                        let task_response = TaskResponse::new(task_config, response, sc, true, None);
                        for sender in senders {
                            let _r = sender.send(task_response.clone());
                        }
                    } else {
                        let (err, sc, task_config) = task_result.err().unwrap();
                        let response = Response::new();
                        let task_response = TaskResponse::new(task_config, response, sc, false, Some(err.to_string()));
                        for sender in senders {
                            let _r = sender.send(task_response.clone());
                        }
                    }
                } else {
                    error!("Function Failed");
                }
            } else {
                let mut has_work = lock.lock().unwrap();
                *has_work = false;
                while !*has_work {
                    has_work = cvar.wait(has_work).unwrap();
                }
            }
            let mut permits = slock.lock().unwrap();
            if *permits < 10000 {
                *permits += 1;
            }
            svar.notify_one();
        }
    }

    fn test_io(_ctx: TaskContext) -> JoinHandle<Result<Response, String>>{
        tokio::spawn(async {
            let mut r = Response::new();
            r.add_result("test_result".to_string(), ArgType::Bool(true));
            r.add_result("is_complete".to_string(), ArgType::Bool(true));
            Ok(r)
        })
    }

    fn test_it(_ctx: TaskContext) -> Result<Response, String> {
        //big test string
        /*
        for _i in 0..1 {
            let lstr = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

            //really big shitty ((n^2)/2)m regex
            //let re = Regex::new(r"L.*").unwrap();
            //re.find_iter(lstr);

            //really small max nm loop
            let f = lstr.find("industry");
            if f.is_some() {
                let idx = f.unwrap();
                let splarr = lstr.split_at(idx);
                let _r = splarr.0.replace("Lorem", "Lore");
            }
        }
         */
        let mut r = Response::new();
        r.add_result("test_result".to_string(), ArgType::Bool(true));
        r.add_result("is_complete".to_string(), ArgType::Bool(true));
        Ok(r)
    }

    fn get_backend_config() -> BackendConfig {
        let bc = BackendConfig {
            url: "127.0.0.1:6379",
            username: None,
            password: None,
            transport_options: None
        };
        bc
    }

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("RABBITMQ_USERNAME").ok().unwrap();
        let pwd = env::var("RABBITMQ_PWD").ok().unwrap();
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user),
            Some(pwd), 1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = get_backend_config();
        let routers = get_routers();
        let mut cannon_conf = CannonConfig::new(conn_conf, config::BackendType::REDIS, backend_conf, routers);
        cannon_conf.prefetch_limit = 1000;
        cannon_conf.num_broker_channels = 5;
        cannon_conf.num_broker_connections=2;
        cannon_conf.num_backend_channels = 20;
        cannon_conf.num_backend_connections = 5;
        cannon_conf.consumers_per_queue = 5;
        cannon_conf.max_concurrent_tasks = 1000;
        cannon_conf
    }

    async fn get_broker(
        result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>,
        rt: Arc<Runtime>) -> RabbitMQBroker {
        let routers = get_routers();
        let config = get_config();
        let broker_result = RabbitMQBroker::new(
            config, routers, result_store, rt).await;
        let mut broker = broker_result.ok().unwrap();
        let _r = broker.setup().await;
        broker
    }

    fn get_fake_task_config() -> TaskConfig {
        let config = get_config();
        TaskConfig::new(
            config,
            broker_type::RABBITMQ.to_string(),
            "test_task".to_string(),
            Some("test_exchange".to_string()),
            Some("DIRECT".to_string()),
            Some("test_key".to_string()),
            Some("test_parent_id".to_string()),
            None,
            None,
            Some("reply_to".to_string()),
            None,
            None,
            None,
            None,
            None,
            10000,
            Some(1),
            None,
            None)
    }

    async fn push_fake_messages(
        num_messages: usize,
        result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>,
        rt: Arc<Runtime>) {
        let config = get_config();
        let mut broker = get_broker(result_store, rt).await;
        for _i in 0..num_messages {
            let cfg = get_fake_task_config();
            let sc = StreamConfig::new(
                None, None);
            let r = broker.send(
                config.clone(),
                cfg,
                Some(sc)).await;
            assert!(r.is_ok());
            if r.is_ok(){
                let h = r.ok().unwrap();
                let _r = h.await;
            }
        }
        broker.close().await;
    }

    fn get_consumers() -> Vec<ConsumerType> {
        let ctype = ConsumerType::RabbitMQ;
        vec![ctype]
    }

    fn get_threadpools() -> Vec<PoolType> {
        let cfg = CPUPoolConfig {
            pool_size: 100,
            max_queued_tasks: 20000,
            failures_per_n_calls: 10,
            reset_failures_on_n_calls: 100
        };
        let ptype = PoolType::CPUPOOL(cfg);
        vec![ptype]
    }

    fn get_io_threadpools() -> Vec<PoolType> {
        let cfg = IOPoolConfig {
            max_tasks: 10000
        };
        let ptype = PoolType::IOPOOL(cfg);
        vec![ptype]
    }

    fn get_custom_threadpool() -> Vec<PoolType> {
        let on_start = || -> Result<(), ()>{
            Ok(())
        };
        let on_teardown = || -> Result<(), ()>{
            Ok(())
        };
        let cfg = CustomPoolConfig {
            pool_size: 4,
            max_tasks: Some(10000),
            max_failures: 10,
            failures_per_n_calls: 100,
            on_start: on_start,
            on_teardown: on_teardown,
            call_fn: test_custom
        };
        let ptype = PoolType::CUSTOMPOOL(cfg);
        vec![ptype]
    }

    fn get_backends() -> Vec<AvailableBackend> {
        let bconf = get_backend_config();
        let backend = AvailableBackend::Redis((bconf, 25));
        vec![backend]
    }


    fn get_app() -> AppBuilder {
        let cfg = get_config();
        let mut builder = AppBuilder::new(cfg.clone());
        builder.consumers(get_consumers());
        builder.backends(get_backends());
        builder.pool_types(get_threadpools());
        builder
    }

    fn get_io_app() -> AppBuilder {
        let cfg = get_config();
        let mut pls = get_io_threadpools();
        pls.extend(get_threadpools());
        let mut builder = AppBuilder::new(cfg.clone());
        builder.consumers(get_consumers());
        builder.backends(get_backends());
        builder.pool_types(pls);
        builder
    }

    fn get_custom_app() -> AppBuilder {
        let cfg = get_config();
        let mut builder = AppBuilder::new(cfg.clone());
        builder.consumers(get_consumers());
        builder.backends(get_backends());
        builder.pool_types(get_custom_threadpool());
        builder
    }

    #[test]
    fn should_setup_and_close_application() {
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let broker_types = vec![BrokerType::RABBITMQ];
            let mut app_builder = get_custom_app();
            app_builder.brokers(broker_types);
            let mut app = app_builder.build();
            let results = Arc::new(RwLock::new(HashMap::new()));
            let notify = Arc::new(Notify::new());
            app.start(results.clone(), notify.clone(), rt).await;
            notify.notified().await;
            app.close().await;
        });
    }

    #[test]
    fn should_process_tasks_from_futures_pool() {
        let rt = get_runtime();
        rt.clone().block_on(async move{
            let results = Arc::new(RwLock::new(HashMap::new()));
            let mut app_builder = get_app();
            app_builder.brokers(vec![BrokerType::RABBITMQ]);
            let mut app = app_builder.build();
            let task = Task {
                f: test_it
            };
            push_fake_messages(10000, results.clone(), rt.clone()).await;
            app.register_cpu_task("test_task", task);
            let notify = Arc::new(Notify::new());
            app.start(results.clone(), notify.clone(), rt).await;
            notify.notified().await;
            while app.general_config.clone().unwrap().get_app_status().load(Ordering::Relaxed) {
                thread::sleep(Duration::from_millis(1000));
            }
            app.close().await;
        });
    }

    #[test]
    fn should_process_task_from_io_pool() {
        let rt = get_runtime();
        rt.clone().block_on(async move{
            let results = Arc::new(RwLock::new(HashMap::new()));
            let mut app_builder = get_io_app();
            let broker_types = vec![BrokerType::RABBITMQ];
            app_builder.brokers(broker_types);
            let mut app = app_builder.build();
            let task = FutureTask{
                f: test_io
            };
            push_fake_messages(10000, results.clone(), rt.clone()).await;
            app.register_io_task("test_task", task).await;
            let notify = Arc::new(Notify::new());
            app.start(results.clone(), notify.clone(),rt).await;
            notify.notified().await;
            while app.general_config.clone().unwrap().get_app_status().load(Ordering::Relaxed) {
                thread::sleep(Duration::from_millis(1000));
            }
            app.close().await;
        });
    }

    #[test]
    fn should_process_task_from_custom_pool() {
        let rt = get_runtime();
        rt.clone().block_on(async move{
            let results = Arc::new(RwLock::new(HashMap::new()));
            let task = Task {
                f: test_it
            };
            push_fake_messages(100000, results.clone(), rt.clone()).await;
            let broker_types = vec![BrokerType::RABBITMQ];
            let mut app_builder = get_custom_app();
            app_builder.brokers(broker_types);
            let mut app = app_builder.build();
            app.register_custom_task("test_task", task);
            let notify = Arc::new(Notify::new());
            app.start(results.clone(), notify.clone(), rt).await;
            notify.notified().await;
            while app.general_config.clone().unwrap().get_app_status().load(Ordering::Relaxed) {
                tokio::time::sleep(Duration::from_millis(1000)).await;
            }
            app.close().await;
        });
    }

    #[test]
    fn integrate_with_client(){
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let results = Arc::new(RwLock::new(HashMap::new()));
            let mut app_builder = get_io_app();
            let broker_types = vec![BrokerType::RABBITMQ];
            app_builder.brokers(broker_types);
            let mut app = app_builder.build();
            let task = FutureTask{
                f: test_io
            };
            let ftask = Task{
                f: test_it
            };
            app.register_io_task("test_it", task).await;
            app.register_cpu_task("test_it_pool", ftask);
            let notify = Arc::new(Notify::new());
            app.start(results.clone(), notify.clone(), rt).await;
            notify.notified().await;
            while app.general_config.clone().unwrap().get_app_status().load(Ordering::Relaxed) {
                tokio::time::sleep(Duration::from_millis(1000)).await;
            }
            app.close().await;
        });
    }
}

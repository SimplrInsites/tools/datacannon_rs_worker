//! A general pool configuration for passing shared items to pools during application related setup
//!
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use tokio::sync::Semaphore;

/// Pool configuration containing common elements to pass to functions
#[derive(Clone, Debug)]
pub struct GeneralConfig{
    app_status: Arc<AtomicBool>,
    sema: Arc<Semaphore>
}


/// Pool config implementation
impl GeneralConfig{

    /// Set the application status to closing
    pub fn set_app_status_closing(&mut self){
        self.app_status.store(false, Ordering::Relaxed);
    }

    /// Get the application status
    pub fn get_app_status(&mut self) -> Arc<AtomicBool>{
        self.app_status.clone()
    }

    /// Get the semaphore for the worker.
    pub fn get_sema(&mut self) -> Arc<Semaphore>{
        self.sema.clone()
    }

    /// Create a new pool configuration
    pub fn new(
        app_status: Arc<AtomicBool>, sema: Arc<Semaphore>) -> GeneralConfig{
        GeneralConfig{
            app_status,
            sema
        }
    }
}

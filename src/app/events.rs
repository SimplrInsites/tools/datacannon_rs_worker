//! Events delegator for handling general events from the system
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::atomic::Ordering;

use datacannon_rs_core::events::context::EventContext;
use datacannon_rs_core::events::event_types;

pub fn delegator(event_type: String, ctx: EventContext) -> bool{
    match event_type.as_str() {
        event_types::TERMINATE => {
            ctx.get_app_status().store(false, Ordering::Relaxed);
        },
        _ => {

        }
    }
    true
}

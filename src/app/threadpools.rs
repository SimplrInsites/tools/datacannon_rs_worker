//! IO configuration for the application
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;

use crate::threadpool::pool_types::{CustomThreadPoolWrapper, FuturesPoolWrapper, IOFuturesPoolWrapper};

pub(in crate) struct PoolStore{
    pub io_wrapper: Option<Arc<IOFuturesPoolWrapper>>,
    pub future_wrapper: Option<Arc<FuturesPoolWrapper>>,
    pub custom_wrapper: Option<Arc<CustomThreadPoolWrapper>>
}

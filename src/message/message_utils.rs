//! Message utilities for unpacking and packing messages in the worker.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::{BTreeMap, HashMap};

use amq_protocol_types::{AMQPValue, FieldTable, ShortString};
use datacannon_rs_core::argparse::argtype::{amqp_value_to_arg_type, ArgType, value_to_arg_map, value_vec_to_arg_vec};
use datacannon_rs_core::config::config::CannonConfig;
use datacannon_rs_core::message_protocol::stream::StreamConfig;
use datacannon_rs_core::task::config::{TaskConfig, TaskConfigBuilder};
use lapin::message::Delivery;
use lapin::protocol::BasicProperties;
use serde_json::Value;

/// Set the task name in the builder or return an error if non-existent.
///
/// # Arguments
/// * `headers` - The headers map from the Delivery message in lapin or an AMQP tool
/// * `builder` - The task configuration builder
fn get_amqp_header_task_name(headers: &BTreeMap<ShortString, AMQPValue>, builder: TaskConfigBuilder) -> Result<TaskConfigBuilder, ()>{
    let mut b = builder;
    let task_opt = headers.get("task");
    if task_opt.is_some() {
        let task_ss = task_opt.unwrap();
        if let AMQPValue::ShortString(task_ss) = task_ss{
            b.task_name(task_ss.as_str().to_string());
            Ok(b)
        }else if let AMQPValue::LongString(task_ss) = task_ss{
            b.task_name(task_ss.as_str().to_string());
            Ok(b)
        }else {
            Err(())
        }
    }else{
        Err(())
    }
}

/// Obtain the broker type
///
/// # Arguments
/// * `config` - Task configuration containing a default value
/// * `headers` - The headers map from the Delivery message in lapin or an AMQP tool
/// * `builder` - The task configuration builder
fn get_broker(config: &CannonConfig, headers: &BTreeMap<ShortString, AMQPValue>, builder: TaskConfigBuilder) -> Result<TaskConfigBuilder, ()>{
    let mut b = builder;
    let task_opt = headers.get("broker");
    if task_opt.is_some() {
        let task_val = task_opt.unwrap();
        if let AMQPValue::LongString(task_val) = task_val{
            b.broker(task_val.as_str().to_string());
            Ok(b)
        }else if let AMQPValue::ShortString(task_val) = task_val{
            b.broker(task_val.as_str().to_string());
            Ok(b)
        }else if let AMQPValue::Void = task_val{
            let lang = config.default_lang;
            b.broker(lang.to_string());
            Ok(b)
        }else{
            Err(())
        }
    }else{
        Err(())
    }
}

/// Obtain retries count from the lapin basic properties map.
///
/// # Arguments
/// * `config` - Task configuration containing a default value
/// * `headers` - The headers map from the Delivery message in lapin or an AMQP tool
/// * `builder` - The task configuration builder
fn get_amqp_retries(headers: &BTreeMap<ShortString, AMQPValue>, builder: TaskConfigBuilder) -> Result<TaskConfigBuilder, ()>{
    let mut b = builder;
    let task_opt = headers.get("retries");
    if task_opt.is_some() {
        let task_val = task_opt.unwrap();
        let arg_type = amqp_value_to_arg_type(task_val.clone());
        if arg_type.is_ok(){
            let arg = arg_type.ok().unwrap();
            if let ArgType::Int(arg) = arg{
                b.retries(arg as u8);
                Ok(b)
            }else if let ArgType::U64(arg) = arg{
                b.retries(arg as u8);
                Ok(b)
            }else{
                Err(())
            }
        }else{
            Err(())
        }
    }else{
        Err(())
    }
}


/// Obtain the language for the task from the headers
///
/// # Arguments
/// * `config` - Task configuration containing a default value
/// * `headers` - The headers map from the Delivery message in lapin or an AMQP tool
/// * `builder` - The task configuration builder
fn get_amqp_lang(config: &CannonConfig, headers: &BTreeMap<ShortString, AMQPValue>, builder: TaskConfigBuilder) -> Result<TaskConfigBuilder, ()>{
    let mut b = builder;
    let task_opt = headers.get("lang");
    if task_opt.is_some() {
        let task_val = task_opt.unwrap();
        if let AMQPValue::LongString(task_val) = task_val{
            b.lang(task_val.as_str().to_string());
            Ok(b)
        }else if let AMQPValue::ShortString(task_val) = task_val{
            b.lang(task_val.as_str().to_string());
            Ok(b)
        }else if let AMQPValue::Void = task_val{
            let lang = config.default_lang;
            b.lang(lang.to_string());
            Ok(b)
        }else{
            Err(())
        }
    }else{
        Err(())
    }
}


/// Get the expires
///
/// # Arguments
///
/// * `headers` - The headers map from the Delivery message in lapin or an AMQP tool
/// * `builder` - The task configuration builder
fn get_amqp_expires(headers: &BTreeMap<ShortString, AMQPValue>, builder: TaskConfigBuilder) -> Result<TaskConfigBuilder, ()>{
    let mut b = builder;
    let task_opt = headers.get("expires");
    if task_opt.is_some() {
        let argtype = amqp_value_to_arg_type(task_opt.unwrap().clone());
        if argtype.is_ok(){
            let arg = argtype.ok().unwrap();
            if let ArgType::U64(arg)  = arg{
                b.result_expires(arg);
                Ok(b)
            }else if let ArgType::Int(arg) = arg{
                b.result_expires(arg as u64);
                Ok(b)
            }else if let ArgType::NULL = arg{
                Ok(b)
            }else{
                Err(())
            }
        }else{
            Err(())
        }
    }else{
        Err(())
    }
}


/// Obtain the shadow for logging
///
/// # Arguments
///
/// * `headers` - The headers map from the Delivery message in lapin or an AMQP tool
/// * `builder` - The task configuration builder
fn get_amqp_shadow(headers: &BTreeMap<ShortString, AMQPValue>, builder: TaskConfigBuilder) -> Result<TaskConfigBuilder, ()>{
    let mut b = builder;
    let task_opt = headers.get("shadow");
    if task_opt.is_some() {
        let task_val = task_opt.unwrap();
        if let AMQPValue::LongString(task_val) = task_val{
            b.lang(task_val.as_str().to_string());
            Ok(b)
        }else if let AMQPValue::ShortString(task_val) = task_val{
            b.lang(task_val.as_str().to_string());
            Ok(b)
        }else if let AMQPValue::Void = task_val{
            b.shadow("WORKER");
            Ok(b)
        }else{
            b.shadow("WORKER");
            Ok(b)
        }
    }else{
        b.shadow("WORKER");
        Ok(b)
    }
}


/// Obtain the soft and hard time limits
///
/// # Arguments
/// * `config` - Task configuration containing a default value
/// * `headers` - The headers map from the Delivery message in lapin or an AMQP tool
/// * `builder` - The task configuration builder
fn get_amqp_time_limits(
    headers: &BTreeMap<ShortString, AMQPValue>, builder: TaskConfigBuilder) -> Result<TaskConfigBuilder, ()>{
    let mut b = builder;
    let limits_opt = headers.get("timelimit");
    if limits_opt.is_some() {
        let limits = limits_opt.unwrap();
        if let AMQPValue::FieldArray(limits) = limits{
            let lslice = limits.as_slice();
            if lslice.len() == 2{
                let soft_limit = lslice[0].clone();
                let hard_limit = lslice[1].clone();
                let mut is_fail = false;
                let sarg_type = amqp_value_to_arg_type(soft_limit);
                let harg_type = amqp_value_to_arg_type(hard_limit);
                if sarg_type.is_ok(){
                    let sarg = sarg_type.ok().unwrap();
                    if let ArgType::Int(sarg) = sarg{
                        b.soft_time_limit(sarg as u64);
                    }else if let ArgType::U64(sarg) = sarg{
                        b.soft_time_limit(sarg);
                    }else if let ArgType::NULL = sarg{

                    }else{
                        is_fail = true;
                    }
                }else{
                    is_fail = true;
                }

                if harg_type.is_ok(){
                    let harg = harg_type.ok().unwrap();
                    if let ArgType::Int(harg) = harg{
                        b.time_limit(harg as u64);
                    }else if let ArgType::U64(harg) = harg{
                        b.time_limit(harg);
                    }else if let ArgType::NULL = harg{

                    }else{
                        is_fail = true;
                    }
                }else{
                    is_fail = true;
                }

                if is_fail{
                    Err(())
                }else{
                    Ok(b)
                }
            }else{
                Err(())
            }
        }else {
            Err(())
        }
    }else{
        Err(())
    }
}

/// Obtain the eta from the task
///
/// # Arguments
/// * `headers` - The headers map from the Delivery message in lapin or an AMQP tool
/// * `builder` - The task configuration builder
fn get_amqp_eta(headers: &BTreeMap<ShortString, AMQPValue>, builder: TaskConfigBuilder) -> Result<TaskConfigBuilder, ()>{
    let mut b = builder;
    let eta_opt = headers.get("eta");
    if eta_opt.is_some() {
        let eta = eta_opt.unwrap();
        let argtype = amqp_value_to_arg_type(eta.clone());
        if argtype.is_ok(){
            let arg = argtype.ok().unwrap();
            if let ArgType::U64(arg)  = arg{
                b.eta(arg);
                Ok(b)
            }else if let ArgType::Int(arg) = arg{
                b.eta(arg as u64);
                Ok(b)
            }else if let ArgType::NULL = arg{
                Ok(b)
            }else{
                Err(())
            }
        }else{
            Err(())
        }
    }else{
        Err(())
    }
}


/// Obtain the parent id from the task
///
/// # Arguments
/// * `headers` - The headers map from the Delivery message in lapin or an AMQP tool
/// * `builder` - The task configuration builder
fn get_parent_id(headers: &BTreeMap<ShortString, AMQPValue>, builder: TaskConfigBuilder) -> Result<TaskConfigBuilder, ()>{
    let mut b = builder;
    let parent_id_opt = headers.get("parent_id");
    if parent_id_opt.is_some(){
        let amq_parent_id = parent_id_opt.unwrap().clone();
        let parent_id= amqp_value_to_arg_type(amq_parent_id);
        if parent_id.is_ok() {
            let parg= parent_id.ok().unwrap();
            if let ArgType::String(parg) = parg {
                b.parent_id(parg);
                Ok(b)
            }else{
                Err(())
            }
        }else{
            Err(())
        }
    }else{
        Err(())
    }
}


/// Handle the lapin amqp based headers
///
///# Arguments
/// * `config` - The application config
///* `headers` - Headers from the Delivery
///* `builder` - Task Configuration Builder
fn handle_amqp_headers(config: CannonConfig<'static>, headers: FieldTable, builder: TaskConfigBuilder) -> Result<TaskConfigBuilder,()>{
    let cfg = config.clone();
    let mut b = builder;
    let hmap = headers.inner();
    let mut r = get_amqp_header_task_name(&hmap, b);
    if r.is_ok(){
        b = r.ok().unwrap();
        r = get_broker(&cfg, &hmap, b);
        if r.is_ok() {
            b = r.ok().unwrap();
            r = get_amqp_lang(&cfg, &hmap, b);
            if r.is_ok() {
                b = r.ok().unwrap();
                r = get_amqp_retries(&hmap, b);
                if r.is_ok() {
                    b = r.ok().unwrap();
                    r = get_amqp_shadow(&hmap, b);
                    if r.is_ok() {
                        b = r.ok().unwrap();
                        r = get_amqp_time_limits(&hmap, b);
                        if r.is_ok() {
                            b = r.ok().unwrap();
                            r = get_amqp_eta(&hmap, b);
                            if r.is_ok() {
                                b = r.ok().unwrap();
                                r = get_amqp_expires(&hmap, b);
                                if r.is_ok() {
                                    b = r.ok().unwrap();
                                    r = get_parent_id(&hmap, b);
                                    if r.is_ok() {
                                        b = r.ok().unwrap();
                                        Ok(b)
                                    }else{
                                        Err(())
                                    }
                                } else {
                                    Err(())
                                }
                            } else {
                                Err(())
                            }
                        } else {
                            Err(())
                        }
                    } else {
                        Err(())
                    }
                } else {
                    Err(())
                }
            } else {
                Err(())
            }
        }else{
            Err(())
        }
    }else {
        Err(())
    }
}


/// Create a task config from an appropriately created AMQP BasicProperities struct
///
/// # Arguments
/// * `config` - Application configuration
/// * `properties` - AMQP properties to unwrap
/// * `routing_key` - Routing key from the message
/// * `exchange` - Name of the message exchange
pub fn amqp_properties_to_task_config(
    config: CannonConfig<'static>,
    routing_key: &str,
    exchange: &str,
    properties: BasicProperties) -> Result<TaskConfig, ()>{
    let correlation_id = properties.correlation_id();
    let reply_to = properties.reply_to();
    let priority = properties.priority();
    let headers_opt = properties.headers();
    if headers_opt.is_some() && correlation_id.is_some() && priority.is_some() && reply_to.is_some(){
        let headers = headers_opt.clone().unwrap();
        let mut task_builder = TaskConfigBuilder::default();
        let task_result = handle_amqp_headers(config, headers, task_builder);
        if task_result.is_ok(){
            task_builder = task_result.ok().unwrap();
            task_builder.kwargs(HashMap::<String, ArgType>::new());
            task_builder.args(Vec::<ArgType>::new());
            task_builder.exchange(Some(exchange.to_string()));
            task_builder.exchange_type(Some("DIRECT".to_string()));
            task_builder.routing_key(Some(routing_key.to_string()));
            task_builder.correlation_id(correlation_id.clone().unwrap().as_str().to_string());
            task_builder.reply_to(reply_to.clone().unwrap().as_str().to_string());
            task_builder.priority(priority.unwrap()as i8);
            task_builder.shadow("WORKER".to_string());
            let task_config = task_builder.build();
            if task_config.is_ok(){
                Ok(task_config.ok().unwrap())
            }else{
                let e = task_config.err().unwrap();
                eprintln!("FAILED TO INITIALIZE TASK - {}", e);
                error!("FAILED TO INITIALIZE TASK - {}", e);
                Err(())
            }
        }else{
            eprintln!("FAILED TO INITIALIZE TASK - TASK BUILDER FAILED");
            error!("FAILED TO INITIALIZE TASK - TASK BULDER FAILED");
            Err(())
        }
    }else {
        Err(())
    }
}


/// Unpack a byte vec to a stream config if in the appropriate form. Uses serde json deserialization
/// to work.
///
/// # Arguments
/// * `stream_data` - bytes vector to unpack
pub fn amqp_bytes_to_stream(stream_data: Vec<u8>) -> Result<(Vec<ArgType>, HashMap<String, ArgType>, StreamConfig), ()>{
    let vals_result = serde_json::from_slice(&stream_data);
    if vals_result.is_ok(){
        let vals: Vec<Value> = vals_result.ok().unwrap();
        if vals.len() == 3{
            let args_value = vals.get(0);
            let kwargs_value = vals.get(1);
            let stream_opt = vals.get(2);
            if stream_opt.is_some() && args_value.is_some() && kwargs_value.is_some(){
                let stream_val = stream_opt.unwrap().clone();
                let streamcfg_result = serde_json::from_value(stream_val);
                if streamcfg_result.is_ok(){
                    let stream_config: StreamConfig = streamcfg_result.ok().unwrap();
                    let args = args_value.unwrap();
                    let kwargs = kwargs_value.unwrap();
                    if let Value::Array(args) = args{
                        if let Value::Object(kwargs) = kwargs{
                            let argv = value_vec_to_arg_vec(args.clone());
                            let kwmap = value_to_arg_map(kwargs.clone());
                            if argv.is_ok() && kwmap.is_ok(){
                                Ok((argv.ok().unwrap(), kwmap.ok().unwrap(), stream_config))
                            }else{
                                Err(())
                            }
                        }else{
                            Err(())
                        }
                    }else{
                        Err(())
                    }
                }else{
                    Err(())
                }
            }else{
                Err(())
            }
        }else{
            Err(())
        }
    }else{
        Err(())
    }
}


/// Unpack a delivery from lapin to a sendable task and stream config
///
/// # Arguments
/// * `delivery` - The lapin Delivery object to unpack
pub fn unpack_delivery_from_amqp(
    config: CannonConfig<'static>, delivery: Delivery) -> Result<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig), ()>{
    let routing_key = delivery.routing_key.as_str();
    let exchange = delivery.exchange.as_str();
    let stream_data = delivery.data;
    let config_properties = delivery.properties;
    let stream_result = amqp_bytes_to_stream(stream_data);
    if stream_result.is_ok() {
        let task_result = amqp_properties_to_task_config(
            config, routing_key, exchange, config_properties);
        if task_result.is_ok() {
            let (args, kwargs, stream) = stream_result.ok().unwrap();
            let task = task_result.ok().unwrap();
            Ok((args, kwargs, task, stream))
        }else{
            Err(())
        }
    }else{
        Err(())
    }
}

/// Package a message for redis
pub fn pack_message_for_redis() {
    unimplemented!()
}


#[cfg(test)]
pub mod test{
    use std::env;

    use amq_protocol_types::FieldTable;
    use datacannon_rs_core::argparse::argtype::ArgType;
    use datacannon_rs_core::backend::config::BackendConfig;
    use datacannon_rs_core::config::config;
    use datacannon_rs_core::config::config::CannonConfig;
    use datacannon_rs_core::connection::amqp::connection_inf::AMQPConnectionInf;
    use datacannon_rs_core::connection::connection::ConnectionConfig;
    use datacannon_rs_core::message_protocol::headers::{Headers, TimeLimit};
    use datacannon_rs_core::router::router::Routers;
    use datacannon_rs_core::task::config::TaskConfigBuilder;
    use lapin::message::Delivery;
    use lapin::protocol::BasicProperties;
    use serde_json::Value;

    use crate::message::message_utils::{amqp_bytes_to_stream, amqp_properties_to_task_config, handle_amqp_headers, unpack_delivery_from_amqp};

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user").ok().unwrap();
        let pwd = env::var("rabbit_test_pwd").ok().unwrap();
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user),
            Some(pwd),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = BackendConfig{
            url: "",
            username: None,
            password: None,
            transport_options: None
        };
        let routers = Routers::new();
        let mut cannon_conf = CannonConfig::new(
            conn_conf, config::BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 100;
        cannon_conf
    }

    #[test]
    fn test_some_stuff(){
        let tarr = r#"[[],{},{"callbacks":[],"chain":[],"chord":[],"errbacks":[]}]"#;
        let barr = tarr.as_bytes();
        let jobj: Vec<Value> = serde_json::from_slice(barr).ok().unwrap();
        for val in jobj {
            let jstr = serde_json::to_string(&val);
            println!("{}", jstr.ok().unwrap());
        }
    }

    #[test]
    fn should_handle_headers(){
        let config = get_config();
        let mut headers = Headers::new("rs", "RABBITMQ","test_task", "id1", "id1");
        headers.retries = Some(2);
        headers.timelimit = Some(TimeLimit{ soft: 1000, hard: 2000 });
        let bmap = headers.convert_to_btree_map();
        let b = TaskConfigBuilder::default();
        let r= handle_amqp_headers(config, FieldTable::from(bmap), b);
        assert!(r.is_ok());
    }

    #[test]
    fn should_unpack_headers_to_task_config(){
        let config = get_config();
        let mut properties = BasicProperties::default();
        let mut headers = Headers::new("rs", "RABBITMQ", "test_task", "id1", "id1");
        headers.retries = Some(2);
        headers.expires = Some(1000);
        headers.eta = Some(1000);
        headers.timelimit = Some(TimeLimit{ soft: 1000, hard: 2000 });
        properties = properties.with_headers(FieldTable::from(headers.convert_to_btree_map()));
        properties = properties.with_correlation_id(amq_protocol_types::ShortString::from("id1"));
        properties = properties.with_priority(1);
        properties = properties.with_reply_to(amq_protocol_types::ShortString::from("id1"));
        let cfg_result = amqp_properties_to_task_config(
            config,"test_key", "test_exchange", properties);
        assert!(cfg_result.is_ok());
        let cfg = cfg_result.ok().unwrap();
        assert!(cfg.get_task_name().eq("test_task"));
        assert!(cfg.get_correlation_id().eq("id1"));
        assert!(cfg.get_routing_key().unwrap().eq("test_key"));
        assert!(cfg.get_task_lang().eq("rs"));
        assert_eq!(cfg.get_retries(), 2);
        assert_eq!(cfg.get_soft_time_limit(), 1000);
        assert_eq!(cfg.get_time_limit(), 2000);
        assert_eq!(cfg.get_eta(), 1000);
    }

    #[test]
    fn should_convert_amqp_bytes_to_stream_config(){
        let tarr = r#"[[1, 3, "hi"],{"mone": 1},{"callbacks":[],"chain":[],"chord":[],"errbacks":[]}]"#;
        let barr = tarr.as_bytes();
        let vu8 = Vec::from(barr);
        let stream_result = amqp_bytes_to_stream(vu8);
        assert!(stream_result.is_ok());
        let (args, kwargs, _sc) = stream_result.ok().unwrap();
        assert!(args.len() == 3);
        let argw = args.get(2).unwrap();
        if let ArgType::String(argw) = argw{
            assert!(argw.eq("hi"));
        }else{
            assert!(false);
        }
        let kwopt = kwargs.get("mone");
        assert!(kwopt.is_some());
        let kwval = kwopt.unwrap();
        if let ArgType::U64(kwval) = kwval{
            assert!(kwval.clone() == 1);
        }else{
            assert!(false);
        }
    }

    #[test]
    fn should_unpack_delivery_to_stream_task_tuple(){
        let cfg = get_config();
        let tarr = r#"[[1, 3, "hi"],{"mone": 1},{"callbacks":[],"chain":[],"chord":[],"errbacks":[]}]"#;
        let barr = tarr.as_bytes();
        let vu8 = Vec::from(barr);
        let mut properties = BasicProperties::default();
        let mut headers = Headers::new("rs", "RABBITMQ","test_task", "id1", "id1");
        headers.retries = Some(2);
        headers.expires = Some(1000);
        headers.eta = Some(1000);
        headers.timelimit = Some(TimeLimit{ soft: 1000, hard: 2000 });
        properties = properties.with_headers(FieldTable::from(headers.convert_to_btree_map()));
        properties = properties.with_correlation_id(amq_protocol_types::ShortString::from("id1"));
        properties = properties.with_priority(1);
        properties = properties.with_reply_to(amq_protocol_types::ShortString::from("id1"));
        let delivery = Delivery{
            delivery_tag: 0,
            exchange: amq_protocol_types::ShortString::from("test_exchange"),
            routing_key: amq_protocol_types::ShortString::from("test_key"),
            redelivered: false,
            properties,
            data: vu8,
            acker: Default::default()
        };
        let pack_result = unpack_delivery_from_amqp(cfg, delivery);
        assert!(pack_result.is_ok());
        let (args, kwargs, cfg, _sc) = pack_result.ok().unwrap();
        assert!(cfg.get_task_name().eq("test_task"));
        assert!(cfg.get_correlation_id().eq("id1"));
        assert!(cfg.get_routing_key().unwrap().eq("test_key"));
        assert!(cfg.get_task_lang().eq("rs"));
        assert_eq!(cfg.get_retries(), 2);
        assert_eq!(cfg.get_soft_time_limit(), 1000);
        assert_eq!(cfg.get_time_limit(), 2000);
        assert_eq!(cfg.get_eta(), 1000);
        assert!(args.len() == 3);
        let argw = args.get(2).unwrap();
        if let ArgType::String(argw) = argw{
            assert!(argw.eq("hi"));
        }else{
            assert!(false);
        }
        let kwopt = kwargs.get("mone");
        assert!(kwopt.is_some());
        let kwval = kwopt.unwrap();
        if let ArgType::U64(kwval) = kwval{
            assert!(kwval.clone() == 1);
        }else{
            assert!(false);
        }
    }
}
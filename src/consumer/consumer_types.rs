//! Consumer types enums including an enumeration storing the consumer type and another storing
//! the actual consumer.
//!
//! ---
//! author: Andrew Evans
//! ---

/// Enumeration containing the consumer type
#[derive(Clone, Debug)]
pub enum ConsumerType{
    RabbitMQ
}

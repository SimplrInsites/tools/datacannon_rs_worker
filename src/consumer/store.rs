//! Consumer store containing different consumers
//!
//! ---
//! author: Andrew Evans
//! ---

use crate::consumer::rabbitmq::RabbitMQConsumer;

/// Enumeration storing the available consumer
pub enum Consumer{
    RabbitMQ(RabbitMQConsumer)
}

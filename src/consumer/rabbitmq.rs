//! Consumer with threadpool from rabbitmq. The consumer contains a semaphor
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::{HashMap, HashSet};
use std::panic;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};
use std::time::Duration;

use amq_protocol_types::ShortUInt;
use datacannon_rs_core::argparse::argtype::ArgType;
use datacannon_rs_core::broker::amqp::rabbitmq::RabbitMQBroker;
use datacannon_rs_core::broker::broker_type;
use datacannon_rs_core::config::config::CannonConfig;
use datacannon_rs_core::error::broker_creation_error::BrokerCreationError;
use datacannon_rs_core::error::broker_start_error::BrokerStartError;
use datacannon_rs_core::error::consumer_create_error::ConsumerCreationError;
use datacannon_rs_core::message_protocol::stream::StreamConfig;
use datacannon_rs_core::message_structure::amqp::queue::AMQPQueue;
use datacannon_rs_core::message_structure::queue_trait::QueueHandler;
use datacannon_rs_core::message_structure::queues::GenericQueue;
use datacannon_rs_core::task::config::TaskConfig;
use datacannon_rs_core::task::result::{Response, TaskResponse};
use futures::{StreamExt, TryStreamExt};
use lapin::{Channel, Consumer, types::FieldTable};
use lapin::message::Delivery;
use lapin::options::*;
use tokio::sync::{mpsc, Mutex as TokioMutex, Notify, RwLock, Semaphore};
use tokio::sync::mpsc::Sender;
use tokio::task::JoinHandle;
use tokio::time;
use uuid::Uuid;

use crate::message::message_utils::{amqp_bytes_to_stream, amqp_properties_to_task_config};
use crate::task::registry;
use crate::task::registry::Registry;
use tokio::runtime::Runtime;

/// Configuration for the RabbitMQ consumer. Do not provide a sender unless the task registry for
/// that sender is configured.
///
/// # Variables
/// * `task_semaphore` - A general semaphore for controlling access to the pools acting as backpressure
/// * `io_task_sender` - If applicable, a sender to send tasks to the io pool
/// * `cpu_task_sender` - If applicable, a sender to send tasks to the default futures pool
/// * `custom_task_sender` - If applicable, a sender to the custom task pool
/// * `routers` - Routers for isntantiating the broker. Not all brokers need to be the same
/// * `status` - General application status atomic boolean
#[derive(Builder)]
pub struct RabbitMQConsumerConfig{
    pub config: CannonConfig<'static>,
    pub task_semaphore: Arc<Semaphore>,
    pub io_task_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
    pub cpu_task_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
    pub custom_task_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
    pub status: Arc<AtomicBool>,
    pub backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>,
    pub consumer_delay: Option<u64>,
    pub consumers_per_queue: usize
}


/// A RabbitMQ consumer
pub struct RabbitMQConsumer{
    config: CannonConfig<'static>,
    task_sema: Arc<Semaphore>,
    io_task_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
    cpu_task_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
    custom_task_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
    broker: RabbitMQBroker,
    consumer_handles: HashMap<String, (Arc<Notify>, Arc<AtomicBool>, JoinHandle<()>)>,
    status: Arc<AtomicBool>,
    backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>,
    consumer_delay: u64,
    consumers_per_queue: usize,
    registry: Registry
}


/// Implementation fo consumer
impl RabbitMQConsumer{

    ///Close the underlying consumers and broker. Wait until complete and issue errors.
    pub async fn close(&mut self){
        self.status.store(false, Ordering::Relaxed);
        let keys = self.consumer_handles.keys();
        for k in keys{
            let mut vopt = self.consumer_handles.get(k);
            if let Some((notify, status, _handle)) = vopt.take() {
                status.store(false, Ordering::Relaxed);
                let _r = time::timeout(Duration::from_millis(1000), notify.notified()).await;
            }
        }
        self.broker.close().await;
    }

    /// Handle missing message elements by creating an unknown task to send back if the reply
    /// key is known. Will avoid the message only if the reply to queue, routing_key, or exchange
    /// are unknown. The last two should never be missing as the message had to come from somewhere.
    ///
    /// # Arguments
    /// * `delivery` - The message delivery
    /// * `backend_senders` - Backend channel for sending task responses
    fn handle_message_with_missing_elements(
        config: CannonConfig<'static>,
        delivery: Delivery,
        backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>,
        receive_sema: Arc<Semaphore>){
        let properties = delivery.properties;
        let reply_key = properties.reply_to();
        if reply_key.is_some() {
            let reply_to = reply_key.clone().unwrap();
            let routing_key = delivery.routing_key.as_str();
            let exchange = delivery.exchange.as_str();
            let config_result = amqp_properties_to_task_config(
                config.clone(), routing_key, exchange, properties.clone());
            let mut cfg = TaskConfig::new(
                config,
                broker_type::RABBITMQ.to_string(),
                "UNKNOWN".to_string(),
                None,
                None,
                None,
                None,
                None,
                None,
                Some(reply_to.as_str().to_string()),
                None,
                None,
                None,
                None,
                None,
                1000000,
                None,
                None,
                None
            );
            if config_result.is_ok() {
                cfg = config_result.ok().unwrap();
            }
            let sc = amqp_bytes_to_stream(delivery.data);
            let mut stream_config = StreamConfig::new(None, None);
            if sc.is_ok(){
                stream_config = sc.ok().unwrap().2;
            }
            let response = Response::new();
            let task_response = TaskResponse::new(
                cfg,
                response,
                stream_config,
                false,
                Some("701 - Failed to Unwrap task".to_string())
            );
            for backend_sender in backend_senders {
                let _r = backend_sender.send(task_response.clone());
            }
        }
        receive_sema.add_permits(1);
    }

    /// Handle an unknown task. Sends a false result to the backend to allow for handling.
    ///
    /// # Arguments
    /// * `task_name` - Name of the task
    /// * `cfg` - The configuration for the task
    /// * `stream_config` - Stream Configuration
    /// * `backend_sender` - Channel for sending results to the backend
    fn handle_unknown_task(
        task_name: String, cfg: TaskConfig, stream_config: StreamConfig, backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>){
        let e = format!("409- Task Key Not Found [{}]", task_name);
        info!("{}", e.clone());
        let response = Response::new();
        let task_response = TaskResponse::new(
            cfg.clone(), response.clone(),stream_config.clone(),false, Some(e));
        for backend_sender in backend_senders {
            let _r = backend_sender.send(task_response.clone());
        }
    }

    /// Handle the tasks. Returns whether the task was found.
    ///
    /// # Arguments
    /// * `task_name` - Task name string
    /// * `args` - Vector of arguments
    /// * `kwargs` - Mapped function arguments
    /// * `cfg` - Task configuration
    /// * `stream_config` - Stream configuration
    /// * `io_sender` - The io pool sender
    /// * `cpu_sender` - The cpu threadpool sender
    /// * `custom_sender` - The custom threadpool sender
    /// * `io_keys` - Keys related to io tasks
    /// * `cpu_keys` - Keys related to cpu tasks
    /// * `custom_keys` - Keys related to custom tasks
    fn handle_task(
        task_name: String,
        args: Vec<ArgType>,
        kwargs: HashMap<String, ArgType>,
        cfg: TaskConfig,
        stream_config: StreamConfig,
        io_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
        cpu_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
        custom_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
        io_keys: HashSet<String>,
        cpu_keys: HashSet<String>,
        custom_keys: HashSet<String>) -> Result<bool, ()>{
        let mut found_task = false;
        if io_keys.contains(&task_name){
            if io_sender.is_some() {
                let _r = io_sender.clone().unwrap().send(
                    (args.clone(), kwargs.clone(), cfg.clone(), stream_config.clone()));
                found_task = true;
            }else{
                error!("IO Sender Not specified with IO Task");
            }
        }
        if cpu_keys.contains(&task_name){
            if cpu_sender.is_some() {
                let _r = cpu_sender.clone().unwrap().send(
                    (args.clone(), kwargs.clone(), cfg.clone(), stream_config.clone()));
                found_task = true;
            }else{
                error!("CPU Sender Not Specified with CPU Bound Task")
            }
        }
        if custom_keys.contains(&task_name) {
            if custom_sender.is_some() {
                let _r = custom_sender.clone().unwrap().send(
                    (args.clone(), kwargs.clone(), cfg.clone(), stream_config.clone()));
                found_task = true;
            }else{
                error!("Custom Sender Not Specified with Custom Task")
            }
        }
        Ok(found_task)
    }

    /// Handle the message delivery object from lapin.
    ///
    /// # Arguments
    /// * `delivery` - The message delivery
    /// * `io_sender` - The io pool sender
    /// * `cpu_sender` - The cpu threadpool sender
    /// * `custom_sender` - The custom threadpool sender
    /// * `io_keys` - Keys related to io tasks
    /// * `cpu_keys` - Keys related to cpu tasks
    /// * `custom_keys` - Keys related to custom tasks
    /// * `backend_senders` - Channel for sending results, required when message is not found
    /// * `sema` - Semaphore to control messages in cache at once
    fn handle_message_delivery(
        config: CannonConfig<'static>,
        delivery: Delivery,
        io_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
        cpu_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
        custom_sender: Option<mpsc::UnboundedSender<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>>,
        io_keys: HashSet<String>,
        cpu_keys: HashSet<String>,
        custom_keys: HashSet<String>,
        backend_senders: Vec<mpsc::UnboundedSender<TaskResponse>>,
        sema: Arc<Semaphore>){
        let handle_delivery = delivery.clone();
        let scopt = amqp_bytes_to_stream(delivery.data);
        let cfgopt = amqp_properties_to_task_config(
            config.clone(),
            delivery.routing_key.as_str(),
            delivery.exchange.as_str(),
            delivery.properties);
        if cfgopt.is_ok() && scopt.is_ok(){
            let (args, kwargs, sc) = scopt.unwrap();
            let cfg = cfgopt.unwrap();
            let task_name = cfg.get_task_name().clone();
            let handle_task_result = RabbitMQConsumer::handle_task(
                task_name.clone(), args, kwargs, cfg.clone(), sc.clone(), io_sender, cpu_sender, custom_sender, io_keys, cpu_keys, custom_keys);
            if handle_task_result.is_ok() && handle_task_result.ok().unwrap() == false {
                RabbitMQConsumer::handle_unknown_task(task_name, cfg, sc, backend_senders);
                sema.add_permits(1);
            }else if handle_task_result.is_err(){
                error!("412 - Task Retrieval Failed Despite being Non-Empty");
                sema.add_permits(1);
            }
        }else{
            RabbitMQConsumer::handle_message_with_missing_elements(
                config, handle_delivery, backend_senders, sema);
        }
    }

    /// Run the consumer in the runtime loop.
    ///
    /// # Arguments
    /// * `qname` - Name of the queue to consume from
    /// * `status` - Status of the application
    /// * `consumer_ch` - The consumer channel
    /// * `prefetch_count` - Number of messages to prefetch
    /// * `close_notifier` - Called when exiting
    async fn run_consumer(
        &mut self,
        qname: String,
        status: Arc<AtomicBool>,
        consumer_ch: Channel,
        prefetch_count: ShortUInt,
        close_notifier: Arc<Notify>) -> (String, JoinHandle<()>, Arc<AtomicBool>){
        let reg = self.registry.clone();
        let app_config = self.config.clone();
        let uid = format!("{}", Uuid::new_v4());
        let consumer_arc  = Arc::new(AtomicBool::new(true));
        let rconsumer_arc = consumer_arc.clone();
        let io_keys = registry::get_io_keys(
            reg.io_registry.clone()).await;
        let cpu_keys = registry::get_cpu_keys(reg.cpu_registry.clone());
        let custom_keys = registry::get_custom_keys(reg.custom_registry.clone());
        let io_sender = self.io_task_sender.clone();
        let  cpu_sender = self.cpu_task_sender.clone();
        let custom_sender = self.custom_task_sender.clone();
        let delay = self.consumer_delay.clone();
        let receive_sema = self.task_sema.clone();
        let backend_sender_rt = self.backend_senders.clone();
        let handle = tokio::spawn(async move{
            while status.load(Ordering::Relaxed) && rconsumer_arc.load(Ordering::Relaxed) {
                let copts = BasicConsumeOptions{
                    no_local: false,
                    no_ack: false,
                    exclusive: false,
                    nowait: false
                };
                let bqos_options = BasicQosOptions::default();
                let _r = consumer_ch.basic_qos(prefetch_count, bqos_options).await;
                let consumer_id = Uuid::new_v4();
                let consumer_name = format!("datacannon_consumer_{}", consumer_id);
                let n_swear = consumer_ch.basic_consume(
                    &qname, &consumer_name, copts, FieldTable::default());
                let consumer_r = n_swear.await;
                if consumer_r.is_ok() {
                    let consumer: Consumer = consumer_r.unwrap();
                    let mut consumer_stream = consumer.into_stream();
                    let mut delivery_opt = consumer_stream.next().await;
                    if delivery_opt.is_some() {
                        while status.load(Ordering::Relaxed)
                            && rconsumer_arc.load(Ordering::Relaxed)
                            && delivery_opt.is_some() {
                            let permit = receive_sema.acquire().await.unwrap();
                            permit.forget();
                            let (_ch, delivery) = delivery_opt
                                .unwrap()
                                .expect("error caught in in consumer");
                            let dtag = delivery.delivery_tag;
                            RabbitMQConsumer::handle_message_delivery(
                                app_config.clone(),
                                delivery,
                                io_sender.clone(),
                                cpu_sender.clone(),
                                custom_sender.clone(),
                                io_keys.clone(),
                                cpu_keys.clone(),
                                custom_keys.clone(),
                                backend_sender_rt.clone(),
                                receive_sema.clone());
                            let ackopts = BasicAckOptions::default();
                            let _r = consumer_ch
                                .basic_ack(dtag, ackopts)
                                .await;
                            delivery_opt = consumer_stream.next().await;
                        }
                    }else{
                        time::sleep(time::Duration::from_millis(delay.clone())).await;
                        receive_sema.add_permits(1);
                    }
                }
            }
            rconsumer_arc.store(false, Ordering::Relaxed);
            close_notifier.notify_one();
        });
        (uid, handle, consumer_arc)
    }

    /// Handle the actual creation of the consumer
    ///
    /// # Arguments
    /// * `q` - The AMQPQueue to use
    async fn create_consumer(&mut self, q: AMQPQueue) -> Result<bool, ConsumerCreationError> {
        for _i in 0..self.consumers_per_queue {
            let bref = &mut self.broker;
            let qname = q.get_name();
            let consumer_ch = bref.get_channel().await.unwrap();
            bref.add_channel_to_pool().await;
            let prefetch_count = q.get_prefetch_count();
            let notifier = Arc::new(Notify::new());
            let (uid, handle, state) = self.run_consumer(
                qname.to_string(),
                self.status.clone(),
                consumer_ch,
                prefetch_count,
                notifier.clone()).await;
            self.consumer_handles.insert(uid, (notifier, state, handle));
        }
        Ok(false)
    }

    /// Create the actual consumers from the provided routers which will be managed by the
    /// parent. This parent object will provide methods for checking consumer health and restarting
    /// as necessary.
    async fn create_consumers(&mut self) -> Result<bool, ConsumerCreationError>{
        let routers = self.config.routers.clone();
        let mut is_fail: bool = false;
        let router_vec =  routers.match_routers(".*");
        for ropt in router_vec {
            let r = ropt.unwrap().clone();
            for queue in r.get_queues() {
                let q = queue.clone();
                if let GenericQueue::AMQPQueue(q) = q {
                    let consumer_result = self.create_consumer(q).await;
                    if consumer_result.is_ok() {
                        let consumer_handle_result = consumer_result.ok().unwrap();
                        is_fail = consumer_handle_result;
                    }else{
                        is_fail = true;
                    }
                }
                if is_fail{
                    break;
                }
            }
            if is_fail{
                break;
            }
        }
        if is_fail{
            Err(ConsumerCreationError)
        }else{
            Ok(true)
        }
    }

    /// Start the underlying consumers based on the provided routers. Initializing the broker in
    /// the constructor ensures that the queues are created.
    pub async fn start(&mut self){
        let create_result = self.create_consumers().await;
        if create_result.is_err(){
            error!("502 - Failed to create Consumers");
            panic!("502 - Failed to create Consumers");
        }
    }

    /// Setup the consumer parent
    ///
    /// # Arguments
    /// * `registry` - Registry containing all tasks
    /// * `cannon_config` - Application configuration
    /// * `consumer_config` - Configuration for rabbitmq broker
    /// * `result_store` - Storage for results
    pub async fn new(
        registry: Registry,
        cannon_config: CannonConfig<'static>,
        config: RabbitMQConsumerConfig,
        result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>,
        rt: Arc<Runtime>) -> Result<Result<RabbitMQConsumer, BrokerStartError>, BrokerCreationError>{
        let delay = config.consumer_delay.unwrap_or(1000);
        let routers = cannon_config.routers.clone();
        let broker_result = RabbitMQBroker::new(
            cannon_config.clone(), routers.clone(), result_store, rt).await;
        if broker_result.is_ok() {
            let mut broker = broker_result.ok().unwrap();
            let is_started = broker.setup().await;
            if is_started.is_ok() {
                let consumer = RabbitMQConsumer {
                    config: cannon_config,
                    task_sema: config.task_semaphore,
                    io_task_sender: config.io_task_sender,
                    cpu_task_sender: config.cpu_task_sender,
                    custom_task_sender: config.custom_task_sender,
                    consumer_handles: HashMap::new(),
                    backend_senders: config.backend_senders.clone(),
                    broker,
                    status: config.status,
                    consumer_delay: delay,
                    consumers_per_queue: config.consumers_per_queue,
                    registry
                };
                Ok(Ok(consumer))
            }else{
                Ok(Err(BrokerStartError))
            }
        }else{
            Err(BrokerCreationError)
        }
    }
}


#[cfg(test)]
mod tests{
    use std::collections::HashMap;
    use std::env;
    use std::sync::{Arc, RwLock as Lock};
    use std::sync::atomic::AtomicBool;

    use amq_protocol_types::FieldTable;
    use datacannon_rs_core::argparse::argtype::ArgType;
    use datacannon_rs_core::backend::config::BackendConfig;
    use datacannon_rs_core::broker::amqp::rabbitmq::RabbitMQBroker;
    use datacannon_rs_core::broker::broker_type;
    use datacannon_rs_core::config::config::{BackendType, CannonConfig};
    use datacannon_rs_core::connection::amqp::connection_inf::AMQPConnectionInf;
    use datacannon_rs_core::connection::connection::ConnectionConfig;
    use datacannon_rs_core::message_protocol::stream::StreamConfig;
    use datacannon_rs_core::message_structure::amqp::exchange::Exchange;
    use datacannon_rs_core::message_structure::amqp::queue::AMQPQueue;
    use datacannon_rs_core::message_structure::queue_trait::QueueHandler;
    use datacannon_rs_core::message_structure::queues::GenericQueue;
    use datacannon_rs_core::replication::rabbitmq::{RabbitMQHAPolicies, RabbitMQHAPolicy};
    use datacannon_rs_core::replication::replication::HAPolicy;
    use datacannon_rs_core::router::router::{Router, Routers};
    use datacannon_rs_core::task::config::TaskConfig;
    use datacannon_rs_core::task::context::TaskContext;
    use datacannon_rs_core::task::result::{Response, TaskResponse};
    use lapin::ExchangeKind;
    use lapin::options::*;
    use tokio::runtime::Runtime;
    use tokio::sync::{Mutex as TokioMutex, RwLock, Semaphore};
    use tokio::sync::mpsc::{self, Sender};

    use crate::consumer::rabbitmq::{RabbitMQConsumer, RabbitMQConsumerConfig};
    use crate::task::registry::Registry;
    use crate::task::wrapper::Task;

    fn get_task_config(reply_to: String) -> TaskConfig{
        let cfg = get_config();
        TaskConfig::new(
            cfg,
            broker_type::RABBITMQ.to_string(),
            "test_it".to_string(),
            Some("test_exchange".to_string()),
            Some("DIRECT".to_string()),
            Some("test_key".to_string()),
            Some("test_parent_id".to_string()),
            None,
            None,
            Some(reply_to),
            None,
            None,
            None,
            None,
            None,
            10000,
            Some(1),
            None,
            None)
    }

    fn get_routers() -> Routers{
        let mut rts = Routers::new();
        let mut queues = Vec::<GenericQueue>::new();
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            5272,
            Some("test".to_string()),
            Some("dev".to_string()),
            Some("rtp*4500".to_string()),
            1000);
        let q= AMQPQueue::new("lapin_test_queue".to_string(), Some("test_exchange".to_string()), Some("test_key".to_string()), 0, HAPolicy::RabbitMQ(RabbitMQHAPolicy::new(RabbitMQHAPolicies::ALL, 1)), false, amq_conf);
        queues.push(GenericQueue::AMQPQueue(q));
        let exch = Exchange::new("test_exchange".to_string(), ExchangeKind::Direct);
        let router = Router::new("test_key".to_string(),queues, Some(exch));
        rts.add_router("test_key".to_string(), router);
        rts
    }

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user").ok().unwrap();
        let pwd = env::var("rabbit_test_pwd").ok().unwrap();
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user),
            Some(pwd), 1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = BackendConfig{
            url: "",
            username: None,
            password: None,
            transport_options: None
        };
        let routers = get_routers();
        let mut cannon_conf = CannonConfig::new(conn_conf, BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 10;
        cannon_conf.consumers_per_queue = 2;
        cannon_conf.max_concurrent_tasks = 1000;
        cannon_conf
    }

    fn get_runtime() -> Arc<Runtime>{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all().worker_threads(4).build();
        Arc::new(rt.unwrap())
    }

    async fn get_broker(
        results: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>,
        rt: Arc<Runtime>) -> RabbitMQBroker{
        let cfg = get_config();
        let rts = get_routers();
        let broker_result  = RabbitMQBroker::new(
            cfg,rts, results, rt).await;
        assert!(broker_result.is_ok());
        let mut broker = broker_result.ok().unwrap();
        let _r = broker.setup().await;
        broker
    }

    fn get_registry() -> Registry{
        let task = Task{
            f: test_it
        };
        let mut mp = HashMap::new();
        mp.insert("test_it", task);
        let tasks = Arc::new(Lock::new(mp));
        let registry = Registry{
            io_registry: Arc::new(RwLock::new(HashMap::new())),
            cpu_registry: tasks,
            custom_registry: Arc::new(Lock::new(HashMap::new()))
        };
        registry
    }

    fn test_it(_ctx: TaskContext) -> Result<Response, String>{
        //big test string
        let lstr = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. \
        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, \
        when an unknown printer took a galley of type and scrambled it to make a type specimen book.\
         It has survived not only five centuries, but also the leap into electronic typesetting, \
         remaining essentially unchanged. It was popularised in the 1960s with the release of \
         Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing \
         software like Aldus PageMaker including versions of Lorem Ipsum.";

        // really big shitty ((n^2)/2)m regex
        //let re = Regex::new(r"L.*").unwrap();
        //re.find_iter(lstr);

        //really small max nm loop
        let f = lstr.find("industry");
        if f.is_some(){
            let idx = f.unwrap();
            let splarr = lstr.split_at(idx);
            let _r = splarr.0.replace("Lorem", "Lore");
        }
        let mut r= Response::new();
        r.add_result("test_result".to_string(), ArgType::Bool(true));
        Ok(r)
    }

    fn get_rabbitmq_config() -> (Arc<AtomicBool>, mpsc::UnboundedReceiver<(Vec<ArgType>, HashMap<String, ArgType>, TaskConfig, StreamConfig)>, RabbitMQConsumerConfig, mpsc::UnboundedReceiver<TaskResponse>){
        let status = Arc::new(AtomicBool::new(true));
        let (task_sender, task_receiver) = mpsc::unbounded_channel();
        let (backend_sender, backend_receiver) = mpsc::unbounded_channel();
        let task_semaphore = Arc::new(Semaphore::new(2));
        let cfg = get_config();
        let rmc = RabbitMQConsumerConfig{
            config: cfg,
            task_semaphore: task_semaphore,
            io_task_sender: None,
            cpu_task_sender: Some(task_sender),
            custom_task_sender: None,
            status: status.clone(),
            consumer_delay: None,
            backend_senders: vec![backend_sender],
            consumers_per_queue: 2
        };
        (status, task_receiver, rmc, backend_receiver)
    }

    async fn push_fake_messages(num_messages: usize, rt: Arc<Runtime>) {
        let config = get_config();
        let results = Arc::new(RwLock::new(HashMap::new()));
        let mut broker = get_broker(results, rt).await;
        for _i in 0..num_messages {
            let cfg = get_task_config("test_reply".to_string());
            let sc = StreamConfig::new(
                None, None);
            let r = broker.send(
                config.clone(),
                cfg,
                Some(sc)).await;
            let ar = r.unwrap();
            let _r = ar.await;
        }
        broker.close().await;
    }

    #[test]
    fn should_create_consumer_parent(){
        let registry = get_registry();
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let config = get_config();
            let results = Arc::new(RwLock::new(HashMap::new()));
            let (_status, _receiever, cfg, _br) = get_rabbitmq_config();
            let rmq = RabbitMQConsumer::new(
                registry,config, cfg, results, rt).await;
            let outer_consumer = rmq.ok().unwrap();
            assert!(outer_consumer.is_ok());
            let _inner_consumer = outer_consumer.ok().unwrap();
        });
    }

    #[test]
    fn should_create_consumers(){
        let config = get_config();
        let rt = get_runtime();
        let registry = get_registry();
        rt.clone().block_on(async move {
            let results = Arc::new(RwLock::new(HashMap::new()));
            let (_status, _receiever, cfg, _br) = get_rabbitmq_config();
            let rmq = RabbitMQConsumer::new(
                registry,config,cfg, results, rt).await;
            let outer_consumer = rmq.ok().unwrap();
            assert!(outer_consumer.is_ok());
            let mut consumer = outer_consumer.ok().unwrap();
            consumer.start().await;
            consumer.close().await;
        });
    }

    #[test]
    fn should_consume_from_queue(){
        env_logger::init();
        let registry = get_registry();
        let config = get_config();
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let results = Arc::new(RwLock::new(HashMap::new()));
            let (_status, mut receiver, cfg, _br) = get_rabbitmq_config();
            let rmq = RabbitMQConsumer::new(
                registry,config, cfg, results, rt.clone()).await;
            let outer_consumer = rmq.ok().unwrap();
            assert!(outer_consumer.is_ok());
            let mut consumer = outer_consumer.ok().unwrap();
            consumer.start().await;
            push_fake_messages(100, rt).await;
            for _i in 0..(100 as i32) {
                let r = receiver.recv().await;
                assert!(r.is_some());
            }
            consumer.close().await;
        });
    }

    #[test]
    fn should_push_to_threadpool(){
        let registry = get_registry();
        let rt = get_runtime();
        let config = get_config();
        rt.clone().block_on(async move {
            let (_status, mut receiver, cfg, _br) = get_rabbitmq_config();
            let ts = cfg.task_semaphore.clone();
            let results = Arc::new(RwLock::new(HashMap::new()));
            let rmq = RabbitMQConsumer::new(
                registry, config, cfg, results, rt.clone()).await;
            let outer_consumer = rmq.ok().unwrap();
            assert!(outer_consumer.is_ok());
            let mut consumer = outer_consumer.ok().unwrap();
            consumer.start().await;
            push_fake_messages(1000, rt).await;
            for _i in 0..1000 as i32 {
                let r = receiver.recv().await;
                ts.add_permits(1);
                assert!(r.is_some());
            }
            let _r = consumer.close().await;
        });
    }

    #[test]
    fn get_consumer_in_test(){
        let rt = get_runtime();
        let ctx_ptr = get_config();
        rt.clone().block_on(async move {
            let results = Arc::new(RwLock::new(HashMap::new()));
            let mut broker = get_broker(results, rt).await;
            let sema_arc = broker.get_channel_semaphore();
            let amq_conf = AMQPConnectionInf::new(
                "amqp".to_string(),
                "127.0.0.1".to_string(),
                5272,
                Some("test".to_string()),
                Some("dev".to_string()),
                Some("rtp*4500".to_string()),
                1000);
            let aq = AMQPQueue::new(
                "lapin_test_queue_two".to_string(),
                Some("test_exchange".to_string()),
                Some("test_key".to_string()),
                1,
                HAPolicy::RabbitMQ(RabbitMQHAPolicy::new(RabbitMQHAPolicies::ALL, 1)),
                true,
                amq_conf
            );
            let gq = GenericQueue::AMQPQueue(aq.clone());
            let exchange = Exchange::new("test_exchange".to_string(), ExchangeKind::Direct);
            let router = Router::new("test_key".to_string(), vec![gq.clone()], Some(exchange));
            let mut routers = Routers::new();
            routers.add_router("test_key".to_string(), router);
            let ctx_cfg = ctx_ptr.routers.clone();
            for ropt in ctx_cfg.match_routers(".*") {
                let r = ropt.unwrap().clone();
                for queue in r.get_queues() {
                    let q = queue.clone();
                    if let GenericQueue::AMQPQueue(q) = q {
                        let bref = &mut broker;
                        let qname = q.get_name();
                        let cname = q.get_name().clone();
                        let oarc = sema_arc.clone();
                        let permit = oarc.acquire().await.unwrap();
                        permit.forget();
                        let ch = bref.get_channel().await.unwrap();
                        let consumer_opt = ch.basic_consume(
                            qname, cname, BasicConsumeOptions::default(), FieldTable::default()).await;
                        assert!(consumer_opt.is_ok());
                        bref.release_channel(ch).await;
                        oarc.add_permits(1);
                    }
                }
            }
            broker.close().await;
        });
    }
}

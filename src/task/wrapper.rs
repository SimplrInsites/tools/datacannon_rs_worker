//! Wrapper around the task context
//!
//! ---
//! author: Andrew Evans
//! ---

use datacannon_rs_core::task::context::TaskContext;
use datacannon_rs_core::task::result::Response;
use tokio::task::JoinHandle;

/// Wrapper for a Future Task
#[derive(Clone)]
pub struct FutureTask{
    pub f: fn(TaskContext) -> JoinHandle<Result<Response, String>>
}

/// Standard task composed of a single function
#[derive(Clone)]
pub struct Task{
    pub f: fn(TaskContext) -> Result<Response, String>
}
//! A task runner. The task runner wraps a function which is registered to the registry.
//! The task runner handles a push to any backend or the broker when a chain is present.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;
use std::sync::Arc;

use datacannon_rs_core::argparse::argtype::ArgType;
use datacannon_rs_core::message_protocol::stream::StreamConfig;
use datacannon_rs_core::task::config::TaskConfig;
use datacannon_rs_core::task::context::TaskContext;
use datacannon_rs_core::task::result::Response;

/// Task Runner structures which stores a oneshot to wait on and contains
/// the asynchronous task to execute the thunk on. Tasks are not converted
/// in the worker until the task runner functions starts. The output is a
/// message used to decipher how to continue in the Tokio loop
#[derive(Clone, Debug)]
pub struct TaskRunner{
    pub args: Vec<ArgType>,
    pub kwargs: HashMap<String, ArgType>,
    pub task_config: TaskConfig,
    pub stream_config: StreamConfig,
    pub f: Arc<fn(ctx: TaskContext) -> Result<Response, String>>
}


/// Task Runner
impl TaskRunner{

    /// Get the function but consume the runner to allow for movement.
    pub fn consume_fn(self) -> Arc<fn(TaskContext) -> Result<Response, String>>{
        self.f
    }

    /// Get a reference to the underlying task config
    pub fn get_task(&mut self) -> &TaskConfig{
        &self.task_config
    }

    /// Get a reference to the funtion to execute
    pub fn get_fn(&mut self) -> &Arc<fn(TaskContext) -> Result<Response,String>>{
        &self.f
    }

    /// Get a reference to the stream config
    pub fn get_stream_config(&mut self) -> &StreamConfig{
        &self.stream_config
    }

    /// Create a new task runn
    ///
    /// # Arguments
    /// * `received_task` - Task name to execute
    /// * `f` - The function to execute
    #[allow(dead_code)]
    pub fn new(
        args: Vec<ArgType>,
        kwargs: HashMap<String,ArgType>,
        task_config: TaskConfig,
        stream_config: StreamConfig,
        f: fn(TaskContext) -> Result<Response, String>) -> TaskRunner {
        TaskRunner{
            args: args,
            kwargs: kwargs,
            f: Arc::new(f),
            task_config: task_config,
            stream_config: stream_config
        }
    }
}
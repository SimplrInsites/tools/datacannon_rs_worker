//! Task registry
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::{HashMap, HashSet};
use std::sync::Arc;
use std::sync::RwLock as Lock;

use tokio::sync::RwLock;

use crate::task::wrapper::{FutureTask, Task};

/// Registry containing the threadpool specific task registries
#[derive(Clone)]
pub struct Registry{
    pub io_registry: Arc<RwLock<HashMap<&'static str, FutureTask>>>,
    pub cpu_registry: Arc<Lock<HashMap<&'static str, Task>>>,
    pub custom_registry: Arc<Lock<HashMap<&'static str, Task>>>
}


/// Gets a key set for the io registry
///
/// # Arguments
/// * `io_registry` - IO Registry
pub(in crate) async fn get_io_keys(
    io_registry: Arc<RwLock<HashMap<&'static str, FutureTask>>>) -> HashSet<String>{
    let mut hset = HashSet::new();
    let registry = io_registry;
    let rlock = registry.read().await;
    let keys = (*rlock).keys();
    for k in keys{
        hset.insert(k.to_string());
    }
    hset
}

/// Get cpu task keys
///
/// # Arguments
/// * `cpu_registry` - CPU registry
pub(in crate) fn get_cpu_keys(
    cpu_registry: Arc<Lock<HashMap<&'static str, Task>>>) -> HashSet<String>{
    let mut hset = HashSet::new();
    let registry = cpu_registry;
    let rlock = registry.read();
    if rlock.is_ok() {
        let gaurd = rlock.unwrap();
        let keys = (*gaurd).keys();
        for k in keys {
            hset.insert(k.to_string());
        }
    }
    hset
}

/// Get custom keys
///
/// # Arguments
/// * `custom_registry` - Custom task registry
pub(in crate) fn get_custom_keys(
    custom_registry: Arc<Lock<HashMap<&'static str, Task>>>) -> HashSet<String>{
    let mut hset = HashSet::new();
    let registry = custom_registry.clone();
    let rlock = registry.read();
    if rlock.is_ok() {
        let gaurd = rlock.unwrap();
        let keys = (*gaurd).keys();
        for k in keys {
            hset.insert(k.to_string());
        }
    }
    hset
}
